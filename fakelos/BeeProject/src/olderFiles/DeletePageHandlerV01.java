package bee.option;

import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.ListCellRenderer;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import bee.undoRedo.EdgeMoveChange;
import bee.controlers.LVControler;
import bee.option.EdgePropertyHandler.MyListCellRenderer;
import bee.option.options.Utilities;
import y.base.Edge;
import y.base.EdgeCursor;
import y.option.OptionHandler;
import y.view.EdgeRealizer;
import y.view.Graph2D;

public class DeletePageHandlerV01 extends OptionHandler{

	LVControler lvcontroler;
	int pages;

	public DeletePageHandlerV01(LVControler controler) {

		super("Delete Page");

		this.lvcontroler=controler;
	}



	public void createHandler(){


		Color	col [] = new Color [pages];
		for(int i=0;i<col.length;i++){

			col[i]=Utilities.COLORS[i];

		}

		JList<Color> list= new JList<Color>(col );	 	
		ListCellRenderer<Color> renderer = new MyListCellRenderer<Color>();
		list.setCellRenderer(renderer);

		this.clear();
		this. addEnum("Delete Page", col, col[0], renderer) ; 

	}
	
	
	
	public void commitProperties(){

		int	num= getEnum("Delete Page");
		
		if(num>0){

		boolean delete =false; 
		int reply = JOptionPane.showConfirmDialog(getEditor(),
				"Do you want to delete this page? If it contains pages these pages are going to move to page 0 cyan ",
				"Delete Page",
				JOptionPane.YES_NO_OPTION);		
		
		if(reply== JOptionPane.YES_OPTION){
			delete=true;
		}				            
		if(delete){
//			lvcontroler.deletePage(num);          	
		}
		
		
		
		lvcontroler.informMainControler();

	
			pages--;
			createHandler();
		
		}
		else{			

		    JOptionPane.showMessageDialog(getEditor(), "Could not delete this page", "Error", JOptionPane.ERROR_MESSAGE);
		}


	}//commitEdgeProperies
	
	
	
	public void setPages(int numPages){
		pages=numPages;
	}

	public int getPages(){
		return pages;
	}

	
	
	class MyListCellRenderer<T> implements ListCellRenderer<T> {


		protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();
		
		

		@Override
		public Component getListCellRendererComponent(JList list,
				Object value, int index, boolean isSelected,
				boolean cellHasFocus) {
			JLabel renderer=(JLabel) defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
			renderer.setText(Integer.toString(index));
			if(index>-1){
				renderer.setBackground(new Color(255,255,240));
				renderer.setForeground(Utilities.COLORS[index].darker());
					renderer.setText("  Page  "+ index +"   "+ Utilities.COLOR_NAMES[index]);
				renderer.setFont(Utilities.CUSTOM_FONT);
				renderer.setVerticalAlignment(SwingConstants.CENTER);
				LineBorder line= new LineBorder(Utilities.COLORS[index],3,true);
				renderer.setBorder(BorderFactory.createTitledBorder(line));				
			}
			return renderer;
		}//getListCellRendererComponent

	}//implements ListCellRenderer

}//DeletePageHandler
