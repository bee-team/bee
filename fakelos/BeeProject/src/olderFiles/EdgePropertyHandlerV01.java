package bee.option;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.ListCellRenderer;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import y.base.Edge;
import y.base.EdgeCursor;
import y.option.OptionHandler;
import y.view.EdgeRealizer;
import y.view.Graph2D;
import bee.undoRedo.Changeable;
//import bee.demo.Changeable;
import bee.undoRedo.EdgeMoveChange;
import bee.demo.LVControler;
import bee.option.options.Utilities;



public class EdgePropertyHandlerV01 extends OptionHandler{


	Color preColor;
	Color newColor;


	
	Color [] col;//=Utilities.COLORS;
		

//	int pages;

	


	LVControler lvcontroler;


	public EdgePropertyHandler(LVControler controler){	

		super(" Proprties");

		this.lvcontroler=controler;




	}//const
	
	public void createHandler(){
		
		
		
		
		int size;
		int pages=this.lvcontroler.getPages();
		
		if(pages==Utilities.COLORS.length)
			 size=pages;
		else
			size=pages+1;
		col= new Color [size];
		for(int i=0;i<col.length;i++){
			
				col[i]=Utilities.COLORS[i];
			
		}
		
		JList<Color> list= new JList<Color>(col );	 	
		ListCellRenderer<Color> renderer = new MyListCellRenderer<Color>();
		list.setCellRenderer(renderer);
		
		this.clear();
		
		this. addEnum("Move to Page", col, col[0], renderer) ; 
		
	}
	
	


	public void updateValuesFromSelection(Graph2D graph){

		EdgeCursor ec = graph.selectedEdges();
		EdgeRealizer er =graph.getRealizer(ec.edge());

		//get the initial values from the first selected edge

		preColor = er.getLineColor();

		boolean samecolor = true;
		
		//boolean page=false;
		//get all further values from the remaining set of selected edges
		if(ec.size() > 1){
			for (ec.next(); ec.ok(); ec.next())
			{
				er = graph.getRealizer(ec.edge());
				if (samecolor && preColor != er.getLineColor())
					samecolor = false;
			}
			set("Move to Page", preColor);
			getItem("Move to Page").setValueUndefined(!samecolor);
			
			
			//set("Delete Page",)

		}
	}//updateVal





	
	

	public void commitEdgeProperties(Graph2D graph){
		

		System.out.println("committing edge changes");

		List<int[]> edgeIndex=new ArrayList<int []>();
		EdgeCursor ec = graph.selectedEdges();
		int[] previousPages=new int[ec.size()];
		int num=0;
		int pos=0;
		for (ec.toFirst(); ec.ok(); ec.next()){

			Edge edge = ec.edge();
			EdgeRealizer er =graph.getRealizer(edge);

			int [] current=new int[2];
			current[0]=edge.source().index();
			current[1]=edge.target().index();
			edgeIndex.add(current);
			previousPages[pos]=Utilities.getColorIndex(er.getLineColor());
					pos++;
			if (!getItem("Move to Page").isValueUndefined())
				er.setLineColor((Color)get("Move to Page"));



			newColor=(Color)get("Move to Page");			 
			num= getEnum("Move to Page");


			lvcontroler.moveEdges(edgeIndex,num);
			
			int pages=this.lvcontroler.getPages();
			if(num>=pages){
				System.out.println(" num "+ num);
				pages++;			
			}


		}//for each edge
		Changeable edgeChange=new EdgeMoveChange(edgeIndex, num, previousPages, lvcontroler);
		
		
		createHandler();
		
		lvcontroler.informMainControler();

	}//commitEdgeProperies






//	public void setPages(int numPages){
//		pages=numPages;
//	}
//
//	public int getPages(){
//		return pages;
//	}







	class MyListCellRenderer<T> implements ListCellRenderer<T> {


		protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();

		@Override
		public Component getListCellRendererComponent(JList list,
				Object value, int index, boolean isSelected,
				boolean cellHasFocus) {
			JLabel renderer=(JLabel) defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
			renderer.setText(Integer.toString(index));
			if(index>-1){
				renderer.setBackground(new Color(255,255,240));
				//System.out.println("index is " +index);
				renderer.setForeground(col[index].darker());
				//System.out.println("in eListCellRenderer the color "+index+"  is the "+col[index]+" with name "+Utilities.COLOR_NAMES[index]);
				int pages=lvcontroler.getPages();
				if(index==pages)
					renderer.setText(" New Page  "+ index +"   "+ Utilities.COLOR_NAMES[index]);
				else
					renderer.setText("  Page  "+ index +"   "+ Utilities.COLOR_NAMES[index]);
				renderer.setFont(Utilities.CUSTOM_FONT);
				renderer.setVerticalAlignment(SwingConstants.CENTER);
				LineBorder line= new LineBorder(col[index],3,true);
				renderer.setBorder(BorderFactory.createTitledBorder(line));				
			}
			return renderer;
		}//getListCellRendererComponent

	}//implements ListCellRenderer






}// class EdgePropertyHandler	
