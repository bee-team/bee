package bee.controlers;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;













import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.ListCellRenderer;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;

import y.base.Node;
import y.option.OptionHandler;
import y.view.EditMode;
import y.view.Graph2D;
import y.view.Graph2DSelectionEvent;
import y.view.Graph2DSelectionListener;
import y.view.Graph2DView;
import y.view.ViewMode;
import bee.algo.Crossings;
import bee.algo.Heuristic;
import bee.demo.graphTemplate.EmbeddedGraph;
import bee.layout.MySimpleLayouter;
import bee.option.DeletePageHandler;
import bee.option.EdgePropertyHandler;
import bee.option.MyEditModeLooseConstraint;
import bee.option.options.Utilities;
import bee.undoRedo.Changeable;
import bee.view.MyGraph2DView;
import bee.view.MyMoveSelectionMode;
import bee.view.MyPopupMode;
import bee.view.MyZoomWheelListener;
import bee.view.MyPopupMode.ShowSelectedEdgesOptions;
import y.view.Graph2DCanvas;

public class LVControlerV01 {


	private EmbeddedGraph embeddedGraph;
	public EmbeddedGraph getEmbeddedGraph()
	{
		return embeddedGraph;
	}


	private List<MyGraph2DView> layviewlist;
	private int pages;
	public int getPages()
	{
		return pages;
	}



	private MainControler mainControler;
	private Crossings crossinghecker;


	public LVControlerV01(List<JPanel> panels, Graph2D intGraph) {
		this.pages=0;

		createCrossings();		
		embeddedGraph=new EmbeddedGraph((Graph2D) intGraph);
		layviewlist= new ArrayList<MyGraph2DView>();
		for(int i=0; i<panels.size();i++){
			MyGraph2DView layview= new MyGraph2DView();
			layviewlist.add(layview);
			layview.getGraph2D().clear();

			layview.getCanvasComponent().setFont(new Font("Serif", Font.PLAIN, 12));
			layview.getCanvasComponent().setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
			layview.getCanvasComponent().setAlignmentY(Component.TOP_ALIGNMENT);
			layview.getCanvasComponent().setAlignmentX(Component.LEFT_ALIGNMENT);
			layview.setBounds(340, 5, 1250 , 250);
			layview.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.LOWERED, null, null, new Color(191, 205, 219), null), (Border) new LineBorder((new Color(227, 227, 227)))));
			layview.setPreferredSize(new Dimension(1250,220));
			MyGraph2DSelectionListener listener=new MyGraph2DSelectionListener();
			//			System.out.println(listener);
			layview.getGraph2D().addGraph2DSelectionListener(listener);
			//			layviewlist.add(layview);
			panels.get(i).add(layview,BorderLayout.CENTER);
		}//for
	}

	//=================================================================	
	//OPEN-RESET ACTION
	//=================================================================		

	public void open(Graph2D intGraph)
	{
		this.pages=0;

		//		embeddedGraph.getGraph2D().clear();
		embeddedGraph.clear();
		//embeddedGraph.setGraph2D((Graph2D) intGraph.createCopy());
		for(int i=0;i<layviewlist.size();i++)
		{
			layviewlist.get(i).getGraph2D().clear();
			layviewlist.get(i).updateView();
			//layviewlist.get(i).setGraph2D((Graph2D)intGraph.createCopy());
		}
	}


	//=================================================================	
	//              CREATE BOOK EMBEDDING ACTION
	//=================================================================		


	public void createLayoutView(Graph2DView graph2dview, int numPages){

		pages=numPages;


		double height=layviewlist.get(0).getCanvasSize().height;
		if(layviewlist.get(0).getGraph2D().nodeCount()>0)
		{
			Node v=layviewlist.get(0).getGraph2D().firstNode();
			height=2*layviewlist.get(0).getGraph2D().getRealizer(v).getCenterY();
			System.out.println(height);
		}
		//		((Graph2DCanvas)layviewlist.get(0).getCanvasComponent()).setCenter(layviewlist.get(0).getCanvasComponent().getWidth()/2, height/2);
		MySimpleLayouter layouter= new  MySimpleLayouter(100,layviewlist.get(0).getCanvasComponent().getWidth(),height);

		layouter.setNumberOfPages(pages);


		Graph2D tempGraph=layouter.doLayoutCore((Graph2D)graph2dview.getGraph2D().createCopy());		
		embeddedGraph.load(tempGraph, true);

		double minDist=layouter.getMinimalNodeDistance();
		double offset=layouter.getOffset();
		height=layouter.getHeight();

		initializeLayviewGraphs(tempGraph, numPages, minDist, height, offset);
	}



	//=================================================================	
	// OPEN THIS BOOK EMBEDDING ACTION
	//=================================================================	

	//	public void openThisBE(Graph2D tempGraph) {
	//		// TODO Auto-generated method stub
	//		
	//		embeddedGraph.load(tempGraph, true);
	//
	//		MySimpleLayouter layouter= new  MySimpleLayouter(100,layviewlist.get(0).getCanvasComponent().getWidth(),layviewlist.get(0).getCanvasSize().height);
	//		layouter.calcMetrics(tempGraph);		
	//		double minDist=layouter.getMinimalNodeDistance();
	//		double offset=layouter.getOffset();
	//		double height=layouter.getHeight();
	//
	//		
	//		MyZoomWheelListener mwzl =   new MyZoomWheelListener();
	//		mwzl.setLVControler(this);
	//
	//		int pages=embeddedGraph.getNumPages();
	//		
	////		mainControler.setPages(pages);
	//		
	//		EdgePropertyHandler eph= new EdgePropertyHandler(this);		
	//		eph.setPages(pages);
	//		eph.createHandler();	
	//
	//
	//		int[][]activeColors= Utilities.createActiveColors(pages);
	//
	//
	//
	//		for(int i=0; i<layviewlist.size();i++){
	//			MyGraph2DView layview= layviewlist.get(i);
	//
	//			
	//			//layview.getEmbeddedGraph(embeddedGraph);//den einai lush dioti kai sthn load oi komboi pernontai opws einai
	//			layview.setGraph2D((Graph2D)tempGraph.createCopy(), offset, minDist);
	//			
	//			layview.setActiveColors(activeColors[i]);
	//			layview.setFitContentOnResize(true);
	//
	//			EditMode  editMode = new MyEditModeLooseConstraint(); 
	//
	//			MyMoveSelectionMode	msm=new MyMoveSelectionMode(offset, height/2, minDist);
	//
	//			msm.setLVControler(this);
	//
	//			editMode.setMoveSelectionMode(msm);
	//			MyPopupMode pUm= new MyPopupMode();
	//			pUm.setGraph2D(layview.getGraph2D());
	//			pUm.setEdgePropertyHandler(eph);
	//			pUm.setLVC(this);
	//
	//			editMode.setPopupMode(pUm);
	//			layview.addViewMode(editMode); 
	//			layview.getCanvasComponent().addMouseWheelListener(mwzl);
	//
	//			layview.updateNodeView();
	//			layview.updateEdgeView();
	//			layview.updateView();
	//			layview.setVisible(true);
	//
	//			informMainControler();
	//		}
	//	}

	//=================================================================	
	//FIT CONTENT ACTION
	//=================================================================	

	public void fitContent()
	{
		for(int i=0;i<layviewlist.size();i++)
		{


			layviewlist.get(i).setZoom(1);
			layviewlist.get(i).updateNodeView();
			layviewlist.get(i).updateView();
		}

	}


	//=================================================================	
	// MOVE ELEMENTS
	//=================================================================		

	public void moveEdges(List<int []> edgeIndex, int toPage){

		System.out.println("moveEdges method in lvcontroler");
		if(toPage>=this.pages)
			pages++;

		embeddedGraph.moveEdges(edgeIndex, toPage);

		int [] [] activePages = new int  [layviewlist.size()] [];
		for(int i=0; i<layviewlist.size();i++){

			layviewlist.get(i).moveEdges(edgeIndex, toPage);
			System.out.println("layviewlist.get(i).activatePage(toPage)");
			layviewlist.get(i).activatePage(toPage);
			activePages[i]= layviewlist.get(i).getActivePages();

			layviewlist.get(i).updateEdgeView();
			layviewlist.get(i).setVisible(true);
		}


		infoMainControlerToActivatePage(activePages);
		System.out.println("Number of pages "+this.pages);
	}


	public void moveNodes(List<Integer> nodeIndex, int distIndex){		

		embeddedGraph.moveNodes(nodeIndex, distIndex);

		for(int s=0;s<layviewlist.size();s++)
		{
			layviewlist.get(s).moveNodes(nodeIndex, distIndex);
			//			layview.reverseEdges(node2);
			layviewlist.get(s).updateNodeView();
			layviewlist.get(s).updateEdgeView();
			layviewlist.get(s).setVisible(true);
			layviewlist.get(s).updateView();	

		}
	}//moveNodes




	//=================================================================	
	//ACTIVATE-DEACTIVATE PAGES
	//=================================================================	

	public void hidePage(JPanel panel, int index)
	{
		BorderLayout layout = (BorderLayout) panel.getLayout();
		MyGraph2DView graph=(MyGraph2DView)layout.getLayoutComponent(BorderLayout.CENTER);
		graph.hidePage(index);
	}

	public void showPage(JPanel panel, int index, int page)
	{
		BorderLayout layout = (BorderLayout) panel.getLayout();
		MyGraph2DView graph=(MyGraph2DView)layout.getLayoutComponent(BorderLayout.CENTER);
		graph.showPage(index, page);
	}


	public void infoMainControlerToActivatePage(int [] [] activecolors){
		mainControler.activatePage(activecolors);
	}

	//****

	public int [] getActiveColors(){

		int []activeColors= layviewlist.get(0).getActivePages();

		return activeColors;
	}







	//***

	public List<MyGraph2DView> getLayviewlist() {
		return layviewlist;
	}



	//*************  Open Connected Components


	public void open(LinkedList<Graph2D> graphList,	boolean firstTime)
	{

		//		Graph2D graph=new Graph2D();
		//		
		//		for(int i=0;i<graphList.size();i++){
		//			//for(int j=0;j<
		//			Graph2D g=	graphList.get(i);
		//			
		//		}


		//	embeddedGraph.clear();

		for(int i=0;i<layviewlist.size();i++)
		{
			layviewlist.get(i).getGraph2D().clear();


			layviewlist.get(i).updateView();

		}
	}





	//======================================
	//               Crossing
	//======================================

	public void createCrossings(){
		//Crossings
		crossinghecker= new Crossings();
		//crossinghecker.setPages(pages);
	}

	public Map<Color, Integer> updateCrossings() {

		Map<Color,Integer> crossingsMap= crossinghecker.checkCrossings(embeddedGraph);
		return crossingsMap;
	}




	//======================================
	//               Zoom
	//======================================


	public void setZoom(double zoom, Point2D p)
	{
		for(int i=0;i<this.layviewlist.size();i++)
		{
			Graph2DView graph=layviewlist.get(i);
			Graph2DCanvas canvas=(Graph2DCanvas)graph.getCanvasComponent();
			canvas.setZoom(zoom);
			canvas.setCenter(p.getX(), p.getY());
			graph.updateView();
		}
	}

	//*********************************************


	public void informMainControler() {
		mainControler.updateCrossings();	

	}



	public void setMainControler(MainControler mainControler) {
		this.mainControler=mainControler;

	}

	//***************************************************



	public void addChangeable(Changeable change)
	{
		this.mainControler.enableUndo();

		this.mainControler.getChangeManager().addChangeable(change);
		// this.controler.getChangeManager().print();
	}





	public Graph2D getBEGraph()
	{
		return layviewlist.get(0).getGraph2D();
	}



	public void load(EmbeddedGraph embeddedGraph, Graph2D graphView)
	{
		this.embeddedGraph=embeddedGraph;
		this.pages=embeddedGraph.getNumPages();

		double height=layviewlist.get(0).getCanvasSize().height;
		if(layviewlist.get(0).getGraph2D().nodeCount()>0)
		{
			Node v=layviewlist.get(0).getGraph2D().firstNode();
			height=2*layviewlist.get(0).getGraph2D().getRealizer(v).getCenterY();
			System.out.println(height);
		}
		MySimpleLayouter layouter= new  MySimpleLayouter(100,layviewlist.get(0).getCanvasComponent().getWidth(),height);
		layouter.setNumberOfPages(pages);

		Graph2D tempGraph=layouter.doNodeLayoutCore((Graph2D) graphView.createCopy(), embeddedGraph.getNodeOrder());	
		//		Graph2D tempGraph=layouter.doLayoutCore((Graph2D)graph2dview.getGraph2D().createCopy());		
		//		embeddedGraph.load(tempGraph, true);
		double minDist=layouter.getMinimalNodeDistance();
		double offset=layouter.getOffset();
		height=layouter.getHeight();
		initializeLayviewGraphs(tempGraph, pages, minDist, height, offset);

	}

	private void initializeLayviewGraphs(Graph2D tempGraph, int noOfPages, double minDist, double height, double offset)
	{
		//		MySimpleLayouter layouter= new  MySimpleLayouter(100,layviewlist.get(0).getCanvasComponent().getWidth(),layviewlist.get(0).getCanvasSize().height);
		//		layouter.setNumberOfPages(noOfPages);
		//
		//		Graph2D tempGraph=layouter.doNodeLayoutCore((Graph2D) graphView.createCopy(), embeddedGraph.getNodeOrder());
		//		double minDist=layouter.getMinimalNodeDistance();
		//		double offset=layouter.getOffset();
		//		double height=layouter.getHeight();
		//		System.out.println(height);
		MyZoomWheelListener mwzl =   new MyZoomWheelListener();
		mwzl.setLVControler(this);
		EdgePropertyHandler eph= new EdgePropertyHandler(this);	
		eph.createHandler();
		int[][]activeColors= Utilities.createActiveColors(noOfPages);
		for(int i=0; i<layviewlist.size();i++){
			MyGraph2DView layview= layviewlist.get(i);	
			layview.getGraph2D().clear();
			layview.fitContent();
			layview.updateView();
			layview.setGraph2D((Graph2D)tempGraph.createCopy(), offset, minDist);	
			MyGraph2DSelectionListener listener=new MyGraph2DSelectionListener();
			//			System.out.println(listener);
						layview.getGraph2D().addGraph2DSelectionListener(listener);
			//			Iterator iter=layview.getGraph2D().getGraph2DSelectionListeners();
			//			while(iter.hasNext())
			//			{
			//				Graph2DSelectionListener listenerTemp=(Graph2DSelectionListener)iter.next();
			//				System.out.println(i+"  "+listenerTemp.getClass().getName());
			//			}
			layview.setActiveColors(activeColors[i]);
			layview.setFitContentOnResize(true);
			ViewMode oldMode=layview.getViewControl().getCurrentMode();
			if(!(oldMode instanceof MyEditModeLooseConstraint))
			{
				layview.getViewControl().remove(oldMode);
				//				System.out.println("create new mode");
				EditMode  editMode = new MyEditModeLooseConstraint(); 
				MyMoveSelectionMode	msm=new MyMoveSelectionMode(offset, height/2, minDist);
				msm.setLVControler(this);		
				editMode.setMoveSelectionMode(msm);
				MyPopupMode pUm= new MyPopupMode();
				pUm.setGraph2D(layview.getGraph2D());
				pUm.setEdgePropertyHandler(eph);
				pUm.setLVC(this);
				editMode.setPopupMode(pUm);
				layview.addViewMode(editMode); 
				layview.getViewControl().setMode(editMode);
				layview.getCanvasComponent().addMouseWheelListener(mwzl);
			}
			else
			{
				MyPopupMode pUm= new MyPopupMode();
				pUm.setGraph2D(layview.getGraph2D());
				pUm.setEdgePropertyHandler(eph);
				pUm.setLVC(this);
				((MyEditModeLooseConstraint)oldMode).setPopupMode(pUm);
			}
			layview.updateNodeView();
			layview.updateEdgeView();
			layview.updateView();
			layview.setVisible(true);
			
			//TODO update crossings
			//			System.out.println(((Graph2DCanvas)layview.getCanvasComponent()).getZoom());
		}
		informMainControler();
	}

	//=======================================================
	//  Delete Page
	//=======================================================	

	private OptionHandler createDeletePageOptionHandler(){

		OptionHandler op = new OptionHandler("Delete Page");

		int pages =embeddedGraph.getNumPages();
		Color [] coldel = new Color [pages];

		for(int i=0;i<coldel.length;i++){

			coldel[i]=Utilities.COLORS[i];

		}

		JList<Color> list= new JList<Color>(coldel );	 	
		ListCellRenderer<Color> renderer =  new MyListCellRenderer<Color>();
		list.setCellRenderer(renderer);


		op. addEnum("Delete Page", coldel, coldel[0], renderer) ;	

		return op;

	}//createOptionHandler



	public void deletePageAction() {



		OptionHandler op = createDeletePageOptionHandler();
		if( op != null) {
			if( !op.showEditor() ){
				return;
			}
			if((op.getEnum(("Delete Page"))>0)){

				boolean delete =false; 
				int reply = JOptionPane.showConfirmDialog(layviewlist.get(0),
						"Do you want to delete this page? If it contains pages these pages are going to move to page 0 cyan ",
						"Delete Page",
						JOptionPane.YES_NO_OPTION);		

				if(reply== JOptionPane.YES_OPTION){
					delete=true;
				}				            
				if(delete){
					deletePage(op.getEnum(("Delete Page")));          	
				}

			}
		}


		System.out.println("  the user  delete  page   "+op.getEnum("Delete Page"));




		informMainControler(); //crossings

	}



	public void deletePage(int num) {

		embeddedGraph.deletePage(num);
		pages--;

		for(int i=0; i<layviewlist.size();i++){

			layviewlist.get(i).deletePage(num,pages);		
			layviewlist.get(i).updateEdgeView();
			layviewlist.get(i).setVisible(true);
		}

	}


	class MyListCellRenderer<T> implements ListCellRenderer<T> {

		protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();

		@Override
		public Component getListCellRendererComponent(JList list,
				Object value, int index, boolean isSelected,
				boolean cellHasFocus) {
			JLabel renderer=(JLabel) defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
			renderer.setText(Integer.toString(index));
			if(index>-1){
				renderer.setBackground(new Color(255,255,240));
				renderer.setForeground(Utilities.COLORS[index].darker());
				renderer.setText("  Page  "+ index +"   "+ Utilities.COLOR_NAMES[index]);
				renderer.setFont(Utilities.CUSTOM_FONT);
				renderer.setVerticalAlignment(SwingConstants.CENTER);
				LineBorder line= new LineBorder(Utilities.COLORS[index],3,true);
				renderer.setBorder(BorderFactory.createTitledBorder(line));				
			}
			return renderer;
		}//getListCellRendererComponent

	}//implements ListCellRenderer


	//=================================
	//          heuristic
	//=================================

	public void heuristic() {

		Heuristic heuristic= new Heuristic();
		heuristic.setEmbeddedGraph(embeddedGraph);
		EmbeddedGraph embeddedgraph=heuristic.run();
		this.embeddedGraph=embeddedgraph;
		mainControler.openEmbedded(embeddedGraph );
		informMainControler();

	}

	public  class MyGraph2DSelectionListener implements Graph2DSelectionListener
	{
		//		int count=0;

		@Override
		public void onGraph2DSelectionEvent(Graph2DSelectionEvent arg0)
		{
			// TODO Auto-generated method stub
			//		    System.out.println(count +" selection "+arg0.getGraph2D().toString());
			//		    System.out.println(count +" selection "+arg0.toString());
			if(arg0.isBendSelection())
				return;
			if(arg0.isEdgeSelection())
			{
//				System.out.println("edge selected");
				int index=0;
				MyGraph2DView layview= layviewlist.get(0);
				for(int i=0; i<layviewlist.size();i++){
					layview= layviewlist.get(i);	
					if(layview.getGraph2D()==arg0.getGraph2D())
					{
						index=i;

						break;
					}
				}
//				System.out.println(index);
				for(int i=0; i<layviewlist.size();i++){
					if(i!=index)
					{
					layview= layviewlist.get(i);
					if(!layview.getGraph2D().isSelectionEmpty())
					{
					layview.getGraph2D().unselectAll();
					layview.updateView();
					}
					}
				}
				return;
			}

//			System.out.println("node selected");
			int index=0;
			MyGraph2DView layview= layviewlist.get(0);
			for(int i=0; i<layviewlist.size();i++){
				layview= layviewlist.get(i);	
				if(layview.getGraph2D()==arg0.getGraph2D())
				{
					index=i;

					break;
				}
			}
//			System.out.println(index);
			for(int i=0; i<layviewlist.size();i++){
				if(i!=index)
				{
				layview= layviewlist.get(i);
				layview.getGraph2D().unselectAll();
				layview.updateView();
				}
			}
			//		    count++;
		}

	}


}
