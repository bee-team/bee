package bee.controlers;



import java.awt.Color;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import wrapper.BEWrapper;
import y.base.Graph;
import y.option.OptionHandler;
import y.util.D;
import y.view.Graph2D;
import y.view.Graph2DView;
import bee.demo.graphTemplate.EmbeddedGraph;
import bee.io.BEIOhandler;
import bee.option.options.Utilities;
import bee.undoRedo.ChangeManager;
import bee.view.BeeGui;

public class MainControler 
{
	private BeeGui gui;	
	private LVControler lvcontroler;	
	private ChangeManager changeManager;

	public MainControler() 
	{
		gui=null;
		lvcontroler=null;
		this.changeManager=new ChangeManager();
	}

	/**
	 * Accesses the manager for undo and redo actions
	 * @return the change-manager
	 */
	public ChangeManager getChangeManager()
	{
		return changeManager;
	}

	private void resetChangeManager()
	{
		changeManager.clear();		
		gui.disableRedo();
		gui.disableUndo();
	}


	//=================================================================	
	//SETUP
	//=================================================================
	/**
	 * Sets up the displaying frame with which the controler interacts 
	 * @param gui
	 */
	public void setGui(BeeGui gui) 
	{
		this.gui = gui;
		this.createLVControler(gui.getLVPanels(),gui.getViewOriginal().getGraph2D());
	}

	/*
	 * Creates the lay-view-controler that applies changes to the embedding views
	 * @param guiPanels the JPanels that contain the embedding views
	 * @param intGraph the initial graph associated with the embedding
	 */
	private void createLVControler(List<JPanel> guiPanels, Graph2D intGraph)
	{
		this.setLvcontroler(new LVControler( guiPanels, intGraph));
	}

	/*
	 *Sets up the view-controler with which the controler interacts for the changes 
	 * @param lvcontroler the lay-view-controler
	 */
	private void setLvcontroler(LVControler lvcontroler) 
	{
		this.lvcontroler = lvcontroler;	
		lvcontroler.setMainControler(this);
	}

	//=================================================================	
	//OPEN ACTION
	//=================================================================

	/**
	 * Opens the given Graph2D graph
	 * @param inputGraph the original graph
	 */
	public void openOriginal(Graph2D inputGraph)
	{
		//TODO open other extension
		lvcontroler.clearViews();
		gui.resetColorBars();

		resetChangeManager();
		System.out.println("MainControler: open original");
	}
	/**
	 * Opens the embedded graph of the file. Creates the embedded views
	 * @param name the file-name of the graph
	 */

	public void openEmbedded(String name)
	{
		//TODO maybe do the reading from the gui?
		//		if(name.endsWith(".be"))
		//		{
		
		try
		{
			//				System.out.println("reading from file...");
			EmbeddedGraph embeddedGraph=BEIOhandler.readFromFile(name);
			openEmbedded(embeddedGraph);

		} 
		catch (Exception e)
		{
			D.show(e);
			e.printStackTrace();
		}
		//		}
	}

	public void openEmbedded(EmbeddedGraph embeddedGraph){

		System.out.println("MainControler: open embedded");
		Graph2D graph=BEWrapper.createGraph2DFromEmbeddedGraph(embeddedGraph);
		lvcontroler.loadEmbedded(embeddedGraph, graph);
		gui.initializeBarsActiveColors(lvcontroler.getActivePages());
		resetChangeManager();
		gui.load(graph);
		updateCrossings();

	}

	//=================================================================	
	//SAVE ACTION
	//=================================================================
	/**
	 * Accesses the embedded graph as a Graph2D object
	 * @return the embedded graph
	 */
	public Graph2D getBEGraph()
	{
		return lvcontroler.getBEGraph();
	}

	/**
	 * Saves the embedded graph in a file with .be extension.
	 * @param fileName the name of the file
	 */
	public void saveEmbeddedGraph(String fileName)
	{
		System.out.println("MainControler: save embedded");
		BEIOhandler.writeToFile(fileName, lvcontroler.getEmbeddedGraph());
	}


	//=================================================================	
	//CREATE BOOK EMBEDDING ACTION
	//=================================================================	

	/**
	 * Creates the embedding of the input graph with the given number of pages and updates the embedding views
	 * @param graph2dview the original graph to embed
	 * @param numPages the number of pages
	 */
	public void createBEview(Graph2DView graph2dview, int numPages)
	{
		System.out.println("MainControler: createBE view");
		//		gui.setBarsActiveColors(Utilities.createActiveColors(numPages));
		//gui.setBarsActiveColors(Utilities.createActiveColors());
		lvcontroler.createBEview(graph2dview,  numPages);
		gui.initializeBarsActiveColors(lvcontroler.getActivePages());
		resetChangeManager();
		this.updateCrossings();
	}




	//=================================================================	
	//FIT CONTENT ACTION
	//=================================================================	
	/**
	 * Fits content of the embedding views of the graph
	 */
	public void fitContent()
	{
		System.out.println("MainControler: fit content");
		lvcontroler.fitContent();
	}


	//=================================================================	
	//ACTIVATE-DEACTIVATE PAGES
	//=================================================================	

	/**
	 * Shows the page with the given color of the selected embedding view, according to the selected state
	 * @param panel the JPanel containing the view to change
	 * @param color the color of the page
	 * @param state the new state of the page
	 */
	public void showPage(JPanel panel, Color color, int state)
	{
		System.out.println("MainControler: show page "+Utilities.COLOR_NAMES[Utilities.getColorIndex(color)]+" on drawing page "+state+" for panel "+panel.getName());
		//		System.out.println("show page-number "+index+" on drawing page "+page+" for panel "+panel.getName());
		lvcontroler.showPage(panel, color, state);
	}

	/**
	 * Hides the page with the given color of the selected embedding view
	 * @param panel the JPanel containing the view to change
	 * @param color the color of the page
	 */
	public void hidePage(JPanel panel, Color color)
	{
		System.out.println("MainControler: hide page "+Utilities.COLOR_NAMES[Utilities.getColorIndex(color)]+" for panel "+panel.getName());
		//		System.out.println("hide page-number "+index+" for panel "+panel.getName());
		lvcontroler.hidePage(panel, color);
	}



	//	public void activatePage(int [] [] activeColors) {
	//		gui.setBarsActiveColors(activeColors);
	//		
	//	}

	//	public void activatePage(Color color) {
	//		//		gui.setBarsActiveColors(activeColors);
	//		gui.activatePage(color);
	//
	//	}


	/**
	 * passes the delete page action to the view controler and informs the gui for the changes
	 */
	public void deletePageAction() 
	{
		System.out.println("MainControler: delete page action");
		Color color=lvcontroler.deletePageAction();
		if(color==null)
			return;
		gui.deletePage(color);
		this.updateCrossings();
	}
	

	public void undoCreatePage(Color color)
	{
		gui.deletePage(color);
		this.updateCrossings();
	}
	/**
	 * passes the create page action to the view controler and informs the gui for the changes
	 */
	public void createPageAction()
	{
		System.out.println("MainControler: create page action");
		Color color=lvcontroler.createPageAction();
		if(color==null)
			return;
		createPage(color);
	}

	public void createPage(Color color)
	{
		System.out.println("MainControler: create page "+Utilities.COLOR_NAMES[Utilities.getColorIndex(color)]);
		gui.createPage(color);
		this.updateCrossings();
	}


	/**
	 * informs the gui that crossings have changed
	 */
	public void updateCrossings()
	{
		System.out.println("MainControler: update crossings ");
		Map<Color,Integer>	crossingsMap=lvcontroler.updateCrossings();
		gui.upadateCrossingInfo(crossingsMap);
	}


	//=======================================================
	//        Heuristic algo
	//=======================================================	

	public void applyHeuristicAlgo() {
		System.out.println("MainControler: apply heuristic");
		lvcontroler.applyHeuristicAlgo();
		updateCrossings();

	}

	//=======================================================
	//        UNDO-REDO actions
	//=======================================================	
	/**
	 * Redo action
	 */
	public void redo()
	{
		System.out.println("MainControler: redo");
		this.changeManager.redo();
		gui.enableUndo();
		if(!changeManager.canRedo())
			gui.disableRedo();
	}

	/*
	 * Undo action
	 */
	public void undo()
	{
		System.out.println("MainControler: undo");
		this.changeManager.undo();
		gui.enableRedo();
		if(!changeManager.canUndo())
			gui.disableUndo();
	}

	public void enableUndo() 
	{
		System.out.println("MainControler: enable undo");
		gui.enableUndo();
	}	

	//TODO implement it
	public void openInGraph2DViews(LinkedList<Graph2D> graphList,boolean firstTime) 
	{

	}

	
}

