package bee.controlers;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;

import y.base.Edge;
import y.base.EdgeCursor;
import y.base.Node;
import y.option.OptionHandler;
import y.view.EdgeRealizer;
import y.view.EditMode;
import y.view.Graph2D;
import y.view.Graph2DCanvas;
import y.view.Graph2DSelectionEvent;
import y.view.Graph2DSelectionListener;
import y.view.Graph2DView;
import y.view.ViewMode;
import bee.algo.Crossings;
import bee.algo.Heuristic;
import bee.demo.graphTemplate.EmbeddedGraph;
import bee.graph.Page;
import bee.layout.MySimpleLayouter;
import bee.option.EdgePropertyHandler;
import bee.option.MyEditModeLooseConstraint;
import bee.option.MyListCellRenderer;
import bee.option.options.Utilities;
import bee.undoRedo.Changeable;
import bee.undoRedo.CreatePageChange;
import bee.undoRedo.EdgeMoveChange;
import bee.view.MyGraph2DView;
import bee.view.MyMoveSelectionMode;
import bee.view.MyPopupMode;
import bee.view.MyZoomWheelListener;

public class LVControler {


	private EmbeddedGraph embeddedGraph;
	private MainControler mainControler;
	private Crossings crossinghecker;
	private List<MyGraph2DView> layviewlist;
	private int pages;

	public LVControler(List<JPanel> panels, Graph2D intGraph) 
	{
		this.pages=0;
		crossinghecker= new Crossings();	
		embeddedGraph=new EmbeddedGraph((Graph2D) intGraph);
		layviewlist= new ArrayList<MyGraph2DView>();
		for(int i=0; i<panels.size();i++)
		{
			MyGraph2DView layview= new MyGraph2DView();
			layviewlist.add(layview);
			layview.getGraph2D().clear();
			layview.getCanvasComponent().setFont(new Font("Serif", Font.PLAIN, 12));
			layview.getCanvasComponent().setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
			layview.getCanvasComponent().setAlignmentY(Component.TOP_ALIGNMENT);
			layview.getCanvasComponent().setAlignmentX(Component.LEFT_ALIGNMENT);
			layview.setBounds(340, 5, 1250 , 250);
			layview.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.LOWERED, null, null, new Color(191, 205, 219), null), (Border) new LineBorder((new Color(227, 227, 227)))));
			layview.setPreferredSize(new Dimension(1250,220));
			MyGraph2DSelectionListener listener=new MyGraph2DSelectionListener();
			//			System.out.println(listener);
			layview.getGraph2D().addGraph2DSelectionListener(listener);
			//			layviewlist.add(layview);
			panels.get(i).add(layview,BorderLayout.CENTER);
		}//for
	}


	public void setMainControler(MainControler mainControler) 
	{
		this.mainControler=mainControler;
	}

	public void informMainControler()
	{
		System.out.println("LVControler: update crossings ");
		mainControler.updateCrossings();
	}
	//=================================================================	
	//		GETTERS-SETTERS
	//=================================================================	

	/**
	 * Accesses the embedded graph
	 * @return the embedded graph
	 */
	public EmbeddedGraph getEmbeddedGraph()
	{
		return embeddedGraph;
	}

	/**
	 * Accesses the number of pages of the embedding.
	 * @return the number of pages
	 */
	public int getPages()
	{
		return pages;
	}

	/**
	 * Accesses the embedding graph as a Graph2D object
	 * @return the embedding graph
	 */
	public Graph2D getBEGraph()
	{
		return layviewlist.get(0).getGraph2D();
	}

	//	/**
	//	 * Accesses the embedding views as graphs
	//	 * @return the views
	//	 */
	//	public List<MyGraph2DView> getLayviewlist() 
	//	{
	//		return layviewlist;
	//	}

	//=================================================================	
	//OPEN-RESET ACTION
	//=================================================================		

	/**
	 * resets the embedding views
	 */
	public void clearViews()
	{
		System.out.println("LVControler: clear views");
		this.pages=0;
		//		embeddedGraph.getGraph2D().clear();
		embeddedGraph.clear();		
		//embeddedGraph.setGraph2D((Graph2D) intGraph.createCopy());
		for(int i=0;i<layviewlist.size();i++)
		{
			layviewlist.get(i).getGraph2D().clear();
			layviewlist.get(i).updateView();
			//layviewlist.get(i).setGraph2D((Graph2D)intGraph.createCopy());
		}
		this.fitContent();
	}

	/**
	 * Shows the embedding views of the input graph based on the embedded graph.
	 * @param embeddedGraph the embedded graph
	 * @param graphView the initial graph
	 */
	public void loadEmbedded(EmbeddedGraph embeddedGraph, Graph2D graphView)
	{
		System.out.println("LVControler: load embedded");
		this.embeddedGraph=embeddedGraph;
		this.pages=embeddedGraph.getNumPages();

		double height=layviewlist.get(0).getCanvasSize().height;
		if(layviewlist.get(0).getGraph2D().nodeCount()>0)
		{
			Node v=layviewlist.get(0).getGraph2D().firstNode();
			height=2*layviewlist.get(0).getGraph2D().getRealizer(v).getCenterY();
			System.out.println(height);
		}
		MySimpleLayouter layouter= new  MySimpleLayouter(100,layviewlist.get(0).getCanvasComponent().getWidth(),height);
		layouter.setNumberOfPages(pages);

		Graph2D tempGraph=layouter.doNodeLayoutCore((Graph2D) graphView.createCopy(), embeddedGraph.getNodeOrder());	
		//		Graph2D tempGraph=layouter.doLayoutCore((Graph2D)graph2dview.getGraph2D().createCopy());		
		//		embeddedGraph.load(tempGraph, true);
		double minDist=layouter.getMinimalNodeDistance();
		double offset=layouter.getOffset();
		height=layouter.getHeight();
		initializeLayviewGraphs(tempGraph, pages, minDist, height, offset);
	}
	//=================================================================	
	//              CREATE BOOK EMBEDDING ACTION
	//=================================================================		


	/**
	 * Creates the book embedding of the input graph with the given number of pages. Also, creates the embedding views
	 * @param graph2dview the input graph
	 * @param numPages the number of pages
	 */
	public void createBEview(Graph2DView graph2dview, int numPages)
	{
		System.out.println("LVControler: create BE view");
		pages=numPages;
		double height=layviewlist.get(0).getCanvasSize().height;
		if(layviewlist.get(0).getGraph2D().nodeCount()>0)
		{
			Node v=layviewlist.get(0).getGraph2D().firstNode();
			height=2*layviewlist.get(0).getGraph2D().getRealizer(v).getCenterY();
			System.out.println(height);
		}
		//		((Graph2DCanvas)layviewlist.get(0).getCanvasComponent()).setCenter(layviewlist.get(0).getCanvasComponent().getWidth()/2, height/2);
		MySimpleLayouter layouter= new  MySimpleLayouter(100,layviewlist.get(0).getCanvasComponent().getWidth(),height);
		layouter.setNumberOfPages(pages);
		Graph2D tempGraph=layouter.doLayoutCore((Graph2D)graph2dview.getGraph2D().createCopy());		
		embeddedGraph.load(tempGraph, true);
		double minDist=layouter.getMinimalNodeDistance();
		double offset=layouter.getOffset();
		height=layouter.getHeight();
		initializeLayviewGraphs(tempGraph, numPages, minDist, height, offset);
	}
	/*
	 * Displays an embedded Graph2D object based on the given parameters.
	 * @param tempEmbeddedGraph the given graph to display
	 * @param noOfPages the number of pages
	 * @param minDist the node distance
	 * @param height the height of the embedding
	 * @param offset the horizontal 
	 */
	private void initializeLayviewGraphs(Graph2D tempEmbeddedGraph, int noOfPages, double minDist, double height, double offset)
	{
		System.out.println("LVControler: createBE view (continued)");
		MyZoomWheelListener mwzl =   new MyZoomWheelListener();
		mwzl.setLVControler(this);
		EdgePropertyHandler eph= new EdgePropertyHandler(this);	
//		eph.createHandler();
		int[][]activeColors= Utilities.createActiveColors(noOfPages);
		for(int i=0; i<layviewlist.size();i++){
			MyGraph2DView layview= layviewlist.get(i);	
			layview.getGraph2D().clear();
			layview.fitContent();
			layview.updateView();
			layview.setGraph2D((Graph2D)tempEmbeddedGraph.createCopy(), offset, minDist);	
			layview.getGraph2D().addGraph2DSelectionListener(new MyGraph2DSelectionListener());
			layview.initializeActiveColors(activeColors[i]);
			layview.setFitContentOnResize(true);
			
			layview.updateNodeView();
			layview.updateEdgeView();
			layview.updateView();
			layview.setVisible(true);
		}
		eph.createHandler();
		for(int i=0; i<layviewlist.size();i++){
			MyGraph2DView layview= layviewlist.get(i);	
			
			ViewMode oldMode=layview.getViewControl().getCurrentMode();
			if(!(oldMode instanceof MyEditModeLooseConstraint))
			{
				layview.getViewControl().remove(oldMode);
				//				System.out.println("create new mode");
				EditMode  editMode = new MyEditModeLooseConstraint(); 
				MyMoveSelectionMode	msm=new MyMoveSelectionMode(offset, height/2, minDist);
				msm.setLVControler(this);		
				editMode.setMoveSelectionMode(msm);
				MyPopupMode pUm= new MyPopupMode();
				pUm.setGraph2D(layview.getGraph2D());
				pUm.setEdgePropertyHandler(eph);
				pUm.setLVC(this);
				editMode.setPopupMode(pUm);
				layview.addViewMode(editMode); 
				layview.getViewControl().setMode(editMode);
				layview.getCanvasComponent().addMouseWheelListener(mwzl);
			}
			else
			{
				MyPopupMode pUm= new MyPopupMode();
				pUm.setGraph2D(layview.getGraph2D());
				pUm.setEdgePropertyHandler(eph);
				pUm.setLVC(this);
				((MyEditModeLooseConstraint)oldMode).setPopupMode(pUm);
			}
		}
		//		informMainControler();
	}

	//=================================================================	
	//FIT CONTENT ACTION
	//=================================================================	

	/**
	 * Resets the zoom level of the embedding views and fits contents
	 */
	public void fitContent()
	{
		System.out.println("LVControler: fit content");
		for(int i=0;i<layviewlist.size();i++)
		{
			//TODO perhaps edges should be redrawn
			layviewlist.get(i).setZoom(1);
			layviewlist.get(i).updateNodeView();
			layviewlist.get(i).updateView();
		}
	}

	//======================================
	//               Zoom
	//======================================
	/**
	 * Changes the zoom and the view-center of the embedding views
	 * @param zoom the new zoom value
	 * @param p the center of the view
	 */
	public void setZoom(double zoom, Point2D p)
	{
		System.out.println("LVControler: set zoom");
		for(int i=0;i<this.layviewlist.size();i++)
		{
			Graph2DView graph=layviewlist.get(i);
			Graph2DCanvas canvas=(Graph2DCanvas)graph.getCanvasComponent();
			canvas.setZoom(zoom);
			canvas.setCenter(p.getX(), p.getY());
			graph.updateView();
		}
	}

	//=================================================================	
	// Inform the main controler about an undoable change 
	//=================================================================	

	/**
	 * Informs the main controler about an undoable change 
	 * @param change the unboable action
	 */
	public void addChangeable(Changeable change)
	{
		System.out.println("LVControler: add changeable");
		this.mainControler.enableUndo();
		this.mainControler.getChangeManager().addChangeable(change);
	}


	//======================================
	//               Crossing
	//======================================

	//	public void createCrossings(){
	//		//Crossings
	//		Crossings crossinghecker= new Crossings();
	//		//crossinghecker.setPages(pages);
	//	}

	/**
	 * Calculates the crossings for each page based on the embedded graph. Returns a map with Color item as keys and Integer item as value
	 * @return the map-encoding of the crossings
	 */
	public Map<Color, Integer> updateCrossings() {
		System.out.println("LVControler: create crossings map");
		System.out.println(Arrays.toString(getActivePages()[0].toArray()));
		Map<Color,Integer> crossingsMap= crossinghecker.checkCrossings(embeddedGraph, this.getActivePages()[0]);
		return crossingsMap;
	}
	//=================================================================	
	// MOVE ELEMENTS
	//=================================================================		

	public void moveEdges(List<int []> edgeIndex, Color toColor)
	{
		System.out.println("LVControler: move edges to page "+Utilities.COLOR_NAMES[Utilities.getColorIndex(toColor)]);
		//		if(toPage>=this.pages)
		//			pages++;
		if(!layviewlist.get(0).isActivePage(toColor))
		{
			createPage(toColor, false);
//			mainControler.createPage(toColor);
		}
		int index=0;
		List<Page> activePages=layviewlist.get(0).getPages();
		for(int i=0;i<activePages.size();i++)
		{
			if(activePages.get(i).getColor().equals(toColor))
			{
				index=i;
				break;
			}
		}
		System.out.println("move edges to page with index "+index);
//		embeddedGraph.moveEdges(edgeIndex, Utilities.getColorIndex(toColor));
		embeddedGraph.moveEdges(edgeIndex, index);
		//		LinkedList<Page>[] activePages = new LinkedList<Page>[layviewlist.size()];
		for(int i=0; i<layviewlist.size();i++)
		{
			layviewlist.get(i).moveEdges(edgeIndex, toColor);
			//			System.out.println("layviewlist.get(i).activatePage(toPage)");
			//			layviewlist.get(i).activatePage(toColor);
			//			activePages[i]= layviewlist.get(i).getPages();

			layviewlist.get(i).updateEdgeView();
			layviewlist.get(i).setVisible(true);
		}
		//		informMainControlerToActivatePage(toColor);
		//		infoMainControlerToActivatePage(activePages);
		System.out.println("               Number of pages "+this.pages);
		this.informMainControler();
	}


	/**
	 * Move the nodes corresponding to the indices of the nodeIndex array by distIndex positions
	 * @param nodeIndex the indices of the nodes to move
	 * @param distIndex the node-distance of the move 
	 */
	public void moveNodes(List<Integer> nodeIndex, int distIndex)
	{
		System.out.println("LVControler: move nodes");
		embeddedGraph.moveNodes(nodeIndex, distIndex);
		for(int s=0;s<layviewlist.size();s++)
		{
			layviewlist.get(s).moveNodes(nodeIndex, distIndex);
			//			layview.reverseEdges(node2);
			layviewlist.get(s).updateNodeView();
			layviewlist.get(s).updateEdgeView();
			layviewlist.get(s).setVisible(true);
			layviewlist.get(s).updateView();
		}
		this.informMainControler();
	}//moveNodes

	//=================================================================	
	//ACTIVATE-DEACTIVATE PAGES
	//=================================================================	

	/**
	 * Creates an array of the active pages of all embedding views. Each row of the array corresponds to an embedding view, 
	 * and contains all available pages.
	 * 
	 * @return the active pages
	 */

	public List<Page>[] getActivePages()
	{
		System.out.println("LVControler: generate active pages");
		List<?>[] result=new LinkedList<?>[layviewlist.size()];
		for(int i=0;i<layviewlist.size();i++)
			result[i]=layviewlist.get(i).getPages();
		return (List<Page>[])result;
	}
	/**
	 * Hides the page with the selected color from the specific embedding view 
	 * @param panel the panel that contains the embedding view
	 * @param color the color of the page to hide
	 */
	public void hidePage(JPanel panel, Color color)
	{
		System.out.println("LVControler: hide page "+Utilities.COLOR_NAMES[Utilities.getColorIndex(color)]+" for panel "+panel.getName());
		BorderLayout layout = (BorderLayout) panel.getLayout();
		MyGraph2DView graph=(MyGraph2DView)layout.getLayoutComponent(BorderLayout.CENTER);
		graph.hidePage(color);
	}

	/**
	 * Shows the page with the selected color at the specific embedding view with respect to the given state. 
	 * @param panel the panel that contains the embedding view
	 * @param color the color of the page to show
	 * @param state the state of the page to show
	 */
	public void showPage(JPanel panel, Color color, int state)
	{
		System.out.println("LVControler: show page "+Utilities.COLOR_NAMES[Utilities.getColorIndex(color)]+" on page "+state+" for panel "+panel.getName());
		BorderLayout layout = (BorderLayout) panel.getLayout();
		MyGraph2DView graph=(MyGraph2DView)layout.getLayoutComponent(BorderLayout.CENTER);
		graph.showPage(color, state);
	}


	//=======================================================
	//  Delete Page
	//=======================================================	


	/**
	 * Asks the user which page to delete
	 * @return the color of the page to delete
	 */
	public Color deletePageAction() 
	{
		//TODO allow the user to specify which page they go to
		System.out.println("LVControler: delete page action");

		OptionHandler op = createDeletePageOptionHandler();

		Color colorToDelete=null;
		Color colorToMove=null;
		if(op==null)
		{
			return colorToDelete;
		}
		if( !op.showEditor())
		{
			return colorToDelete;
		}
		if((op.getEnum("Delete Page")>-1)&& op.getEnum("Move to Page")>-1)
		{
			boolean delete =false; 
			int reply = JOptionPane.showConfirmDialog(layviewlist.get(0),
					"Are you sure you want to delete this page?",
					"Delete Page",
					JOptionPane.YES_NO_OPTION);	
			if(reply== JOptionPane.YES_OPTION)
			{
				delete=true;
			}				            
			if(delete)
			{
				System.out.println("deleting");
				//				String selection=op.getString("Delete Page");
				//				selection=selection.substring(selection.lastIndexOf(" ")+1);
				//					deletePage(op.getEnum(("Delete Page")));
				//				colorToDelete=Color.getColor(selection);
				//				colorToDelete=Utilities.getColorFromName(selection);

				colorToDelete=Utilities.COLORS[Utilities.getColorIndex((Color)op.get("Delete Page"))];
				colorToMove=Utilities.COLORS[Utilities.getColorIndex((Color)op.get("Move to Page"))];
				System.out.println(colorToDelete);
				System.out.println(colorToMove);
				deletePage(colorToDelete, colorToMove);
				//				return colorToDelete;
			}
		}
		System.out.println("                         the user  deleted  page   "+
				op.getEnum("Delete Page") +"("+Utilities.COLOR_NAMES[Utilities.getColorIndex((Color)op.get("Delete Page"))]+")" 
				+" moving to "+"("+Utilities.COLOR_NAMES[Utilities.getColorIndex(colorToMove)]+")");
		//		informMainControler(); //crossings
		return colorToDelete;
	}


	private OptionHandler createDeletePageOptionHandler()
	{
		OptionHandler op = new OptionHandler("Delete Page");
		int noOfPages =embeddedGraph.getNumPages();		
		List<Page> pages=layviewlist.get(0).getPages();		
		Color [] coldel = new Color [noOfPages];
		for(int i=0;i<coldel.length;i++)
		{
			coldel[i]=pages.get(i).getColor();
		}
		JList<Color> list= new JList<Color>(coldel);	 	
		ListCellRenderer<Color> renderer =  new MyListCellRenderer<Color>();
		list.setCellRenderer(renderer);
		op. addEnum("Delete Page", coldel, coldel[0], renderer) ;	

		op.addEnum("Move to Page", coldel, coldel[0], renderer);
		return op;
	}//createOptionHandler

	//	private class MyListCellRenderer<T> implements ListCellRenderer<T> 
	//	{
	//
	//		protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();
	//
	//		@Override
	//		public Component getListCellRendererComponent(JList list,
	//				Object value, int index, boolean isSelected, boolean cellHasFocus) 
	//		{
	//			JLabel renderer=(JLabel) defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
	//			renderer.setText(Integer.toString(index));
	//			if(index>-1){
	//				renderer.setBackground(new Color(255,255,240));
	//				//				Color color=Utilities.COLORS[index];
	//				Color color=(Color)value;
	//				renderer.setForeground(color.darker());
	//				renderer.setText("  Page  "+ index +"   "+ Utilities.COLOR_NAMES[Utilities.getColorIndex(color)]);
	//				renderer.setFont(Utilities.CUSTOM_FONT);
	//				renderer.setVerticalAlignment(SwingConstants.CENTER);
	//				LineBorder line= new LineBorder(Utilities.COLORS[index],3,true);
	//				renderer.setBorder(BorderFactory.createTitledBorder(line));				
	//			}
	//			return renderer;
	//		}//getListCellRendererComponent
	//
	//	}//implements ListCellRenderer
	/**
	 * Deletes the page with the specified color and moves all the edges to another page
	 * @param fromColor the color of the page to delete
	 * @param toColor the color of the page to move the edges
	 */
	public void deletePage(Color fromColor, Color toColor) 
	{
		System.out.println("LVControler: delete page "+Utilities.COLOR_NAMES[Utilities.getColorIndex(fromColor)]);
		int fromIndex=Utilities.getColorIndex(fromColor);
		int toIndex=Utilities.getColorIndex(toColor);
		boolean newPage=false;
		List<int[]> edgeIndex=new ArrayList<int []>();
		Graph2D graph=layviewlist.get(0).getGraph2D();
		EdgeCursor ec = graph.edges();
		List<Color> previousPagesList=new ArrayList<Color>();
		
		for (ec.toFirst(); ec.ok(); ec.next())
		{
			Edge edge = ec.edge();
			EdgeRealizer er =graph.getRealizer(edge);
			if(er.getLineColor().equals(fromColor))
			{
				int [] current=new int[2];
				current[0]=edge.source().index();
				current[1]=edge.target().index();
				edgeIndex.add(current);
				previousPagesList.add(fromColor);
			}
			
			//			if (!getItem("Move to Page").isValueUndefined())
			//				er.setLineColor((Color)get("Move to Page"));			
			//			newColor=(Color)get("Move to Page");			 
			//			num= getEnum("Move to Page");
			//			num=Utilities.getColorIndex(newColor);
			//			lvcontroler.moveEdges(edgeIndex,num);
			//			lvcontroler.moveEdges(edgeIndex,newColor);
		}//for each edge
		Color[] previousPages=new Color[previousPagesList.size()];
		for(int i=0;i<previousPages.length;i++)
			previousPages[i]=previousPagesList.get(i);
		new EdgeMoveChange(edgeIndex, toColor, previousPages, this, false);

		embeddedGraph.deletePage(fromIndex, toIndex);
		pages--;
		for(int i=0; i<layviewlist.size();i++)
		{
			layviewlist.get(i).deletePage(fromColor,toColor);		
			layviewlist.get(i).updateEdgeView();
			layviewlist.get(i).setVisible(true);
		}
	}


	/**
	 * Deletes the page with the given color
	 * @param fromColor the color of the page to delete
	 */
	//	public void deletePage(Color fromColor) 
	//	{
	//		Color toColor=layviewlist.get(0).getPages().get(0).getColor();
	//		deletePage(fromColor, toColor);
	//	}

	public void undoCreatePage(Color fromColor) 
	{
		Color toColor=layviewlist.get(0).getPages().get(0).getColor();
		undoCreatePage(fromColor, toColor);
	}

	public void undoCreatePage(Color fromColor, Color toColor) 
	{
		System.out.println("LVControler: delete page (undo)"+Utilities.COLOR_NAMES[Utilities.getColorIndex(fromColor)]);
		int fromIndex=Utilities.getColorIndex(fromColor);
		int toIndex=Utilities.getColorIndex(toColor);
		embeddedGraph.deletePage(fromIndex, toIndex);
		pages--;
		for(int i=0; i<layviewlist.size();i++)
		{
			layviewlist.get(i).deletePage(fromColor,toColor);		
			layviewlist.get(i).updateEdgeView();
			layviewlist.get(i).setVisible(true);
		}
		mainControler.undoCreatePage(fromColor);
	}

	//=======================================================
	//  Create Page
	//=======================================================	


	/**
	 * Create a new page
	 * @return the color of the new page
	 */
	public Color createPageAction() 
	{
		System.out.println("LVControler: create page action ");
		int noOfPages =embeddedGraph.getNumPages();	
		if(noOfPages==Utilities.TOTAL_PAGES)
		{
			System.out.println("Can't create new page in LVControler");
			return null;
		}
		List<Page> pages=layviewlist.get(0).getPages();		
		Color color=pages.get(noOfPages).getColor();
		createPage(color, true);
		System.out.println("  								the user  created  page   "+Utilities.COLOR_NAMES[Utilities.getColorIndex(color)]);
		//			informMainControler(); //crossings
		return color;
	}

	/**
	 * Creates the page with the specified color
	 * @param color the color of the new page
	 */
	public void createPage(Color color, boolean undoable) 
	{
		System.out.println("LVControler: create page "+Utilities.COLOR_NAMES[Utilities.getColorIndex(color)]);
		pages++;
		for(int i=0; i<layviewlist.size();i++)
		{
			layviewlist.get(i).activatePage(color);
			layviewlist.get(i).updateEdgeView();
			layviewlist.get(i).setVisible(true);
		}
		this.embeddedGraph.moveEdges(null, pages);
		if(undoable)
			new CreatePageChange(color, this);
		else
		{
			mainControler.createPage(color);
		}
	}





	//=================================
	//          heuristic
	//=================================

	public void applyHeuristicAlgo() {

		System.out.println("LVControler: apply heuristic");
		Heuristic heuristic= new Heuristic();
		heuristic.setEmbeddedGraph(embeddedGraph);
		EmbeddedGraph newEmbeddedgraph=heuristic.run();
		this.embeddedGraph=newEmbeddedgraph;
		mainControler.openEmbedded(embeddedGraph);
		//		informMainControler();

	}

	private class MyGraph2DSelectionListener implements Graph2DSelectionListener
	{
		@Override
		public void onGraph2DSelectionEvent(Graph2DSelectionEvent arg0)
		{
			if(arg0.isBendSelection())
				return;
			if(arg0.isEdgeSelection())
			{
				int index=0;
				MyGraph2DView layview= layviewlist.get(0);
				for(int i=0; i<layviewlist.size();i++){
					layview= layviewlist.get(i);	
					if(layview.getGraph2D()==arg0.getGraph2D())
					{
						index=i;
						break;
					}
				}
				for(int i=0; i<layviewlist.size();i++){
					if(i!=index)
					{
						layview= layviewlist.get(i);
						if(!layview.getGraph2D().isSelectionEmpty())
						{
							layview.getGraph2D().unselectAll();
							layview.updateView();
						}
					}
				}
				return;
			}
			int index=0;
			MyGraph2DView layview= layviewlist.get(0);
			for(int i=0; i<layviewlist.size();i++){
				layview= layviewlist.get(i);	
				if(layview.getGraph2D()==arg0.getGraph2D())
				{
					index=i;
					break;
				}
			}
			//			System.out.println(index);
			for(int i=0; i<layviewlist.size();i++){
				if(i!=index)
				{
					layview= layviewlist.get(i);
					layview.getGraph2D().unselectAll();
					layview.updateView();
				}
			}
		}
	}



	//	public void open(LinkedList<Graph2D> graphList,	boolean firstTime)
	//	{
	//
	//		//		Graph2D graph=new Graph2D();
	//		//		
	//		//		for(int i=0;i<graphList.size();i++){
	//		//			//for(int j=0;j<
	//		//			Graph2D g=	graphList.get(i);
	//		//			
	//		//		}
	//
	//
	//		//	embeddedGraph.clear();
	//
	//		for(int i=0;i<layviewlist.size();i++)
	//		{
	//			layviewlist.get(i).getGraph2D().clear();
	//
	//
	//			layviewlist.get(i).updateView();
	//
	//		}
	//	}

}
