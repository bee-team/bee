package bee.view;


import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;

import y.base.Edge;
import y.base.Node;
import y.util.D;
import y.view.Graph2D;
import y.view.PopupMode;
import bee.controlers.LVControler;
import bee.demo.graphTemplate.EmbeddedGraph;
import bee.option.EdgePropertyHandler;



public class MyPopupMode extends PopupMode {
	/*
	 * To activate the popup menus right click  on a node
	 * 
	 * it shows node's info
	 * 
	 * with a right click on an edge, you can change its color
	 * 
	 */

	LVControler lvcontroler;
	
	public MyPopupMode(){
		super();
	}//constr

	/** Popup menu for a  node */
	public JPopupMenu getNodePopup(Node v)
	{
		JPopupMenu pm = new JPopupMenu();
		pm.add(new ShowNodeInfo(v));

		return pm;
	}

	public JPopupMenu getEdgePopup(Edge e){
		JPopupMenu pm = new JPopupMenu();
		if(e==null)
			System.out.println("error");
		pm.add(new ShowEdgeOptions(e));
//		pm.addSeparator();
//		pm.add(new ShowDeleteOptions());

		return pm;
	}

	public JPopupMenu getSelectionPopup(double x, double y){
		JPopupMenu pm = new JPopupMenu();
		pm.add(new ShowSelectedEdgesOptions());
//		pm.addSeparator();
//		pm.add(new ShowDeleteOptions());
		return pm;	
	}

	

	/** 
	 * Action that displays and information dialog for a node. 
	 */
	class ShowNodeInfo extends AbstractAction
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		Node v;

		ShowNodeInfo(Node v)
		{
			super("Node Info");
			this.v = v;
		}

		public void actionPerformed(ActionEvent e)
		{
			//    String vtext = view.getGraph2D().getLabelText(v);
			JOptionPane.showMessageDialog(view,
					"Label text of node is " + 
							view.getGraph2D().getLabelText(v));
		}
	}// class ShowNodeInfo

	/*
	 * 
	 */
	EdgePropertyHandler edgeHandler;
	Graph2D graph;



	class ShowEdgeOptions extends AbstractAction
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		//Edge edge;


		ShowEdgeOptions(Edge edge)
		{
			super("Edge Options");
		}


		public void actionPerformed(ActionEvent e)
		{
			try{
				EmbeddedGraph eg=lvcontroler.getEmbeddedGraph();
				if(eg==null)
					System.out.println("error");
				int pages =eg.getNumPages();
				System.out.println("edge handler pages "+pages);
//				edgeHandler.setPages(pages);
				edgeHandler.createHandler();
				
				edgeHandler.updateValuesFromSelection(graph);

				if(edgeHandler.showEditor(view.getFrame())) {
					edgeHandler.commitEdgeProperties(graph);
					graph.updateViews();

				}

			}
			catch(NullPointerException exc){
				D.show(exc);
				exc.printStackTrace();
			}
		}
	}


	public class ShowSelectedEdgesOptions extends AbstractAction
	{
		
		private static final long serialVersionUID = 1L;
		//EdgeList edgeList;


		ShowSelectedEdgesOptions()
		{
			super("Edge Options");

		}

		public void actionPerformed(ActionEvent e)
		{
			try{

				int pages =lvcontroler.getEmbeddedGraph().getNumPages();
//				edgeHandler.setPages(pages);
				edgeHandler.createHandler();
				
				edgeHandler.updateValuesFromSelection(graph);
				if(edgeHandler.showEditor(view.getFrame())) {	 

					edgeHandler.commitEdgeProperties(graph);

					graph.updateViews();
				}

			}
			catch(NullPointerException exc){
				D.show(exc);
			}
		}
	}

	
	/*
	 * delete page handler

DeletePageHandler	deletepageHandler;



class ShowDeleteOptions extends AbstractAction
{
	
	private static final long serialVersionUID = 1L;
	


	ShowDeleteOptions()
	{
		super("Delete Page");

	}

	public void actionPerformed(ActionEvent e)
	{
		try{
			
			if(deletepageHandler.showEditor(view.getFrame())) {	 

				deletepageHandler.commitProperties();
			}

		}
		catch(NullPointerException exc){
			D.show(exc);
		}
	}
}

	
	//public EdgePropertyHandler createEdgePropertyHandler(){
	//	EdgePropertyHandler edgeHandler = new EdgePropertyHandler();
	//	return edgeHandler;
	//}
	
	public void setDeletePageHandler(DeletePageHandler dph){
		deletepageHandler=dph;
	}

	

	 */
	
	
	
	public void setEdgePropertyHandler(EdgePropertyHandler eph){
		edgeHandler=eph;
	}

	public EdgePropertyHandler getEdgePropertyHandler(){
		return edgeHandler;
	}

	public void setGraph2D(Graph2D graph2d){
		graph=graph2d;
	}

	
	public void setLVC(LVControler lvc){
		lvcontroler=lvc;
	}
	
}//class MyPopupMode
