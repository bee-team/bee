package bee.view;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;





//import bee.view.ToolBarsPanel.ColorSelectionAction;
import y.algo.GraphChecker;
import y.algo.GraphConnectivity;
import y.base.Graph;
import y.base.Node;
import y.base.NodeCursor;
import y.base.NodeList;
import y.io.GMLIOHandler;
import y.layout.circular.CircularLayouter;
import y.layout.organic.OrganicLayouter;
import y.layout.orthogonal.OrthogonalLayouter;
import y.option.OptionHandler;
import y.util.D;
import y.view.EditMode;
import y.view.Graph2D;
import y.view.Graph2DSelectionEvent;
import y.view.Graph2DSelectionListener;
import y.view.Graph2DView;
import y.view.Graph2DViewMouseWheelZoomListener;
import y.view.LineType;
import y.view.NodeRealizer;
import bee.controlers.MainControler;
import bee.graph.Page;
import bee.graph.info.OriginalViewInfo;
import bee.option.MyEditModeConstraint;
import bee.option.PageColorRenderer;
import bee.option.options.Utilities;
import bee.planar.PeelingInfo;



public class BeeGui extends JFrame {

	private static final long serialVersionUID = 1L;

	//private JPanel contentPane;
	private MainControler mainControler;
	protected Graph2DView viewOriginal;
	private ArrayList<JPanel> lvPanels;
	private OriginalViewInfo graphInfo=new OriginalViewInfo();
	//Map<Node, Integer>conNodeMap =new HashMap<Node,Integer>();
	//LinkedList<Graph2D> graphList= new LinkedList<Graph2D> ();

	JTabbedPane infoPane;
	JPanel crossingspanel;

	JMenuItem undo;
	JMenuItem redo;
	//Graph2DUndoManager undo;

	/**
	 * Create the frame.
	 */
	public BeeGui() {
		initGui();
	}//constr

	/**
	 * Setup the main controler for the GUI, that is responsible for the functionality. 
	 * @param mainControler the main controler of the GUI
	 */
	public void setMainControler(MainControler mainControler)
	{
		this.mainControler=mainControler;
	}

	//=================================================================	
	//CREATE GUI
	//=================================================================
	/*
	 * Builds the application GUI
	 */
	private void initGui()
	{
		//TODO fix problems with the dimensions
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(BeeGui.class.getResource("/javaguiresources/250px-Petersen_double_cover.svg.png")));
		this.setForeground(Color.PINK);
		this.setTitle("Bee 1.2");
		this.setFont(Utilities.CUSTOM_FONT);
		this.setBounds(0, 0, 1300, 602);

		this.buildMainMenu();

		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);

		this.buildToolBars(contentPane);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.insets = new Insets(0, 0, 0, 5);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 1;

		getContentPane().add(scrollPane,gbc_scrollPane);//, BorderLayout.CENTER);

		final Dimension d= new Dimension (2000,1150);// = scrollPane.getPreferredSize();

		scrollPane.setPreferredSize( d );
		scrollPane. setMinimumSize(new Dimension(1200,500));
		scrollPane.setMaximumSize(new Dimension(2000,1150));		    

		JDesktopPane desktopPane = new JDesktopPane();
		desktopPane.setBackground(new Color(245, 245, 220));
		desktopPane.setSize(new Dimension(1250,300));
		desktopPane.setVisible(true);

		desktopPane.setPreferredSize( d );
		desktopPane. setMinimumSize(new Dimension(1200,500));
		desktopPane.setMaximumSize(new Dimension(2000,1150));

		GridBagConstraints gbc_desktopPane = new GridBagConstraints();
		gbc_desktopPane.insets = new Insets(0, 0, 0, 5);
		gbc_desktopPane.fill = GridBagConstraints.BOTH;
		gbc_desktopPane.gridx = 0;
		gbc_desktopPane.gridy = 1;

		scrollPane.add(desktopPane,gbc_desktopPane);		
		scrollPane.setViewportView(desktopPane);		
		this.makeVieworiginal(desktopPane);
		this.makeLayPanels(desktopPane);		
		this.makeInfoTabPane(desktopPane);		
		scrollPane.revalidate();
		scrollPane.updateUI();
	}

	/*
	 * Builds the main menu at the top of the frame
	 */
	private void buildMainMenu(){

		JMenuBar menuBar = new JMenuBar();
		menuBar.setFont(Utilities.CUSTOM_FONT);
		menuBar.setBackground(UIManager.getColor("MenuBar.background"));
		setJMenuBar(menuBar);

		JMenu mnFile = new JMenu("File");
		mnFile.setMnemonic(KeyEvent.VK_F);
		mnFile.setFont(Utilities.CUSTOM_FONT);
		menuBar.add(mnFile);

		JMenuItem mntmOpen = new JMenuItem("Open");		
		mntmOpen.addActionListener(new OpenAction());
		mntmOpen.setIcon(new ImageIcon(BeeGui.class.getResource("/javaguiresources/11949984141591900034fileopen.svg.med.png")));
		mnFile.add(mntmOpen);
		//TODO add a close menuitem for closing the original graph and the book embedding views

		mnFile.addSeparator();

		JMenuItem mntmSaveBE = new JMenuItem("Save book embedding");
		mntmSaveBE.addActionListener(new SaveBEAction());
		mntmSaveBE.setIcon(new ImageIcon(BeeGui.class.getResource("/javaguiresources/save.png")));
		mnFile.add(mntmSaveBE);

		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(new ActionListener() 
		{//TODO add a lambda expression
			public void actionPerformed(ActionEvent e) 
			{
				System.exit(0);
			}
		});
		mnFile.add(mntmExit);

		JMenu mnEditMenu = new JMenu("Edit");
		mnEditMenu.setFont(Utilities.CUSTOM_FONT);
		mnEditMenu.setMnemonic(KeyEvent.VK_E);//alt +E
		menuBar.add(mnEditMenu);

		undo = new JMenuItem("Undo");
		undo.setIcon(new ImageIcon(BeeGui.class.getResource("/javaguiresources/Undo-icon.png")));
		disableUndo();
		undo.setAccelerator( KeyStroke.getKeyStroke( KeyEvent.VK_Z, ActionEvent.CTRL_MASK));
		undo.addActionListener(new UndoAction());
		mnEditMenu.add(undo);
		redo = new JMenuItem("Redo");
		redo.setIcon(new ImageIcon(BeeGui.class.getResource("/javaguiresources/redoo.png")));		
		disableRedo();
		redo.addActionListener(new  RedoAction());
		mnEditMenu.add(redo);

		mnEditMenu.addSeparator();

		JMenu mnView = new JMenu("View");
		mnView.setMnemonic(KeyEvent.VK_V);
		mnView.setFont(Utilities.CUSTOM_FONT);
		menuBar.add(mnView);

		JMenuItem mntmCreateBE = new JMenuItem("Create Book Embedding");
		mntmCreateBE.addActionListener(new CreateBEAction());
		mntmCreateBE.setIcon(new ImageIcon(BeeGui.class.getResource("/javaguiresources/layout.gif")));
		mnView.add(mntmCreateBE);

		mnView.addSeparator();

		JMenuItem mntmFitContent = new JMenuItem("Fit Content");		
		mntmFitContent.setIcon(new ImageIcon(BeeGui.class.getResource("/javaguiresources/FitContent.gif")));
		mntmFitContent.addActionListener(new FitContentAction());
		mnView.add(mntmFitContent);

		JMenu mnTools = new JMenu("Tools");
		mnTools.setMnemonic(KeyEvent.VK_T);
		mnTools.setFont(Utilities.CUSTOM_FONT);
		menuBar.add(mnTools);

		JMenu mnAlgorithms= new JMenu("Algorithms");
		mnTools.add(mnAlgorithms);

		JMenuItem mntmHeuristics = new JMenuItem("Heuristics");
		mnAlgorithms.add(mntmHeuristics);		
		mntmHeuristics.addActionListener(new HeuristicAction());

		JMenu mnHelp = new JMenu("Help");
		mnHelp.setFont(Utilities.CUSTOM_FONT);
		JMenuItem mntmHelp = new JMenuItem("Help");
		mnHelp.add(mntmHelp);		
		mntmHelp.addActionListener(new HelpAction());
		menuBar.add(mnHelp);

		JMenu mnAbout = new JMenu("About");
		mnAbout.setFont(Utilities.CUSTOM_FONT);
		menuBar.add(mnAbout);
		//TODO add functionality to about
	}

	/*
	 * Builds the toolbar bellow the main menu with shortcuts to functions. 
	 */
	JButton deletePageButton;
	JButton createPageButton;
	
	private void buildToolBars(JPanel toolBarContainer){
		JToolBar toolBar = new JToolBar();
		toolBar.setBackground(SystemColor.menu);
		GridBagConstraints gbc_toolBar = new GridBagConstraints();
		gbc_toolBar.insets = new Insets(0, 0, 5, 5);
		gbc_toolBar.fill = GridBagConstraints.HORIZONTAL;
		gbc_toolBar.gridx = 0;
		gbc_toolBar.gridy = 0;
		toolBarContainer.add(toolBar, gbc_toolBar);

		JButton openButton = new JButton(new OpenAction());
		openButton.setToolTipText("Open");
		openButton.setIcon(new ImageIcon(BeeGui.class.getResource("/javaguiresources/11949984141591900034fileopen.svg.med.png")));
		toolBar.add(openButton);

		JButton saveButton = new JButton(new SaveBEAction());		
		saveButton.setBackground(SystemColor.menu);
		saveButton.setToolTipText("Save book embedding");
		saveButton.setIcon(new ImageIcon(BeeGui.class.getResource("/javaguiresources/save.png")));
		toolBar.add(saveButton);

		//		JButton createBEButton = new JButton("");
		JButton createBEButton = new JButton(new CreateBEAction());
		createBEButton.setToolTipText("Create book embedding");
		createBEButton.setBackground(SystemColor.menu);
		createBEButton.setIcon(new ImageIcon(BeeGui.class.getResource("/javaguiresources/layout.gif")));
		//		createBEButton.addActionListener(new CreateBEAction());
		toolBar.add(createBEButton);

		//		JButton openThisBE = new JButton("");
		//		openThisBE.setIcon(new ImageIcon(BeeGui.class.getResource("/javaguiresources/brush.png")));
		//		openThisBE.setToolTipText("open in  book embedding  mode");
		//		openThisBE.setBackground(SystemColor.menu);		
		//		openThisBE.addActionListener(new OpenBEAction());
		//		toolBar.add(openThisBE);

		//		JButton deletePage = new JButton("");
		//JButton 
		deletePageButton = new JButton(new DeletePageAction());
		deletePageButton.setIcon(new ImageIcon(BeeGui.class.getResource("/javaguiresources/delete.png")));
		deletePageButton.setToolTipText("Delete Page");
		deletePageButton.setBackground(SystemColor.menu);
		//		deletePage.addActionListener(new DeletePageAction());
		deletePageButton.setEnabled(false);
		toolBar.add(deletePageButton);

		createPageButton = new JButton(new CreatePageAction());
		createPageButton.setIcon(new ImageIcon(BeeGui.class.getResource("/javaguiresources/adding.jpg")));
		createPageButton.setToolTipText("Create New Page");
		createPageButton.setBackground(SystemColor.menu);
		//		deletePage.addActionListener(new DeletePageAction());
		createPageButton.setEnabled(false);
		toolBar.add(createPageButton);

	}//toolbars

	public void activateToolBarBEButtons(){
		deletePageButton.setEnabled(true);
		createPageButton.setEnabled(true);
	}
	
	
	public void deactivateToolBarBEButtons(){
		deletePageButton.setEnabled(false);
		createPageButton.setEnabled(false);
	}
	/*
	 * Creates the view of the original graph with its menu 
	 */
	private void makeVieworiginal(JDesktopPane originalViewContainer)
	{

		JPanel mainViewPanel= new JPanel();
		mainViewPanel.setBounds(5, 5, 310, 310);
		viewOriginal = new Graph2DView();
		viewOriginal.getCanvasComponent().setBackground(new Color(253, 245, 230));
		viewOriginal.setBounds(10, 10, 300, 300);
		viewOriginal.setPreferredSize(new Dimension(300,300));
		//		viewOriginal.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.LOWERED, null, null, new Color(191, 205, 219), null), 
		//new LineBorder((new Color(227, 227, 227)))));
		mainViewPanel.add(viewOriginal);
		originalViewContainer.add(mainViewPanel); 
		viewOriginal.setVisible(true);
		mainViewPanel.setVisible(true);

		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(5, 318, 130, 19);
		originalViewContainer.add(menuBar);

		JMenu mnEdit = new JMenu("Edit");
		mnEdit.setFont(Utilities.CUSTOM_FONT);
		menuBar.add(mnEdit);

		JMenuItem mnEditOpen = new JMenuItem("Edit Mode");
		mnEditOpen.setFont(Utilities.CUSTOM_FONT);
		mnEditOpen.addActionListener(new EditModeAction());
		mnEdit.add(mnEditOpen);

		JMenu mnTools = new JMenu("Tools");
		mnTools.setFont(Utilities.CUSTOM_FONT);
		menuBar.add(mnTools);

		JMenuItem mntmAnalyze= new JMenuItem("Analyze");
		mntmAnalyze.addActionListener(new AnalysisAction());
		mnTools.add(mntmAnalyze);

		JMenu mnLayout = new JMenu("Layout");
		mnLayout.setFont(Utilities.CUSTOM_FONT);
		menuBar.add(mnLayout);

		JMenuItem mntmCircular= new JMenuItem("Circular");
		mntmCircular.setIcon(new ImageIcon(BeeGui.class.getResource("/javaguiresources/circular-layout.gif")));
		mntmCircular.addActionListener(new CircularLayoutAction());
		mnLayout.add(mntmCircular);

		JMenuItem mntmOrganic= new JMenuItem("Organic");
		mntmOrganic.setIcon(new ImageIcon(BeeGui.class.getResource("/javaguiresources/organicLayout.png")));
		mntmOrganic.addActionListener(new OrganicLayoutAction());
		mnLayout.add(mntmOrganic);

		JMenuItem mntmOrthogonal= new JMenuItem("Orthogonal");
		mntmOrthogonal.setIcon(new ImageIcon(BeeGui.class.getResource("/javaguiresources/layout_orthogonal.jpg")));
		mntmOrthogonal.addActionListener(new OrthogonalLayoutAction());
		mnLayout.add(mntmOrthogonal);
	}

	/*
	 * Creates the views of the book embedding of the original graph
	 */
	private void makeLayPanels(JDesktopPane  layviewContainer)
	{	
		//TODO fix the dimensions
		lvPanels=new ArrayList<JPanel>();
		int x=340;
		int y=6;
		int width=1250;
		int height=350;
		//		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		//		int width=screenSize.width-
		for(int i= 0; i<Utilities.INSTANCES;i++)
		{
			ColorBarsPanel laypanel= new ColorBarsPanel(i==0);
			laypanel.setBounds(x, y + height*i , width , height);
			laypanel.setBorder( BorderFactory.createEtchedBorder());
			//			laypanel.setPreferredSize(preferredSize);
			laypanel.setName("laypanel-"+i);
			lvPanels.add(laypanel);
			layviewContainer.add(laypanel);
		}
	}

	/*
	 * Creates the tabbed pane that holds information for the graph and the embedding
	 */
	private void makeInfoTabPane(JDesktopPane infoContainer)
	{
		infoPane= new JTabbedPane(JTabbedPane.TOP);   	 
		infoPane.setBounds(5, 348, 310, 310);
		//LineBorder line= new LineBorder(new Color(153,153,255),2,true);
		//infoTab.setBorder(BorderFactory.createTitledBorder(line, "Info"));
		infoPane.setFont(Utilities.CUSTOM_FONT);
		infoPane.setBackground(new Color(224,224,224));			
		infoContainer.add(infoPane);
	}

	/*
	 * Creates the panel that holds information about the crossings of the embedding
	 */
	private void createCrossingsPanel()
	{
		crossingspanel = new JPanel();
		crossingspanel.setLayout(new BoxLayout(crossingspanel, BoxLayout.Y_AXIS));
		crossingspanel.setBackground(Color.WHITE);
		infoPane.addTab("Crossings Panel",crossingspanel);
		crossingspanel.setFont(Utilities.CUSTOM_FONT);
		crossingspanel.setVisible(true);
	}

	/*
	 * Removes the panel of the crossings, if the original graph is closed
	 */
	private void removeCrossingsPanel()
	{
		infoPane.remove(crossingspanel);
	}
	
	public void upadateCrossingInfo(Map<Color, Integer> crossingsMap)
	{
		if(crossingspanel==null)
			this.createCrossingsPanel();
		crossingspanel.removeAll();
		//		BorderLayout bl= new BorderLayout();

		JLabel label = new JLabel("  ");
		label.setFont(Utilities.CUSTOM_FONT);
		label.setVerticalAlignment(SwingConstants.CENTER);
		crossingspanel.add(label,BorderLayout.NORTH);
		crossingspanel.setBackground(new Color(255,250,240));
		String[] columnProperties={" Color ", " # Crossings"};
		Object [][] data= new  Object [ crossingsMap.size()] [];
		Set<Entry<Color, Integer>> entries=crossingsMap.entrySet();
		int index=0;
		for(Iterator<Entry<Color, Integer>> iter=entries.iterator();iter.hasNext();)
		{
			Entry<Color, Integer> entry=iter.next();
			String colorName= Utilities.COLOR_NAMES[Utilities.getColorIndex(entry.getKey())];
			Object[] dataItem	= {colorName,Integer.toString(entry.getValue())};
			data[index]=dataItem;
			index++;
		}
//		for(int i=0;i<crossingsMap.size();i++)
//		{
//			String colorName=Utilities.getColorIndex(crossingsMap.)
//			Object[] dataItem	= { Utilities.COLOR_NAMES[i],Integer.toString(crossingsMap.get(Utilities.COLORS[i]))};
//			data[i]=dataItem;
//		}
		DefaultTableModel model = new DefaultTableModel(data, columnProperties);
		JTable  table = new JTable(model);
		table.setShowVerticalLines(false);
		table.setShowHorizontalLines(false);
		//table.setBorder(BorderFactory.createSoftBevelBorder(BevelBorder.RAISED));
		//LineBorder line= new LineBorder(new Color(153,153,255),1,true);
		//table.setBorder(BorderFactory.createTitledBorder(line));
		table.setRowHeight(25);
		table.setFont(Utilities.CUSTOM_FONT);
		TableColumn column = null;
		for (int i = 0; i < 2; i++) 
		{
			column = table.getColumnModel().getColumn(i);
			column.setPreferredWidth(100);
			column.setMinWidth(100);
			column.setMaxWidth(100);
			//column.setCellRenderer(new MyColorRenderer());
		}
		table.setDefaultRenderer(Object.class, new PageColorRenderer());
		crossingspanel.add(table,BorderLayout.LINE_START);
		table.setVisible(true);		
		crossingspanel.updateUI();
	}//upadateCrossingInfoPane	

	//=================================================================	
	//GETTERS
	//=================================================================
	/**
	 * Accesses the list of panels that hold the views of the embedding
	 * @return the list of panels
	 */
	public List<JPanel> getLVPanels()
	{
		return this.lvPanels;
	}

	/**
	 * Accesses the view of the original graph
	 * @return the original graph
	 */
	public Graph2DView getViewOriginal() 
	{
		return viewOriginal;
	}


	//=================================================================	
	//ACTIVATE-RESET COLOR BARS
	//=================================================================

	/**
	 * Activates the pages of the parameter. Each row of the array corresponds to a view of the embedding.
	 * It is called whenever an embedding is opened or a new book embedding is created.
	 * @param activePages the active pages of the embedding
	 */
	public void initializeBarsActiveColors(List<Page>[] activePages)
	{
		//		System.out.println("bee, lvcontroler->gui.setBarsActiveColors");
		for(int i=0;i<lvPanels.size();i++)
		{
			((ColorBarsPanel)lvPanels.get(i)).initializeBarsActiveColors(activePages[i]);
		}
	}

	/**
	 * Resets the color bars of the embedding views, that is all pages are removed.
	 * It is called whenever there is no book embedding
	 */
	public void resetColorBars()
	{
		for(int i=0;i<lvPanels.size();i++)
		{
			((ColorBarsPanel)lvPanels.get(i)).resetColorBars();
		}	    
	}

	/**
	 * Creates a new page with the given color. The new page is added to the right along the color bar.
	 * @param color the color of the new page.
	 */
	public void createPage(Color color)
	{
		for(int i=0;i<lvPanels.size();i++)
		{
			((ColorBarsPanel)lvPanels.get(i)).createPage(color);			
		}
	}

	public void deletePage(Color color)
	{
		for(int i=0;i<lvPanels.size();i++)
		{
			((ColorBarsPanel)lvPanels.get(i)).deletePage(color);
		}
	}

	//=================================================================
	//=================================================================
	//=================         A C T I O N S         =================


	//=================================================================
	//HELP ACTION
	//=================================================================

	private class HelpAction extends AbstractAction
	{
		private static final long serialVersionUID = 1L;

		public void actionPerformed(ActionEvent helpEvent) 
		{
						System.out.println("BeeGui: help action");
			new HelpFrame();
		}
	}//help

	//=================================================================	
	//OPEN ACTION
	//=================================================================

	private class OpenAction extends AbstractAction
	{
		private static final long serialVersionUID = 1L;

		public void actionPerformed(ActionEvent openEvent) 
		{
			System.out.println("BeeGui: open action");
			JFileChooser chooser = new JFileChooser();
			if(chooser.showOpenDialog(BeeGui.this) == JFileChooser.APPROVE_OPTION)
			{
				String name = chooser.getSelectedFile().toString();
				openFile(name);
				deactivateToolBarBEButtons();
			}
		}
	}//open

	/**
	 * Open the given file. It supports only extensions .gml and .be.
	 * @param name the name of the file to open
	 */
	public void openFile(String name)
	{
		if(name.endsWith(".be"))
		{
			if(infoPane!=null)
				infoPane.removeAll();
			//			createCrossingsPanel();
			this.mainControler.openEmbedded(name);
		}
		if (name.endsWith(".gml"))
		{
			GMLIOHandler ioh = new GMLIOHandler();
			try
			{
				viewOriginal.getGraph2D().clear();
				ioh.read(viewOriginal.getGraph2D(),name);
				viewOriginal.setVisible(true);
				this.mainControler.openOriginal(viewOriginal.getGraph2D());
				if(infoPane!=null)
					infoPane.removeAll();
				//				createCrossingsPanel();
			}
			catch (IOException ioe)
			{
				D.show(ioe);
			}
		} 
		viewOriginal.fitContent();
		viewOriginal.getGraph2D().updateViews();
	}

	//TODO is this necessary?
	public void load(Graph2D graph)
	{
		System.out.println("BeeGui: load original graph");
		viewOriginal.getGraph2D().clear();
		viewOriginal.setGraph2D(graph);
		viewOriginal.fitContent();
		viewOriginal.updateView();	
		if(infoPane!=null)
			infoPane.removeAll();
	}



	//=================================================================	
	//SAVE ACTION
	//=================================================================
	private class SaveBEAction extends AbstractAction 
	{
		private static final long serialVersionUID = 1L;

		public void actionPerformed(ActionEvent saveEvent) 
		{
			System.out.println("BeeGui: save action");
			JFileChooser chooser = new JFileChooser();
			if(chooser.showSaveDialog(BeeGui.this) == JFileChooser.APPROVE_OPTION)
			{
				String name = chooser.getSelectedFile().toString();
				if(name.endsWith(".be"))
				{
					try
					{
						mainControler.saveEmbeddedGraph(name);
					}
					catch(Exception exc)
					{
						D.show(exc);
						exc.printStackTrace();
					}
				}
				if (name.endsWith(".gml"))
				{
					GMLIOHandler ioh = new GMLIOHandler();
					try
					{
						Graph2D embeddedGraph	=	mainControler.getBEGraph();
						//TODO will it be really embedded???
						ioh.write(embeddedGraph,name);// Writes the contents of the given graph in GML format to a stream
					} 
					catch (IOException ioe)
					{
						D.show(ioe);
						ioe.printStackTrace();
					}
				}//end if
			}//end if
		}//actionPerformed
	}//AbstractAction


	//=================================================================	
	//EDIT MODE ACTION
	//=================================================================	
	private class EditModeAction extends AbstractAction 
	{
		private static final long serialVersionUID = 1L;

		//TODO check that edit view works nice
		public void actionPerformed(ActionEvent editEvent) 
		{					
			System.out.println("BeeGui: editMode action");
			//			EditView editView = new EditView(viewOriginal,(BeeGui)SwingUtilities.getWindowAncestor(contentPane));	
			new EditView(viewOriginal,(BeeGui)SwingUtilities.getWindowAncestor(getContentPane()));	
		}
	}

	//=================================================================	
	//UNDO-REDO ACTION
	//=================================================================
	private class RedoAction extends  AbstractAction
	{
		private static final long serialVersionUID = 1L;

		public void actionPerformed(ActionEvent redoAction) 
		{
			System.out.println("BeeGui: redo action");
			mainControler.redo();
		}
	}

	private class UndoAction extends  AbstractAction
	{
		private static final long serialVersionUID = 1L;

		public void actionPerformed(ActionEvent undoAction) 
		{
			System.out.println("BeeGui: undo action");
			mainControler.undo();
		}
	}

	/**
	 * Disables undo actions
	 */
	public void disableUndo() 
	{
		System.out.println("BeeGui: disableUndo action");
		undo.setEnabled(false);	
	}


	/**
	 * Disables redo actions
	 */
	public void disableRedo() 
	{
		System.out.println("BeeGui: disableRedo action");
		redo.setEnabled(false);
	}

	/**
	 * Enables undo actions
	 */
	public void enableUndo() 
	{
		System.out.println("BeeGui: enableUndo action");
		undo.setEnabled(true);
	}

	/**
	 * Enables redo actions
	 */
	public void enableRedo() 
	{
		System.out.println("BeeGui: disableUndo action");
		redo.setEnabled(true);		
	}

	//=================================================================	
	//CREATE BOOK EMBEDDING ACTION
	//=================================================================	

	private class CreateBEAction extends AbstractAction
	{
		private static final long serialVersionUID = 1L;

		public void actionPerformed(ActionEvent createBEEvent)
		{
			System.out.println("BeeGui: createBE action");
			OptionHandler op = createBEOptionHandler();
			if(op==null)
				return;
			if(!op.showEditor())
			{
				return;
			}
			if((op.getInt("number of pages")<1)||(op.getInt("number of pages")>Utilities.TOTAL_PAGES))
			{
				JOptionPane.showMessageDialog(lvPanels.get(0),
						"The number of pages should range between [1,"+Utilities.TOTAL_PAGES+"]",
						"Forbidden!",
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			int pages=op.getInt("number of pages");
			createBEview(viewOriginal,pages);
			
			activateToolBarBEButtons();
		}//action performed
	}//launch
	
	private OptionHandler createBEOptionHandler()
	{
		OptionHandler op = new OptionHandler("Options");
		op.addInt("number of pages",2 );
		return op;
	}//createOptionHandler
	
	/*
	 * Creates the embedding of the input graph with the given number of pages and updates the embedding views
	 * @param graph2dview the original graph to embed
	 * @param numPages the number of pages
	 */
	private void createBEview(Graph2DView graph2dview, int numPages)
	{
//		this.createCrossingsPanel();
		mainControler.createBEview(graph2dview,numPages);
		
	}//createLayoutView

	
	//=================================================================	
	//FIT CONTENT ACTION
	//=================================================================		

	private class FitContentAction extends AbstractAction 
	{
		private static final long serialVersionUID = 1L;

		public void actionPerformed(ActionEvent fitContentEvent) 
		{
			System.out.println("BeeGui: fitContent action");
			mainControler.fitContent();
		}
	}



	//=================================================================	
	// DELETE PAGE ACTION
	//=================================================================	

	private class DeletePageAction extends AbstractAction
	{
		private static final long serialVersionUID = 1L;

		public void actionPerformed(ActionEvent deletePageEvent) 
		{
			System.out.println("BeeGui: delete page action");
			mainControler.deletePageAction();
		}
	}


	//=================================================================	
	// DELETE PAGE ACTION
	//=================================================================	

		private class CreatePageAction extends AbstractAction
		{
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent createPageEvent) 
			{
				System.out.println("BeeGui: create page action");
				mainControler.createPageAction();
			}

		}
	//=================================================================	
	//HEURISTIC ACTION
	//=================================================================		

	private class HeuristicAction extends  AbstractAction
	{

		private static final long serialVersionUID = 1L;

		public void actionPerformed(ActionEvent heuristicAction) 
		{
			System.out.println("BeeGui: apply heuristic action");
			mainControler.applyHeuristicAlgo();
		}
	}


	//=================================================================	
	//ANALYSIS ACTION
	//=================================================================		

	//TODO when is it called? Is panel required?
	public boolean planarityChecker(JPanel panel)
	{
		boolean	planar=  GraphChecker.isPlanar(viewOriginal.getGraph2D());
		if(planar)
		{
			System.out.println("its planar");
			JFrame editViewFrame=new JFrame("planar Mode");				        	 
			editViewFrame.setIconImage(Toolkit.getDefaultToolkit().getImage(BeeGui.class.getResource("/javaguiresources/250px-Petersen_double_cover.svg.png")));
			editViewFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
			Graph2DView planarView = new Graph2DView();
			planarView.setGraph2D((Graph2D)viewOriginal.getGraph2D().createCopy());		
			EditMode mode=new MyEditModeConstraint();
			planarView.addViewMode(mode);
			editViewFrame.getContentPane().add(planarView);

			OrthogonalLayouter r = new OrthogonalLayouter(); 
			if(planarView.getGraph2D()!=null)
			{
				r.doLayout(planarView.getGraph2D());
				planarView.fitContent(); 
				planarView.updateView();
			}

			//==================================================== rubbish		
			//			OrthogonalLayouter rl = new OrthogonalLayouter(); 
			//			if(viewOriginal.getGraph2D()!=null){
			//				rl.doLayout(viewOriginal.getGraph2D());
			//				viewOriginal.fitContent(); 
			//				viewOriginal.updateView();
			//			}	

			//mallon axrhsta
			//			System.out.println("*************simple palanar showCircularEdgeOrder()   ************");
			//			
			//			SimplePlanarInformation spi= new SimplePlanarInformation(viewOriginal.getGraph2D());
			//
			//			spi.showCircularEdgeOrder();			

			//			System.out.println("*************simple palanar calcOrdering()   ************");
			////
			////			spi.calcOrdering();
			//
			//			System.out.println("**********simple palanar info  cyclicNextEdge **************");
			//
			//			Graph graph=viewOriginal.getGraph2D();
			//			EdgeCursor ec =graph.edges();
			//			for(ec.toFirst();ec.ok();ec.next()){
			//				Edge edge =spi.cyclicNextEdge(ec.edge());
			//				System.out.println(edge.toString());
			//			}
			//			
			//			
			//			//spi.calcFaces();
			//			spi.showFaces();
			//			
			//			System.out.println(spi.faceCount());// No faces???? wtf?!
			////
			////				EdgeRealizer er =((Graph2D)graph).getRealizer( ec.edge());
			////				PolyLineEdgeRealizer  per = new PolyLineEdgeRealizer ( er);		
			////				((Graph2D) graph).setRealizer(ec.edge(), per);
			////				int bc=er.bendCount();
			////				if(bc>0){
			////					System.out.println("you  have bends!");
			//////					per.clearBends();
			////				}
			////			}//make arcs lines!!
			//
			//======================================================== end of rubbish

			//			PeelingInfo peeling = new PeelingInfo(planarView.getGraph2D());	
			//			peeling.calculatePeelingList();
			LinkedList<Graph2D> graphList=graphInfo.getGraphList();
			System.out.println("graphList");
			System.out.println(graphList.toString());

			if(graphList.isEmpty())
				graphList.add(planarView.getGraph2D());

			System.out.println("graphList  2");
			System.out.println(graphList.toString());

			for(int i=0;i<graphList.size();i++)
			{
				System.out.println("for each component");
				//Graph2D graph =(Graph2D)graphList.get(i).createCopy();
				Graph2D graph =(Graph2D)graphList.get(i);
				System.out.println(graph.toString());

				PeelingInfo peeling = new PeelingInfo(graph);	
				//				peeling.outerFace(true);
				//				peeling.innerGraph();
				peeling.findLevels();
				//peeling.function(graph,null,true);
			}

			planarView.fitContent();
			planarView.updateView();
			planarView.setVisible(true);

			editViewFrame.pack();
			editViewFrame.setVisible(true);

		}//if planar
		return planar;
	}//graphchecker


	/**
	 * Checks whether the original graph is biconnected 
	 * @param panel ?
	 * @return true if biconnected
	 */
	public boolean graphBiconnected(JPanel panel)
	{
		//TODO is the parameter needed?
		boolean biconnected=  GraphConnectivity.isBiconnected(viewOriginal.getGraph2D());
		return biconnected;
	}//graphBiconnected


	/**
	 * Checks whether the original graph is connected 
	 * @param panel ?
	 * @return true if connected
	 */
	public boolean graphConnected(JPanel panel)
	{
		//TODO Does is apply changes only to the original graph? is the parameter needed?
		boolean connected = GraphConnectivity.isConnected(viewOriginal.getGraph2D());
		if(!connected)
		{
			Graph2D g= (Graph2D)viewOriginal.getGraph2D();
			NodeList[] nodelist= GraphConnectivity.connectedComponents(viewOriginal.getGraph2D());
			LinkedList<Graph2D> graphList=graphInfo.getGraphList();
			if(graphList!=null)
			{
				graphList.clear();
			}
			Map<Node, Integer> conNodeMap =graphInfo.getConNodeMap();
			for(int i=0;i<nodelist.length;i++)
			{	
				NodeCursor nc= nodelist[i].nodes();
				Graph2D graph= new Graph2D(g, nc);
				graphList.add(graph);

				int k=Utilities.TOTAL_PAGES;
				for(nc.toFirst();nc.ok();nc.next())
				{
					NodeRealizer nr =g.getRealizer(nc.node());
					if(i<Utilities.TOTAL_PAGES){
						nr.setFillColor(Utilities.COLORS[i]);
						nr.setLineType(LineType.LINE_2);
						nr.getLabel().setFont(new Font("Serif", Font.BOLD, 18));
						conNodeMap.put(nc.node(), i);
					}
					else
					{
						Color randomColor= new Color((int)(Math.random() * 0x1000000));
						nr.setFillColor(randomColor.darker() );
						nr.getLabel().setTextColor(Color.WHITE);//???
						conNodeMap.put(nc.node(), k);
						k++;
					}
				}	//gia ka8e node 
			}//for
			NodeCursor nc1= g.nodes();
			for(nc1.toFirst();nc1.ok();nc1.next())
			{
				System.out.println("conNodeMap "+nc1.node()+" "+  conNodeMap.get(nc1.node()));
			}
			viewOriginal.updateView();
		}//isnt connected
		return connected;
	}//graphconnected




	public void functionality()
	{	
		EditMode editMode=new MyEditModeConstraint();
		viewOriginal.addViewMode(editMode);
		viewOriginal.getCanvasComponent().addMouseWheelListener(new Graph2DViewMouseWheelZoomListener());
		final boolean firstTime =true;
		viewOriginal.getGraph2D().addGraph2DSelectionListener(new Graph2DSelectionListener(){
			@Override
			public void onGraph2DSelectionEvent(Graph2DSelectionEvent event) {				

				if(event.isNodeSelection()){
					Node node=(Node)event.getSubject();
					Map<Node, Integer> conNodeMap =graphInfo.getConNodeMap();
					LinkedList<Graph2D> graphList=graphInfo.getGraphList();
					if(conNodeMap!=null){
						int	i=conNodeMap.get(node);
						System.out.println("you are in connected component "+i);
						//						Graph2D graph2d= graphList.get(i);
						//mainControler.openInGraph2DViews(graph2d,firstTime);
						mainControler.openInGraph2DViews(graphList,firstTime);
						System.out.println("set the graph componet to layview");
					}//if 
				}//addGraph2DSelectionListener
			}
		});
	}//functionality

	private class AnalysisAction extends AbstractAction
	{
		private static final long serialVersionUID = 1L;

		public void actionPerformed(ActionEvent analysisEvent)
		{
			System.out.println("BeeGui: analysis action");
			//TODO ask the user to update the embedding with respect to the connected components
			Graph graph = viewOriginal.getGraph2D();
			int  nodeCount = graph.nodeCount();
			int edgeCount = graph.edgeCount();
			JPanel panel = new JPanel();
			panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
			panel.setBackground(Color.WHITE);
			infoPane.addTab("Graph Analysis",panel);
			infoPane.setFont(Utilities.CUSTOM_FONT);
			JLabel label= new JLabel("   ");
			panel.add(label);

			boolean connected=graphConnected(panel);
			boolean biconnected=graphBiconnected(panel);
			boolean planar=planarityChecker(panel);

			String[] columnProperties={"",""};
			Object [][] data={{"Number Of Nodes",Integer.toString(nodeCount)},
					{"Number Of Edges",Integer.toString(edgeCount)},
					{"Connected",connected},{"Biconnected", biconnected},
					{"Planar",planar}};
			JTable  table = new JTable(data, columnProperties);

			table.setShowVerticalLines(false);
			table.setShowHorizontalLines(false);			
			LineBorder line= new LineBorder(new Color(153,153,255),1,true);
			table.setBorder(BorderFactory.createTitledBorder(line));
			table.setDefaultRenderer(Object.class, new PageColorRenderer());
			table.setRowHeight(25);

			TableColumn column = null;

			for (int i = 0; i < 2; i++) 
			{
				column = table.getColumnModel().getColumn(i);
				column.setPreferredWidth(150); 
				column.setMinWidth(150);
				column.setMaxWidth(150);
			}

			panel.add(table);
			table.setVisible(true);
			panel.updateUI();
			functionality();
		}
	}//AnalysisAction


	//=================================================================	
	//LAYOUTS
	//=================================================================
	private class CircularLayoutAction extends AbstractAction
	{
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent layoutEvent) 
		{
			System.out.println("BeeGui: circular layout action");
			CircularLayouter cl = new CircularLayouter();
			if(viewOriginal.getGraph2D()!=null)
			{
				cl.doLayout(viewOriginal.getGraph2D());
				viewOriginal.fitContent(); 
				viewOriginal.updateView();
			}		
		}		
	}

	private class OrganicLayoutAction  extends AbstractAction
	{
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent layoutEvent) 
		{
			System.out.println("BeeGui: organic layout action");
			OrganicLayouter ol = new OrganicLayouter(); 
			//TODO why chose this edge-length
			ol.setPreferredEdgeLength(40);
			if(viewOriginal.getGraph2D()!=null)
			{
				ol.doLayout(viewOriginal.getGraph2D());
				viewOriginal.fitContent(); 
				viewOriginal.updateView();
			}	
		}
	}

	private class OrthogonalLayoutAction extends AbstractAction
	{
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent layoutEvent) 
		{
			System.out.println("BeeGui: orthogonal layout action");
			OrthogonalLayouter orl = new OrthogonalLayouter(); 
			if(viewOriginal.getGraph2D()!=null)
			{
				orl.doLayout(viewOriginal.getGraph2D());
				viewOriginal.fitContent(); 
				viewOriginal.updateView();
			}		
		}		
	}


	//=================================================================	
	//=================================================================	
	//CUSTOM COLOR BARS CLASS
	//=================================================================

	private class ColorBarsPanel extends JPanel 
	{
		private static final long serialVersionUID = 1L;
		
		JToolBar upToolBar;
		JToolBar downToolBar;
		boolean showAll;

		public ColorBarsPanel(boolean showAll) 
		{
			
			super(new BorderLayout());
			upToolBar=new JToolBar();
			upToolBar.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);	
			upToolBar.setAlignmentX(Component.LEFT_ALIGNMENT);
			this.add(upToolBar,BorderLayout.NORTH);
			
			downToolBar=new JToolBar();
			downToolBar.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);	
			downToolBar.setAlignmentX(Component.LEFT_ALIGNMENT);
			this.add(downToolBar,BorderLayout.SOUTH);
			resetColorBars();	
			this.showAll=showAll;
			System.out.println("ColorBarsPanel(BeeGui): initializing");
		}
		
//		public boolean isShowAll() 
//		{
//			return showAll;
//		}

		
		public void createPage(Color color)
		{
			boolean selected=upToolBar.getComponentCount()%2==0;
			JCheckBox checkBoxUp=createCheckBox(color, selected);
			JCheckBox checkBoxDown=createCheckBox(color, !selected);
			upToolBar.add(checkBoxUp);
			downToolBar.add(checkBoxDown);
			String colorName=Utilities.COLOR_NAMES[Utilities.getColorIndex(color)];
			System.out.println("ColorBarsPanel(BeeGui): create page "+colorName);
		}
		
		public void deletePage(Color color)
		{
			for(int i=0;i<upToolBar.getComponentCount();i++)
			{
				JCheckBox checkBox=(JCheckBox)this.upToolBar.getComponentAtIndex(i);
				Color c=Utilities.getColorFromName(checkBox.getName());
				System.out.println(color+"  "+c);
				if(color.equals(c))
				{
					System.out.println("Removing color check box");
					this.upToolBar.remove(i);
					this.downToolBar.remove(i);
					break;
				}
			}
			this.updateUI();
			String colorName=Utilities.COLOR_NAMES[Utilities.getColorIndex(color)];
			System.out.println("ColorBarsPanel(BeeGui): delete page "+colorName);
		}
		

		public void initializeBarsActiveColors(List<Page> pageIndex)
		{
			
			//			System.out.println(Arrays.toString(pageIndex));
			if(pageIndex==null || pageIndex.isEmpty())
			{
				noDisplay();
				return;
			}
			System.out.println("ColorBarsPanel(BeeGui): showing colorBars "+Arrays.toString(pageIndex.toArray()));
			upToolBar.removeAll();
			downToolBar.removeAll();
			for(int i=0;i<pageIndex.size();i++)
			{
				Page page=pageIndex.get(i);
//				System.out.println(page);
				if(page.getState()==Utilities.PAGE_UP)
				{
					JCheckBox checkBoxUp=createCheckBox(page.getColor(), true);
					JCheckBox checkBoxDown=createCheckBox(page.getColor(), false);
					upToolBar.add(checkBoxUp);
					downToolBar.add(checkBoxDown);
				}

				else if(page.getState()==Utilities.PAGE_DOWN)
				{
					JCheckBox checkBoxUp=createCheckBox(page.getColor(), false);
					JCheckBox checkBoxDown=createCheckBox(page.getColor(), true);
					upToolBar.add(checkBoxUp);
					downToolBar.add(checkBoxDown);
				}
				else if(page.getState()==Utilities.PAGE_HIDE)//***
				{
					JCheckBox checkBoxUp=createCheckBox(page.getColor(), false);
					JCheckBox checkBoxDown=createCheckBox(page.getColor(), false);
					upToolBar.add(checkBoxUp);
					downToolBar.add(checkBoxDown);
				}
				else
				{
//					System.err.println("error with Pages");
				}
			}
			this.updateUI();
		}


		private void noDisplay()
		{
			upToolBar.removeAll();
			final JTextField chckbxColInfo = new JTextField("there is no book embedding...");
			chckbxColInfo.setName("up-info");
			upToolBar.add(chckbxColInfo);				
			chckbxColInfo.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Color.LIGHT_GRAY));
			chckbxColInfo.setVisible(true);
			chckbxColInfo.setEditable(false);
			chckbxColInfo.setForeground(Color.LIGHT_GRAY);
			chckbxColInfo.setFont(new Font(chckbxColInfo.getFont().getName(), Font.ITALIC,chckbxColInfo.getFont().getSize()));

			downToolBar.removeAll();
			final JTextField chckbxColInfo1 = new JTextField("there is no book embedding...");
			chckbxColInfo1.setName("up-info");
			downToolBar.add(chckbxColInfo1);				
			chckbxColInfo1.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Color.LIGHT_GRAY));
			chckbxColInfo1.setVisible(true);
			chckbxColInfo1.setEditable(false);
			chckbxColInfo1.setForeground(Color.LIGHT_GRAY);
			chckbxColInfo1.setFont(new Font(chckbxColInfo1.getFont().getName(), Font.ITALIC,chckbxColInfo1.getFont().getSize()));
		}

		private JCheckBox createCheckBox(Color color, boolean selected)
		{			
			String colorName=Utilities.COLOR_NAMES[Utilities.getColorIndex(color)];
			final JCheckBox chckbxCol = new JCheckBox(colorName);
			chckbxCol.setName(colorName);
			chckbxCol.addActionListener(new PageSelectionAction());
			chckbxCol.setSelected(selected);
			chckbxCol.setBorderPainted(true);
			chckbxCol.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, color));
			chckbxCol.setVisible(true);
			chckbxCol.setEnabled(true);
			return chckbxCol;
		}


		public void resetColorBars()
		{
			
			System.out.println("ColorBarsPanel(BeeGui): reset bars");
			this.initializeBarsActiveColors(null);
		}

		class PageSelectionAction implements ActionListener
		{
			//		class ColorSelectionAction implements ItemListener{

			@Override
			//			public void itemStateChanged(ItemEvent e) {
			public void actionPerformed(ActionEvent pageSelectionEvent) 
			{
				System.out.println("ColorBarsPanel(BeeGui): page selection event...");
				JCheckBox checkbox=(JCheckBox) pageSelectionEvent.getSource();
				JPanel panel=(JPanel) upToolBar.getParent();
				int index;
				int state=Utilities.PAGE_HIDE;
				String colorname=checkbox.getName();
//				System.out.println(colorname);
				Color color=Utilities.getColorFromName(colorname);
				//				System.out.println(checkbox.getName());
				if(checkbox.isSelected())
				{
					
//					if(colorname.startsWith("up"))
					if(upToolBar.getComponentIndex(checkbox)>-1)
					{
						index=upToolBar.getComponentIndex(checkbox);
						((JCheckBox)downToolBar.getComponentAtIndex(index)).setSelected(false);
						state=Utilities.PAGE_UP;
					}
					else
					{
						index=downToolBar.getComponentIndex(checkbox);
						((JCheckBox)upToolBar.getComponentAtIndex(index)).setSelected(false);
						state=Utilities.PAGE_DOWN;
					}
					//					mainControler.showPage(panel,index, page);
					mainControler.showPage(panel, color, state);
				}
				else
				{
					if(showAll)
					{						
						if(upToolBar.getComponentIndex(checkbox)>-1){
							index=upToolBar.getComponentIndex(checkbox);
							((JCheckBox)downToolBar.getComponentAtIndex(index)).setSelected(true);
							state=Utilities.PAGE_DOWN;
						}
						else
						{
							index=downToolBar.getComponentIndex(checkbox);
							((JCheckBox)upToolBar.getComponentAtIndex(index)).setSelected(true);
							state=Utilities.PAGE_UP;
						}
						//						mainControler.showPage(panel,index, page);
						mainControler.showPage(panel,color, state);
					}
					else
					{
						if(upToolBar.getComponentIndex(checkbox)>-1)
						{
							index=upToolBar.getComponentIndex(checkbox);
						}
						else
						{
							index=downToolBar.getComponentIndex(checkbox);
						}
						//						mainControler.hidePage(panel,index);
						mainControler.hidePage(panel,color);
					}
				}
				System.out.println(index);

//				mainControler.updateCrossings();//
			}
		}
	}//class

	//	  private void openInGraph2DViews(Graph2D graph2d, boolean firstTime){
	//		  for(int i=0;i<layviewlist.size();i++){
	//			  
	//			  if(firstTime){  
	//			  MyLayouter layouter= new MyLayouter();
	//			  layouter.setNumberOfPages(pages);
	//			  layouter.doLayoutCore(graph2d);
	//			  layouter.underSpineEdges(graph2d);
	//			  firstTime=false;}
	//			  
	//			  layviewlist.get(i).getGraph2D().clear();
	//			  layviewlist.get(i).setGraph2D(graph2d);
	//			  layviewlist.get(i).updateView();
	//			  //.setVisible(true);
	//		  }
	//	  }
}//class





