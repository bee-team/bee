package bee.view;


import java.awt.Color;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import y.base.Edge;
import y.base.EdgeCursor;
import y.base.Node;
import y.view.ArcEdgeRealizer;
import y.view.EdgeRealizer;
import y.view.Graph2D;
import y.view.Graph2DSelectionEvent;
import y.view.Graph2DSelectionListener;
import y.view.Graph2DView;
import y.view.NodeRealizer;
import bee.demo.graphTemplate.EmbeddedGraph;
import bee.graph.Page;
import bee.option.options.Utilities;

public class MyGraph2DView extends Graph2DView {

	private static final long serialVersionUID = 1L;
	LinkedList<Node> list;

	//	int [] activePages;
	LinkedList<Page> pages;




	private double offset;
	private double nodeDist;




	public MyGraph2DView()
	{
		super();
		Node [] nodes=getGraph2D().getNodeArray();
		list= new LinkedList<Node>((Arrays.asList(nodes)));
		pages=Page.generatePages();
		//		print();
	}



	public void setGraph2D(Graph2D graph, double offset, double nodeDist)
	{
		super.setGraph2D(graph);

		Node [] nodes= getGraph2D().getNodeArray();
		list= new LinkedList<Node>((Arrays.asList(nodes)));
		this.offset=offset;
		this.nodeDist=nodeDist;
		pages=Page.generatePages();
	}




	public LinkedList<Node> getNodeList(){		

		return list;
	}


	public LinkedList<Page> getPages()
	{
		return pages;
	}

	public boolean isActivePage(Color color)
	{
		Page page= findPageByColor(color);
		if(page==null)
		{
			System.err.println("error locating page in MyGraph2DView");
			return false;
		}
		return page.getState()!=Utilities.PAGE_INACTIVE;
	}
	
	public void initializeActiveColors(int []activeColors){
		//		this.activePages=activeColors;
		//		updateEdgeView();
		for(int i=0;i<pages.size();i++)
		{
			pages.get(i).setState(activeColors[i]);
		}
		//TODO check for inactive pages
	}




	//	public void updateView(){
	//
	//		updateNodeView();
	//
	//		updateEdgeView();
	//	}



	public void updateNodeView()
	{
//		System.out.println("MyGraph2DView: update node view");
		for(int i=0; i<list.size();i++){
			Node node= list.get(i);
			(this.getGraph2D()).setCenter(node, offset+i*nodeDist, (this.getGraph2D()).getCenterY(node));
		}

		this.repaint();
		super.updateView();
	}


	public void updateEdgeView()
	{
//		System.out.println("MyGraph2DView: update edge view");
		EdgeCursor ec=this.getGraph2D().edges();
		for (ec.toFirst(); ec.ok(); ec.next())
		{	
			Edge edge= (Edge)ec.current();
			EdgeRealizer er= this.getGraph2D().getRealizer(edge);
			Page page=findPageByColor(er.getLineColor());
			if(page==null)
			{
				System.err.println("an error occured when updating edge view in MyGraph2DView");
				return;
			}
			if(page.getState() ==Utilities.PAGE_HIDE)
				er.setVisible(false);			
			else
			{
				er.setVisible(true);
				Node source=edge.source();		
				Node target= edge.target();
				NodeRealizer nrt =getGraph2D().getRealizer(target);
				NodeRealizer nrs =getGraph2D().getRealizer(source);
				ArcEdgeRealizer aer = new ArcEdgeRealizer( er);		
				getGraph2D().setRealizer(edge, aer);
				if(page.getState()==Utilities.PAGE_UP  )
				{
					if(nrt.getCenterX()<nrs.getCenterX())
					{
						aer.setRatio(-Math.abs(aer.getRatio()));
					}
					else
					{
						aer.setRatio(Math.abs(aer.getRatio()));
					}
				}
				else
				{
					if(nrt.getCenterX()<nrs.getCenterX())
					{
						aer.setRatio(Math.abs(aer.getRatio()));
					}
					else
					{
						aer.setRatio(-Math.abs(aer.getRatio()));
					}
				}
			}
		}
		super.updateView();
	}



	public void activatePage(Color color)
	{
//		System.out.println("MyGraph2DView: activate page "+Utilities.COLOR_NAMES[Utilities.getColorIndex(color)]);
		Page page= findPageByColor(color);
		if(page==null)
		{
			System.err.println("error activating page in MyGraph2DView");
			return;
		}
		if(page.getState()==Utilities.PAGE_INACTIVE)
		{
			if(pages.indexOf(page)%2==1)
				page.setState(Utilities.PAGE_DOWN);
			else
				page.setState(Utilities.PAGE_UP);	
		}
		//		System.out.println("my activate Page array : " + Arrays.toString(activePages));
	}



	//	public int  []  getActivePages(){
	//		return activePages;
	//	}





	public void moveNodes(List<Integer> nodeIndex, int distIndex)
	{
//		System.out.println("MyGraph2DView: move nodes");
		LinkedList<Node> list= getNodeList();
		//		System.out.println("embedded graph");



		if(distIndex>0){			//right
			for(int s=nodeIndex.size()-1;s>=0;s--)
			{
				int startIndex=nodeIndex.get(s);
				Node node= list.get(startIndex);

				int endIndex=Math.min(list.size()-1, startIndex+distIndex);
				for(int i=startIndex+1;i<=endIndex;i++){				
					list.set(i-1, list.get(i));
				}
				list.set(endIndex, node);

			}
		}else{
			for(int s=0;s<nodeIndex.size();s++)
			{
				int startIndex=nodeIndex.get(s);
				Node node= list.get(startIndex);

				int endIndex= Math.max(startIndex+distIndex, 0);
				for(int it=startIndex-1; it>=endIndex; it--){
					list.set(it+1, list.get(it));
				}
				list.set(endIndex, node);

			}
		}


	}




	public void moveEdges(List<int []> edgeIndex, Color color)
	{
//		System.out.println("MyGraph2DView: update move edges to page "+Utilities.COLOR_NAMES[Utilities.getColorIndex(color)]);
		EdgeCursor ec= this.getGraph2D().edges();
		for(ec.toFirst();ec.ok();ec.next()){
			Edge e= ec.edge();
			int source=e.source().index();
			int target=e.target().index();
			for(int j=0;j<edgeIndex.size();j++)
			{
				if(source==edgeIndex.get(j)[0]&&target==edgeIndex.get(j)[1]
						|| source==edgeIndex.get(j)[1]&&target==edgeIndex.get(j)[0]){
					EdgeRealizer er= this.getGraph2D().getRealizer(e);
					er.setLineColor(color);		
				}
			}
		}	
	}


	private Page findPageByColor(Color color)
	{
		for(int i=0;i<this.pages.size();i++)
		{
			if(pages.get(i).getColor().equals(color))
			{
				return pages.get(i);
			}

		}
		return null;
	}

	public void print()
	{
		for(int i=0;i<list.size();i++)

			System.out.print(list.get(i).index()+", ");
		System.out.println("-------");
	}

	//===========================
	// show page
	//===========================

	public void showPage(Color color, int state)
	{
//		System.out.println("MyGraph2DView: show page "+Utilities.COLOR_NAMES[Utilities.getColorIndex(color)]+" on page "+state);
		//	System.out.println("       showing page "+index);
		Page page=findPageByColor(color);
		if(page==null)
		{
			System.err.println("error activating page in MyGraph2DView");
			return;
		}
		if(page.getState()==state)
			return;
		page.setState(state);
		EdgeCursor ec=this.getGraph2D().edges();
		//	EdgeReverser erev =new EdgeReverser();	
		//	EdgeList list=new EdgeList();
		for (ec.toFirst(); ec.ok(); ec.next()){	
			Edge edge= (Edge)ec.current();
			EdgeRealizer er= this.getGraph2D().getRealizer(edge);
			if(er.getLineColor().equals(color))
			{
				er.setVisible(true);
				//			Node source=edge.source();		
				//			Node target= edge.target();
				//
				//			NodeRealizer nrt =getGraph2D().getRealizer(target);
				//			NodeRealizer nrs =getGraph2D().getRealizer(source);
				//
				//			if(nrt.getCenterX()<nrs.getCenterX() && page==Utilities.PAGE_UP ||
				//					nrt.getCenterX()>nrs.getCenterX() && page==Utilities.PAGE_DOWN){
				//				er.clearBends();
				//				list.add(ec.edge());				
				//			}
			}
		}
		//	System.out.println("     reversing "+list.size()+" edges");
		//	erev.reverseEdges(this.getGraph2D(), list);	//
		updateEdgeView();

		updateView();

	}


	//===========================
	// hide page
	//===========================

	public void hidePage(Color color)
	{
//		System.out.println("MyGraph2DView: hide page "+Utilities.COLOR_NAMES[Utilities.getColorIndex(color)]);
		Page page=this.findPageByColor(color);
		if(page==null)
		{
			System.err.println("error when hiding page in MyGraph2DView");
			return;
		}
		//	System.out.println("       hiding page "+index);
		if(page.getState()==Utilities.PAGE_HIDE)
			return;
		page.setState(Utilities.PAGE_HIDE);
		EdgeCursor ec=this.getGraph2D().edges();
		for (ec.toFirst(); ec.ok(); ec.next()){	
			Edge edge= (Edge)ec.current();
			EdgeRealizer er= this.getGraph2D().getRealizer(edge);
			if(er.getLineColor().equals(color))
			{
				er.setVisible(false);
			}
		}
		updateView();
	}


	//===========================
	//    delete page
	//===========================


	public void deletePage(Color deleteColor, Color moveToColor) 
	{
		System.out.println("MyGraph2DView: delete page "+Utilities.COLOR_NAMES[Utilities.getColorIndex(deleteColor)]);
		Page pagefrom=findPageByColor(deleteColor);
		Page pageTo=findPageByColor(moveToColor);
		if(pagefrom==null || pageTo==null || pageTo.getState()==Utilities.PAGE_INACTIVE)
		{
			System.err.println("error deleting page in MyGraph2DView");
			return;
		}
		EdgeCursor ec=this.getGraph2D().edges();
		for (ec.toFirst(); ec.ok(); ec.next()){	
			Edge edge= (Edge)ec.current();
			EdgeRealizer er= this.getGraph2D().getRealizer(edge);
			if(er.getLineColor().equals(deleteColor))
			{
				er.setLineColor(moveToColor);
			}
			//		else{
			//			int i=Utilities.getColorIndex(er.getLineColor());
			//			if(i>index)
			//				er.setLineColor(Utilities.COLORS[i-1]);
			//
			//		}
		}

		pagefrom.setState(Utilities.PAGE_INACTIVE);//?
		int index=pages.indexOf(pagefrom);
		while(index<pages.size()-1)
		{
			if(pages.get(index+1).getState()==Utilities.PAGE_INACTIVE)
				break;
			pages.remove(index);
			pages.add(index+1, pagefrom);
			index++;		
		}

		updateView();
	}

	public void deletePage(Color deleteColor) 
	{
		Color moveTo=pages.get(0).getColor();
		this.deletePage(deleteColor, moveTo);
	}
}//class
