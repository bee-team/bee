package bee.graph;

import java.awt.Color;
import java.util.LinkedList;

import bee.option.options.Utilities;

public class Page
{
	private int state;
	public int getState()
	{
		return state;
	}

	public void setState(int state)
	{
		this.state = state;
	}

	public Color getColor()
	{
		return color;
	}

	private Color color;
	
	public Page(int state, Color color)
	{
		this.state=state;
		this.color=color;
	}
	
	public void disablePage()
	{
		this.state=Utilities.PAGE_INACTIVE;
	}
	
	public void makeUpVisible()
	{
		this.state=Utilities.PAGE_UP;
	}
	
	public void makeDownVisible()
	{
		this.state=Utilities.PAGE_DOWN;
	}
	
	public void hidePage()
	{
		this.state=Utilities.PAGE_HIDE;
	}
	
	public static LinkedList<Page> generatePages()
	{
		LinkedList<Page> pages=new LinkedList<Page>();
		for(int i=0;i<Utilities.TOTAL_PAGES;i++)
		{
			Page page=new Page(Utilities.PAGE_INACTIVE,Utilities.COLORS[i]);
			pages.add(page);
		}
		return pages;
	}
	
	public String toString()
	{
		return Utilities.COLOR_NAMES[Utilities.getColorIndex(color)]+"  "+this.state;
	}
}
