package bee.graph.info;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import y.base.Node;
import y.view.Graph2D;

public class OriginalViewInfo
{

	private Map<Node, Integer>conNodeMap;
	private LinkedList<Graph2D> graphList;
	
	public OriginalViewInfo()
	{
		conNodeMap =new HashMap<Node,Integer>();
		graphList= new LinkedList<Graph2D> ();
	}

	public Map<Node, Integer> getConNodeMap()
	{
		return conNodeMap;
	}

	public LinkedList<Graph2D> getGraphList()
	{
		return graphList;
	}
}
