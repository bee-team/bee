package bee.functionality;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.JPanel;

import y.view.Graph2D;
import y.view.Graph2DView;
import bee.controlers.MainControler;

public interface GUIFunctionality
{

	/**
	 * Setup the main controler that allows the GUI to execute commands
	 * @param contr the main controler
	 */
	void setMainControler(MainControler contr);
	
	/**
	 * Gets the lists of JPanels that contain the different views of the embedding
	 * @return a list of JPanels 
	 */
	ArrayList<JPanel> getLVPanels();
	
	/**
	 * Gets the original view of the graph which is independent of the embedded views
	 * @return the original view of the graph
	 */
	Graph2DView getViewOriginal();
	
	/**
	 * Sets the active colors that correspond to pages at the panels of the embedded views based on the parameter activeColors: 
	 * each row corresponds to an embedded view and contains an int value that corresponds to top, down or no page
	 * @param activeColors the encoded matrix of visible pages
	 */
	void setBarsActiveColors(int[][] activeColors);
	
	/**
	 * Resets the active colors that correspond to pages at the panels of the embedded views, when there is no embedding created
	 */
	void resetColorBars();
	
	/**
	 * Loads a specific Graph2D object as the original graph
	 * @param graph the graph to be loaded
	 */
	void load(Graph2D graph);
	
	/**
	 * Opens a .gml or .be file that is associated with a graph
	 * @param name the file-name of the graph
	 */
	void openFile(String name);
	
	/**
	 * Checks if the original graph is planar
	 * @param panel
	 * @return true if the original graph is planar
	 */
	boolean planarityChecker(JPanel panel);
	
	/**
	 * Checks if the original graph is biconnected
	 * @param panel
	 * @return true if the original graph is biconnected
	 */
	boolean graphBiconnected(JPanel panel);
	/**
	 * Checks if the original graph is connected
	 * @param panel
	 * @return true if the original graph is connected
	 */
	boolean graphConnected(JPanel panel);
	
	/**
	 * TODO check out what it does
	 */
	void functionality();
	
	/**
	 * Updates the panel that holds the information of the number of crossings on each page based on crossingsMap.
	 * CrossingsMap is a mapping of Color object to Integer object: each color is associated to a number of crossings
	 * @param crossingsMap the map containing the info
	 */
	void upadateCrossingInfoPane(Map<Color, Integer> crossingsMap);
	
	/**
	 * Disables the undo command
	 */
	void disableUndo();
	
	/**
	 * Disables the redo command
	 */
	void disableRedo();

	/**
	 * Enables the undo command
	 */
	public void enableUndo();

	/**
	 * Enables the redo command
	 */
	public void enableRedo();
}
