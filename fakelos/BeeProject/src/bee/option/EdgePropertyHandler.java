package bee.option;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JList;
import javax.swing.ListCellRenderer;

import y.base.Edge;
import y.base.EdgeCursor;
import y.option.OptionHandler;
import y.view.EdgeRealizer;
import y.view.Graph2D;
import bee.controlers.LVControler;
import bee.graph.Page;
import bee.option.options.Utilities;
//import bee.demo.Changeable;
import bee.undoRedo.EdgeMoveChange;



public class EdgePropertyHandler extends OptionHandler
{
	Color preColor;
	Color newColor;
	//	Color [] col;//=Utilities.COLORS;
	//	int pages;
	LVControler lvcontroler;

	public EdgePropertyHandler(LVControler controler)
	{	
		super(" Proprties");
		this.lvcontroler=controler;
	}//const

	public void createHandler()
	{
		System.out.println("EdgePropertyHandler: create handler");
		int size;
		int pages=this.lvcontroler.getPages();
		List<Page> activePages=lvcontroler.getActivePages()[0];
		if(pages==Utilities.TOTAL_PAGES)
			size=pages;
		else
			size=pages+1;
		Color[] col= new Color[size];
		int index=0;
		for(int i=0;i<activePages.size();i++)
		{
//			if(index!=i)
//				System.err.println("pages are not well defined in my graph 2d view");
			if(activePages.get(i).getState()!=Utilities.PAGE_INACTIVE)
			{
				col[index]=activePages.get(i).getColor();
//				System.out.print(index+" ");
				index++;
			}
			
		}
		if(index<col.length)
		{
			for(int i=0;i<activePages.size();i++)
			{
				if(activePages.get(i).getState()==Utilities.PAGE_INACTIVE)
				{
					col[index]=activePages.get(i).getColor();
//					System.out.print("extra page");
					break;
				}				
			}
		}
//		System.out.println();
		JList<Color> list= new JList<Color>(col );	 	
		ListCellRenderer<Color> renderer = new MyListCellRenderer<Color>();
		list.setCellRenderer(renderer);		
		this.clear();		
		this. addEnum("Move to Page", col, col[0], renderer) ; 		
	}




	public void updateValuesFromSelection(Graph2D graph)
	{
		System.out.println("EdgePropertyHandler: updating values from selection");
		EdgeCursor ec = graph.selectedEdges();
		EdgeRealizer er =graph.getRealizer(ec.edge());
		//get the initial values from the first selected edge
		preColor = er.getLineColor();
		boolean samecolor = true;		
		//boolean page=false;
		//get all further values from the remaining set of selected edges
		if(ec.size() > 1)
		{
			for (ec.next(); ec.ok(); ec.next())
			{
				er = graph.getRealizer(ec.edge());
				if (samecolor && preColor != er.getLineColor())
					samecolor = false;
			}
			set("Move to Page", preColor);
			getItem("Move to Page").setValueUndefined(!samecolor);			
			//set("Delete Page",)
		}
	}//updateVal
	
	public void commitEdgeProperties(Graph2D graph)
	{
		System.out.println("EdgePropertyHandler: committing edge changes");
//		System.out.println("committing edge changes from EdgePropertyHandler");
		boolean newPage=false;
		List<int[]> edgeIndex=new ArrayList<int []>();
		EdgeCursor ec = graph.selectedEdges();
		Color[] previousPages=new Color[ec.size()];
		int pos=0;
		newColor=Utilities.COLORS[Utilities.getColorIndex((Color)get("Move to Page"))];
		for (ec.toFirst(); ec.ok(); ec.next())
		{
			Edge edge = ec.edge();
			EdgeRealizer er =graph.getRealizer(edge);
			int [] current=new int[2];
			current[0]=edge.source().index();
			current[1]=edge.target().index();
			edgeIndex.add(current);
			previousPages[pos]=er.getLineColor();
			pos++;
//			if (!getItem("Move to Page").isValueUndefined())
//				er.setLineColor((Color)get("Move to Page"));			
//			newColor=(Color)get("Move to Page");			 
			//			num= getEnum("Move to Page");
//			num=Utilities.getColorIndex(newColor);
//			lvcontroler.moveEdges(edgeIndex,num);
//			lvcontroler.moveEdges(edgeIndex,newColor);
			int pages=this.lvcontroler.getPages();
			if(getEnum("Move to Page")>=pages){
				System.out.println(" new page "+ getEnum("Move to Page"));
				newPage=true;
				pages++;			
			}
		}//for each edge
		lvcontroler.moveEdges(edgeIndex,newColor);
		new EdgeMoveChange(edgeIndex, newColor, previousPages, lvcontroler, newPage);
		createHandler();
//		lvcontroler.informMainControler();
	}//commitEdgeProperies
}// class EdgePropertyHandler	
