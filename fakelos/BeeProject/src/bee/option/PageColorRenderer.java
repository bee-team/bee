package bee.option;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import bee.option.options.Utilities;

public class PageColorRenderer extends DefaultTableCellRenderer  
{
	private static final long serialVersionUID = 1L;

	@Override
	public Component getTableCellRendererComponent(	JTable table, Object color,	boolean isSelected, boolean hasFocus,	int row, int column) 
	{
		Component c = super.getTableCellRendererComponent(table, color, isSelected, hasFocus, row, column);	
		Object valueAt = table.getModel().getValueAt(row, column);
		String s = "";
		if (valueAt != null) 
		{
			s = valueAt.toString();
		}
		Color colorBack = new Color(255,255,240);
		if (s.equalsIgnoreCase("Cyan"))				
		{
			c.setForeground(Color.cyan.darker());				
			c.setBackground(colorBack);
		} else if(s.equalsIgnoreCase("Pink")) 
		{
			c.setForeground(Color.pink.darker());				
			c.setBackground(colorBack);
		}
		else if (s.equalsIgnoreCase("Green"))
		{
			c.setForeground(Color.green.darker());				
			c.setBackground(colorBack);
		}
		else if (s.equalsIgnoreCase("Black"))
		{
			c.setForeground(Color.black.darker());				
			c.setBackground(colorBack);
		}
		else if (s.equalsIgnoreCase("Light Gray"))
		{
			c.setForeground(Color.lightGray.darker());				
			c.setBackground(colorBack);
		}
		else if (s.equalsIgnoreCase("Magenta"))
		{
			c.setForeground(Color.magenta.darker());				
			c.setBackground(colorBack);
		}
		else if (s.equalsIgnoreCase("Red"))
		{
			c.setForeground(Color.red.darker());				
			c.setBackground(colorBack);
		}
		else if (s.equalsIgnoreCase("Orange"))
		{
			c.setForeground(Color.orange.darker());				
			c.setBackground(colorBack);
		}
		else if(s.equalsIgnoreCase("Gray"))
		{
			//				Color col=Color.gray.darker();
			c.setForeground(Color.gray.darker());				
			c.setBackground(colorBack);				
		}
		else if(s.equalsIgnoreCase("Dark Gray"))
		{
			c.setForeground(Color.darkGray.darker());				
			c.setBackground(colorBack);
		}
		else
		{
			c.setForeground(Color.black);
			c.setBackground(colorBack);
		}
		//			LineBorder line= new LineBorder(,1,true);
		//			c.setBorder(BorderFactory.createTitledBorder(line));
		c.setFont(Utilities.CUSTOM_FONT);
		return c;
	}
}
