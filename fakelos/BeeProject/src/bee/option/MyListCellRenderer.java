package bee.option;

import java.awt.Color;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import bee.option.options.Utilities;

public class MyListCellRenderer<T> implements ListCellRenderer<T> 
{

	protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();

	@Override
	public Component getListCellRendererComponent(JList list,
			Object value, int index, boolean isSelected, boolean cellHasFocus) 
	{
		JLabel renderer=(JLabel) defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
		renderer.setText(Integer.toString(index));
		if(index>-1){
			renderer.setBackground(new Color(255,255,240));
			//				Color color=Utilities.COLORS[index];
			Color color=(Color)value;
			renderer.setForeground(color.darker());
			renderer.setText("  Page  "+ index +"   "+ Utilities.COLOR_NAMES[Utilities.getColorIndex(color)]);
			renderer.setFont(Utilities.CUSTOM_FONT);
			renderer.setVerticalAlignment(SwingConstants.CENTER);
			LineBorder line= new LineBorder(color,3,true);
			renderer.setBorder(BorderFactory.createTitledBorder(line));				
		}
		return renderer;
	}//getListCellRendererComponent
}
