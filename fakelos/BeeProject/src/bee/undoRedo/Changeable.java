package bee.undoRedo;

public interface Changeable
{
	void undo();
	void redo();
}
