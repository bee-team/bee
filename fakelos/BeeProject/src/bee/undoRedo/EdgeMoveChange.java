package bee.undoRedo;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import bee.controlers.LVControler;
import bee.option.options.Utilities;

public class EdgeMoveChange implements Changeable

{
	String actionCommand;
	List<int []> edgeIndex;
	Color toPage;
	Color[] fromPage;
	LVControler controler;
	boolean newPage;
	
	public EdgeMoveChange(List<int []> edgeIndex, Color toPage, Color[] fromPage, LVControler controler, boolean newPage)
	{
		this.actionCommand="Coloring edges";
		this.edgeIndex=edgeIndex;
		this.toPage=toPage;
		this.fromPage=fromPage;
		this.controler=controler;
		this.newPage=newPage;
		controler.addChangeable(this);
		this.print();
		if(edgeIndex.size()!=fromPage.length)
			System.err.println("error creating edge move Change");
	}

	@Override
    public void undo()
    {	
		if(fromPage.length>0)
		{
	    for(int i=0;i<fromPage.length;i++)
	    {
	    	List<int[]> edges=new ArrayList<>();
	    	edges.add(edgeIndex.get(i));
	    	controler.moveEdges(edges, fromPage[i]);
	    }
		}
	    if(newPage)
	    {
	    	controler.undoCreatePage(toPage);
	    }	    
    }

	@Override
    public void redo()
    {
	    controler.moveEdges(edgeIndex, toPage);	    
    }

	public String toString()
	{
		return actionCommand;
	}
	
	public void print()
	{
		System.out.println("-----------------EDGE MOVE-------------------");
		System.out.println("move to page "+Utilities.COLOR_NAMES[Utilities.getColorIndex(toPage)]);
		if(edgeIndex.isEmpty())
			return;
		for(int i=0;i<edgeIndex.size();i++)
		{
			System.out.print(Arrays.toString(edgeIndex.get(i)));
			System.out.println(" "+fromPage[i]);
		}
	}
}
