package bee.undoRedo;

import java.awt.Color;

import bee.controlers.LVControler;

public class CreatePageChange implements Changeable

{
	String actionCommand;
	Color pageColor;
	LVControler controler;
	
	public CreatePageChange(Color pageColor, LVControler controler)
	{
		this.actionCommand="Coloring edges";
		this.pageColor=pageColor;
		this.controler=controler;
		controler.addChangeable(this);
	}

	@Override
    public void undo()
    {		
	    
	    	controler.undoCreatePage(pageColor);
    }

	@Override
    public void redo()
    {
	    controler.createPage(pageColor, false);
    }

	public String toString()
	{
		return actionCommand;
	}
}
