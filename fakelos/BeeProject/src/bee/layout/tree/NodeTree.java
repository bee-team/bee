package bee.layout.tree;

import java.util.ArrayList;
import java.util.List;

import y.base.Node;

public class NodeTree<T>
{
	private NodeTree<T> parent;
	private List<NodeTree<T>> children;
	private T data;
	private int height;
	
	public NodeTree(T data)
	{
		this.parent=null;
		this.children=new ArrayList<NodeTree<T>>();
		this.data=data;
		height=0;
	}

	public NodeTree<T> getParent()
	{
		return parent;
	}

	public void setParent(NodeTree<T> parent)
	{
		this.parent = parent;
		parent.addChild(this);
		height=parent.getHeight()+1;
	}

	public int getHeight()
	{
		return height;
	}

	private void addChild(NodeTree<T> child)
    {
	    if(children.indexOf(child)<0)
	    {
	    	children.add(child);
	    	}
	    
    }

	public List<NodeTree<T>> getChildren()
	{
		return children;
	}

	public T getData()
	{
		return data;
	}
	
	public boolean isRoot()
	{
		return height==0;
	}
	
	public boolean isLeaf()
	{
		return children.isEmpty();
	}
	
	public String toString()
	{
		String result=data.toString()+" ";
		if(data instanceof Node)
		{
		result=((Node)data).index()+" ";
		}
		if(this.isLeaf())
			return result;
		for(int i=0;i<children.size();i++)
			result=result+children.get(i).toString()+", ";
		return result;
	
	}
	
	public List<T> getPathUpTo(T data)
	{
		List<T> path=new ArrayList<T>();
		boolean found=false;
		NodeTree<T> currentNode=this;
		while(!found &&currentNode!=null)
		{
			if(currentNode.getData()==data)
				found=true;
			path.add(currentNode.getData());
			currentNode=currentNode.getParent();
		}
		if(found)
		return path;
		return null;
	}
	
	public List<T> getPathUpTo(NodeTree<T> endNode)
	{
		return this.getPathUpTo(endNode.getData());
	}
	
	public List<T> getPathUpToRoot()
	{
		List<T> path=new ArrayList<T>();
		NodeTree<T> currentNode=this;
		while(currentNode!=null)
		{
			path.add(currentNode.getData());
			currentNode=currentNode.getParent();
		}
		return path;
	}
	
	
	public boolean isAncestorOf(T data)
	{
		if(this.getData()==data)
			return true;
		for(int i=0;i<this.children.size();i++)
		{
			if(children.get(i).isAncestorOf(data))
				return true;
		}
		return false;
	}
	
	public boolean isDescendantOf(T data)//strictly defined                       
	{
		NodeTree<T> currentNode=this.getParent();
		while(currentNode!=null)
		{
			if(currentNode.getData()==data)
				return true;
			currentNode=currentNode.getParent();
		}
		return false;
	}
	
	public boolean isAncestorOf(NodeTree<T> endNode)
	{
		return this.isAncestorOf(endNode.getData());
	}
	
	public boolean isDescendantOf(NodeTree<T> endNode)
	{
		return this.isDescendantOf(endNode.getData());
	}

//	public NodeTree<T>[] toArray()
//    {
//		List<NodeTree<T>> list=this.toList();
////		NodeTree<T>[]  result= Arrays.copyOf(list.toArray(), list.size(), NodeTree[].class);
//		NodeTree<T>[] result=new NodeTree<T>[list.size()]
//		 NodeTree<T>[]  result=(NodeTree<T>[]) list.toArray();
//		return result;
//	    
//    }
	
//	public T[] dataToArray()
//    {
//
//		 NodeTree<T>[]  nodeTreeArray=this.toArray();
//		 T[] result=(T[]) new Object[nodeTreeArray.length];
//		 for(int i=0;i<result.length;i++)
//		 {
//			 result[i]=nodeTreeArray[i].data;
//		 }
//		return result;
//	    
//    }
	
	public List<NodeTree<T>> toList()
    {
	    if(this.isLeaf())
	    {
	    	 List<NodeTree<T>>  result=new ArrayList<>();
	    	result.add(this);
	    	return result;
	    }
	    List<NodeTree<T>>  result=new ArrayList<>();
    	result.add(this);
    	for(int i=0;i<this.children.size();i++)
    	{
    		List<NodeTree<T>> childList=this.children.get(i).toList();
    		for(int j=0;j<childList.size();j++)
    		{
    			result.add(childList.get(j));
    		}
    	}
    	return result;
	    
    }
	
	public List<T> toDataList()
    {
	    if(this.isLeaf())
	    {
	    	 List<T>  result=new ArrayList<>();
	    	result.add(this.data);
	    	return result;
	    }
	    List<T>  result=new ArrayList<>();
    	result.add(this.data);
    	for(int i=0;i<this.children.size();i++)
    	{
    		 List<T> childList=this.children.get(i).toDataList();
    		for(int j=0;j<childList.size();j++)
    		{
    			result.add(childList.get(j));
    		}
    	}
    	return result;
	    
    }
}
