package bee.layout;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import y.base.Edge;
import y.base.EdgeCursor;
import y.base.EdgeList;
import y.base.Graph;
import y.base.Node;
import y.layout.hierarchic.EdgeReverser;
import y.view.EdgeRealizer;
import y.view.Graph2D;
import bee.layout.tree.NodeTree;

public class PlanarLayouter
{
	Graph2D graph;
	public PlanarLayouter(Graph2D graph)
	{
		this.graph=graph;
	}

	public Graph2D planarizeGraph()
	{
		try{
			//			JFrame log=new JFrame("Planarize log file");
			//			JScrollPane scroll=new JScrollPane();
			//
			//			JTextArea textArea=new JTextArea();
			//			scroll.add(textArea);
			//			scroll.setVisible(true);
			//			log.getContentPane().add(scroll);
			//			log.pack();
			//			//			log.setSize(400, 400);
			//			log.setMinimumSize(new Dimension(400,400));
			////			log.setResizable(false);
			//			log.setVisible(true);
			//			textArea.setVisible(true);
			//			textArea.setMinimumSize(new Dimension(350,350));
			//			textArea.setColumns(100);
			//			textArea.setEditable(false);
			//			textArea.setBackground(Color.WHITE);
			//			textArea.setBorder(BorderFactory.createEtchedBorder());
			//			textArea.setForeground(Color.BLUE);
			//			textArea.setFont(new Font("Serif", Font.BOLD, 15));
			//			textArea.setText("//PLANARIZING STARTED...");

			//			textArea.setForeground(Color.BLACK);
			//			textArea.setFont(new Font("Serif", Font.PLAIN, 12));

			System.out.println("//PLANARIZING STARTED...");
			Graph2D tempGraph=(Graph2D) graph.createCopy();
			GraphEnriched richGraph=new GraphEnriched(tempGraph);
			//		richGraph.computeFields();
			//		Node[] nodeDFS=createDFS(tempGraph);
			//			List<NodeTree<NodeEnriched>> nodeDFS=richGraph.getNodeDFS();
			//		System.out.println(Arrays.toString(nodeDFS.));
			//			for(int i=0;i<nodeDFS.size();i++)
			//			{
			//				System.out.println(nodeDFS.get(i).getData().node.index()+":");
			//				System.out.print(nodeDFS.get(i));
			//				System.out.println();
			//			}
			//			System.out.println();
//			NodeEnriched[] nodeArray=richGraph.getArrayDFS();
//			for(int i=0;i<nodeArray.length;i++)
//			{
//				int nodeIndex=nodeArray[i].node.index();
//				System.out.println("\n\n-------------------------------\n-------------------------------");
//				System.out.println("NODE "+nodeIndex+"\n-------------------------------");
//				List<EdgeEnriched> lowEdges=nodeArray[i].lowEdges;
//				System.out.print("low edges: {  ");
//				for(int j=0;j<lowEdges.size();j++)
//				{
//					System.out.print(lowEdges.get(j).getSource()+"->"+lowEdges.get(j).getTarget()+"  ");
//				}
//				System.out.println("}\n-------------------------------");
				//			List<EdgeEnriched> edge=nodeArray[i].outEdges;
				//			for(int j=0;j<edge.size();j++)
				//			{
				//				System.out.println(edge.get(j).getSource()+"->"+edge.get(j).getTarget()+" ("+edge.get(j).isTreeEdge()+")  "+edge.get(j).getType());
				//				System.out.println("     low Node: "+edge.get(j).getLowNode());
				//				System.out.println("     fringe   :");
				//				List<EdgeEnriched> fringe=edge.get(j).getFringe();
				//				for(int k=0;k<fringe.size();k++)
				//				{
				//					System.out.println("           "+fringe.get(k).getSource()+"->"+fringe.get(k).getTarget());
				//				}
				//				System.out.println("==============");
				//				//				System.out.println(edge.get(j).getEdge().source().index()+"->"+edge.get(j).getEdge().target().index()+" ("+edge.get(j).isTreeEdge()+")");
				//				if(edge.get(j).getSource().node!=edge.get(j).getEdge().source() ||
				//						edge.get(j).getTarget().node!=edge.get(j).getEdge().target())
				//				{
				//					System.out.println("MAJOR ERROR: Invalid source-target");
				//				}
				//				if(edge.get(j).edge.source().index()!=nodeIndex)
				//				{
				//					System.out.println("MAJOR ERROR: node-"+nodeIndex+", edge "+edge.get(j).edge.source().index()+"->"+edge.get(j).edge.target().index());
				//				}
				//			}
//				List<List<EdgeEnriched>> orderedEdges=nodeArray[i].getOrderedOutEdges();
//				System.out.println("=== ORDERING ===");
//				for(int j=0;j<orderedEdges.size();j++)
//				{
//					List<EdgeEnriched> sameEdges=orderedEdges.get(j);
//					for(int k=0;k<sameEdges.size();k++)
//					{
//						EdgeEnriched e=sameEdges.get(k);
//						System.out.print(e.getSource()+"->"+e.getTarget()+"  ");
//					}
//					System.out.print("<   ");
//				}
//			}
			//			System.out.println("\n==============");
			//			for(int j=0;j<nodeArray[i].outEdges.size();j++)
			//			{
			//				EdgeEnriched e=nodeArray[i].outEdges.get(j);
			//				System.out.println("EDGE "+e.getSource()+"-->"+e.getTarget());
			//				e.dataStructure.print();
			//				System.out.println("_________________");
			//			}
			System.out.println("\n\n\n//Create auxiliary graph...");
			AuxiliaryGraph auxiliaryGraph=new AuxiliaryGraph(richGraph);
			//			HashMap<Node, Integer> isBipartite=PlanarLayouter.isBipartite(auxiliaryGraph);
			//			System.out.println(isBipartite!=null);
			HashMap<EdgeEnriched, Integer> map=auxiliaryGraph.generateEdgePartition();
			Set<Entry<EdgeEnriched, Integer>> edgeMappingSet=map.entrySet();
			for (Iterator<Entry<EdgeEnriched, Integer>> iter=edgeMappingSet.iterator();iter.hasNext();)
			{
				Entry<EdgeEnriched, Integer> entry=iter.next();
				EdgeEnriched edgeRich=entry.getKey();
				Integer edgeColor=entry.getValue();
				Edge edge=edgeRich.getEdge();
				EdgeRealizer er=richGraph.getGraph().getRealizer(edge);
				if(er.getLineColor()==Color.RED && edgeColor==-1)
				{
					er.setLineColor(Color.ORANGE);
				}

				if(er.getLineColor()==Color.BLACK && edgeColor==-1)
				{
					er.setLineColor(Color.BLUE);
				}
			}



			return tempGraph;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}



	private static HashMap<Node, Integer> isBipartite(Graph auxiliaryGraph)
	{
		Node[] nodes=auxiliaryGraph.getNodeArray();
		HashMap<Node, Integer> nodeMap=new HashMap<>();
//		boolean bipartite=true;
		boolean finished=false;
		List<Node> nodesToCheck=new ArrayList<>();
		while(!finished)
		{
			if(nodesToCheck.isEmpty())
			{
				boolean found=false;
				for(int i=0;i<nodes.length&&!found;i++)
				{
					Node tempNode=nodes[i];
					if(!nodeMap.containsKey(tempNode))
					{
						nodeMap.put(tempNode, new Integer(1));
						nodesToCheck.add(tempNode);
						found=true;
					}
				}
				if(!found)
				{
					finished=true;
				}
			}
			else
			{
				Node currentNode=nodesToCheck.remove(0);
				EdgeCursor ec=currentNode.edges();
				for(ec.toFirst();ec.ok();ec.next())
				{
					Edge edge=ec.edge();
					Node source=edge.source();
					Node target=edge.target();
					if(source!=currentNode)
					{
						Node temp=source;
						source=target;
						target=temp;
					}
					int sourceColor=nodeMap.get(source).intValue();
					if(!nodeMap.containsKey(target))
					{
						nodeMap.put(target, new Integer(-sourceColor));
						nodesToCheck.add(target);
					}
					else{
						int targetColor=nodeMap.get(target).intValue();
						if(targetColor+sourceColor!=0)
							return null;
					}
				}
			}
		}
		return nodeMap;
	}


	private class AuxiliaryGraph extends Graph
	{
		private GraphEnriched graph;
		Map<EdgeEnriched,Node> edgeNodeMap;
		//		Map<List<EdgeEnriched>,Node> constraintNodeMap;
		//		Map<NodeEnriched,Node> nodeNodeMap;

		public AuxiliaryGraph(GraphEnriched graph)
		{
			super();
			this.graph=graph;
			edgeNodeMap=new HashMap<>();

			createAuxiliaryGraph();
		}

		private void createAuxiliaryGraph()
		{
			Map<List<EdgeEnriched>,Node> constraintNodeMap=new HashMap<>();
			Map<NodeEnriched,Node> nodeNodeMap=new HashMap<>();
			List<List<EdgeEnriched>[]> allConstraints=new ArrayList<>();
			NodeEnriched[] arrayDFS=graph.getArrayDFS();
			for(int i=0;i<arrayDFS.length;i++)
			{
				List<EdgeEnriched> outEdges=arrayDFS[i].outEdges;
				for(int j=0;j<outEdges.size();j++)
				{
					EdgeEnriched edge=outEdges.get(j);
					if(!edge.isTreeEdge())
					{
						Node edgeNode=this.createNode();
						edgeNodeMap.put(edge, edgeNode);
					}
					allConstraints.addAll(edge.dataStructure.constraints);
					//					System.out.println(edgeNode.index()+": "+edge.toString());
				}
			}
			for(int i=0;i<allConstraints.size();i++)
			{
				List<EdgeEnriched>[] constraint=allConstraints.get(i);
				List<EdgeEnriched> left=constraint[0];
				List<EdgeEnriched> right=constraint[1];
				Node leftNode=this.createNode();
				constraintNodeMap.put(left, leftNode);
				Node rightNode=this.createNode();
				constraintNodeMap.put(right, rightNode);
				this.createEdge(leftNode, rightNode);
				//				System.out.println(leftNode.index()+": ");
				for(int j=0;j<left.size();j++)
				{
					Node temp=edgeNodeMap.get(left.get(j));
					this.createEdge(leftNode, temp);
					//					System.out.println("    "+left.get(j));
				}
				System.out.println(rightNode.index()+": ");
				for(int j=0;j<right.size();j++)
				{
					Node temp=edgeNodeMap.get(right.get(j));
					this.createEdge(rightNode, temp);
					//					System.out.println("    "+right.get(j));
				}

			}
			for(int i=0;i<arrayDFS.length;i++)
			{
				NodeEnriched vertex=arrayDFS[i];

				List<EdgeEnriched> lowEdges=vertex.lowEdges;
				Node vertexNode=this.createNode();
				nodeNodeMap.put(vertex, vertexNode);
				for(int j=0;j<lowEdges.size();j++)
				{
					Node edgeNode=edgeNodeMap.get(lowEdges.get(j));
					this.createEdge(vertexNode, edgeNode);
				}				
			}
		}

		public HashMap<EdgeEnriched, Integer> generateEdgePartition()
		{
			HashMap<Node, Integer> nodeColors=isBipartite(this);
			HashMap<EdgeEnriched, Integer> resultMap=new HashMap<>();
			Set<Entry<EdgeEnriched, Node>> edgeMappingSet=this.edgeNodeMap.entrySet();
			for (Iterator<Entry<EdgeEnriched, Node>> iter=edgeMappingSet.iterator();iter.hasNext();)
			{
				Entry<EdgeEnriched, Node> entry=iter.next();
				EdgeEnriched edge=entry.getKey();
				Node auxNode=entry.getValue();
				Integer edgeColor=nodeColors.get(auxNode);
				System.out.println("Node "+auxNode.index()+": "+edge.toString()+" color: "+edgeColor);
				if(edge.isTreeEdge())
					System.out.println("     TREE EDGE ERROR");
				resultMap.put(edge,edgeColor);
			}//done with back edges
			System.out.println("Tree edges");
			NodeEnriched[] allNodes=this.graph.arrayDFS;
			for(int i=0;i<allNodes.length;i++)
			{
				NodeEnriched node=allNodes[i];
				List<EdgeEnriched> edges=node.outEdges;
				for(int j=0;j<edges.size();j++)
				{
					EdgeEnriched edge=edges.get(j);
					if(edge.isTreeEdge())
					{
						System.out.println(edge+" "+edge.getUpEdge());
						if(edge.getUpEdge()!=null)
						{
							Node auxNode=this.edgeNodeMap.get(edge.getUpEdge());
							Integer edgeColor=nodeColors.get(auxNode);
							resultMap.put(edge,edgeColor);
							System.out.println("tree edge "+edge+" "+" color "+edgeColor);
						}
						else
							resultMap.put(edge, 1);
						System.out.println("tree edge "+edge+" "+" color "+1);
					}
				}
			}
			return resultMap;
		}

	}


	private class GraphEnriched
	{
		private Graph2D graph;
		private  List<NodeTree<NodeEnriched>> nodeDFS;
		private NodeEnriched[] arrayDFS;

		//		private  List<List<EdgeEnriched>> enrichedEdges;

		public Graph2D getGraph()
		{
			return graph;
		}

		public NodeEnriched[] getArrayDFS()
		{
			return arrayDFS;
		}


		public List<NodeTree<NodeEnriched>> getNodeDFS()
		{
			return nodeDFS;
		}

		//		public List<List<EdgeEnriched>> getEnrichedEdges()
		//		{
		//			return enrichedEdges;
		//		}



		public GraphEnriched(Graph2D graph)
		{
			System.out.println("//\t Creating Enriched Graph");
			this.graph=graph;
			this.computeFields();
		}

		private void computeFields()
		{
			System.out.println("\n\n//\t Computing Fields");
			System.out.println("\n\n//\t\t DFS");
			this.createDFS();

			System.out.println("\n\n//\t\t low nodes");
			this.computeLowNodes();

			System.out.println("\n\n//\t\t fringe");
			this.computeFringe();
			System.out.println("\n\n//\t\t type of edges");
			this.computeType();
			System.out.println("\n\n//\t\t edge ordering");
			this.computeEdgeOrdering();
			System.out.println("\n\n//\t\t constraints");
			this.computeConstraints();
			System.out.println("\n\n//\t\t low edges");
			this.computeLowEdges();
			System.out.println("\n\n//\t\t up edges");
			this.computeUpEdges();
		}

		private void createDFS()
		{
			this.nodeDFS=new ArrayList<>();
			//			this.enrichedEdges=new ArrayList<>();
			boolean[] visited=new boolean[this.graph.nodeCount()];
			boolean[] oriented=new boolean[this.graph.edgeCount()];
			EdgeList edgelist=this.graph.getEdgeList();
			for(int i=0;i<visited.length;i++)
				visited[i]=false;
			for(int i=0;i<oriented.length;i++)
				oriented[i]=false;
			Node[] nodeArray=this.graph.getNodeArray();//assume the nodeArray is sorted based on index

			Arrays.sort(nodeArray, new Comparator<Node>(){

				@Override
				public int compare(Node arg0, Node arg1)
				{
					if(arg0.index()<arg1.index())
						return -1;
					return 1;
				}

			});
			//			for(int i=0;i<nodeArray.length;i++)
			//				System.out.print(nodeArray[i].index()+", ");
			//			System.out.println();

			visited[0]=true;
			this.arrayDFS=new NodeEnriched[visited.length];

			Node currentNode=nodeArray[0];
			NodeEnriched currentEnrichedNode=new NodeEnriched(currentNode);
			NodeTree<NodeEnriched> treeNode=new NodeTree<NodeEnriched>(currentEnrichedNode);
			currentEnrichedNode.setTreeNode(treeNode);
			//			NodeTree<NodeEnriched> root=treeNode;
			//			this.nodeDFS.add(root);
			this.nodeDFS.add(treeNode);
			//			List<EdgeEnriched> currentEdges=new ArrayList<>();
			//			List<EdgeEnriched> tempCurrentEdges=currentEdges;
			//			this.enrichedEdges.add(tempCurrentEdges);
			//			System.out.println("Current Node="+currentNode.index());
			this.arrayDFS[0]=currentEnrichedNode;
			int pos=1;
			boolean finished=false;
			EdgeList edgesToReverse=new EdgeList();
			while(!finished)
			{
				EdgeCursor ec=currentNode.edges();
				//				EdgeCursor ec1=currentNode.edges();
				//				for(ec1.toFirst();ec1.ok();ec1.next())
				//				{
				//					System.out.println("       "+ec1.edge().source().index()+"->"+ec1.edge().target().index());
				//				}
				boolean moved=false;
				for(ec.toFirst();ec.ok()&&(!moved);ec.next())
				{
					Edge edge=ec.edge();
					int edgeIndex=edgelist.indexOf(edge);
					if(!oriented[edgeIndex])
					{
						int source=edge.source().index();
						int target=edge.target().index();
						//						System.out.println("               marking edge"+source+"->"+target);
						oriented[edgeIndex]=true;
						EdgeEnriched enrichedEdge=new EdgeEnriched(edge);
						currentEnrichedNode.addOutEdge(enrichedEdge);
						//						currentEdges.add(enrichedEdge);
						if(!visited[target])
						{
							enrichedEdge.setSource(currentEnrichedNode);
							//							currentEnrichedNode.addOutEdge(enrichedEdge);
							//							System.out.println("coloring edge");
							currentNode=edge.target();
							//							System.out.println("Current Node="+currentNode.index());
							currentEnrichedNode=new NodeEnriched(currentNode);
							enrichedEdge.setTarget(currentEnrichedNode);
							this.arrayDFS[pos]=currentEnrichedNode;
							visited[target]=true;
							pos++;
							NodeTree<NodeEnriched> newTreeNode=new NodeTree<NodeEnriched>(currentEnrichedNode);
							currentEnrichedNode.setTreeNode(newTreeNode);
							newTreeNode.setParent(treeNode);
							treeNode=newTreeNode;
							moved=true;
							EdgeRealizer er=this.graph.getRealizer(edge);
							er.setLineColor(Color.RED);
							enrichedEdge.setTreeEdge(true);

						}
						else if(!visited[source])
						{
							enrichedEdge.setSource(currentEnrichedNode);
							//							System.out.println("coloring edge + reversing");
							//							currentEnrichedNode.addOutEdge(enrichedEdge);
							currentNode=edge.source();
							edgesToReverse.add(edge);
							currentEnrichedNode=new NodeEnriched(currentNode);
							enrichedEdge.setTarget(currentEnrichedNode);
							this.arrayDFS[pos]=currentEnrichedNode;
							//							System.out.println("Current Node="+currentNode.index());
							pos++;
							NodeTree<NodeEnriched> newTreeNode=new NodeTree<NodeEnriched>(currentEnrichedNode);
							currentEnrichedNode.setTreeNode(newTreeNode);
							newTreeNode.setParent(treeNode);
							treeNode=newTreeNode;
							visited[source]=true;
							moved=true;
							EdgeRealizer er1=this.graph.getRealizer(edge);
							er1.setLineColor(Color.RED);
							enrichedEdge.setTreeEdge(true);
						}						
						else
						{
							//							System.out.println("back edge ..."+source+"->"+target);
							enrichedEdge.setTreeEdge(false);
							enrichedEdge.setSource(currentEnrichedNode);
							if(source!=currentNode.index())
							{
								Node tempNode=edge.source();
								for(int it=0;it<pos;it++)
								{
									NodeEnriched toCheck=this.arrayDFS[it];
									if(toCheck.node==tempNode)
									{
										enrichedEdge.setTarget(toCheck);
										break;
									}
								}
								edgesToReverse.add(edge);
							}
							else
							{
								Node tempNode=edge.target();
								for(int it=0;it<pos;it++)
								{
									NodeEnriched toCheck=this.arrayDFS[it];
									if(toCheck.node==tempNode)
									{
										enrichedEdge.setTarget(toCheck);
										break;
									}
								}
							}

						}
					}
				}
				if(!moved)
				{
					if(pos<visited.length)//we are not finished
					{
						if(treeNode.isRoot())
						{
							for(int i=0;i<visited.length;i++)
							{
								if(!visited[i])
								{
									currentNode=nodeArray[i];
									currentEnrichedNode=new NodeEnriched(currentNode);
									treeNode=new NodeTree<NodeEnriched>(currentEnrichedNode);
									currentEnrichedNode.setTreeNode(treeNode);
									//									root=treeNode;
									//									this.nodeDFS.add(root);
									this.nodeDFS.add(treeNode);
									//									currentEdges=new ArrayList<>();
									//									tempCurrentEdges=currentEdges;
									//									this.enrichedEdges.add(tempCurrentEdges);
									//									System.out.println("Current Node="+currentNode.index());
									this.arrayDFS[pos]=currentEnrichedNode;
									visited[i]=true;
									pos++;
									break;
								}
							}
						}
						else{
							//							System.out.println("STEP BACK");
							treeNode=treeNode.getParent();
							currentEnrichedNode=treeNode.getData();
							currentNode=currentEnrichedNode.node;
						}
					}
					else
					{
						finished=true;
					}
				}
			}
			EdgeReverser er=new EdgeReverser();
			er.reverseEdges(this.graph, edgesToReverse);
		}

		private void computeLowNodes()
		{
			NodeEnriched[] nodeLowNodes=new NodeEnriched[arrayDFS.length];
			for(int i=0;i<arrayDFS.length;i++)
			{
				List<EdgeEnriched> outEdges=arrayDFS[i].outEdges;
				for(int j=0;j<outEdges.size();j++)
				{
					if(!outEdges.get(j).isTreeEdge())
					{
						outEdges.get(j).lowNode=outEdges.get(j).getTarget();
					}
				}
			}

			for(int i=arrayDFS.length-1;i>=0;i--)
			{
				NodeEnriched currentNode=arrayDFS[i];
				//				System.out.println("NODE "+currentNode);
				//				System.out.println("compare nodes ");
				List<EdgeEnriched> outEdges=currentNode.outEdges;
				if(!outEdges.isEmpty())
				{
					if(outEdges.get(0).isTreeEdge())
					{
						NodeEnriched target=outEdges.get(0).getTarget();
						int targetIndex=arrayDFS.length;
						for(int k=0;k<arrayDFS.length;k++)
						{
							if(arrayDFS[k]==target)
							{
								targetIndex=k;
								break;
							}
						}
						outEdges.get(0).setLowNode(nodeLowNodes[targetIndex]);
					}
					NodeEnriched lowerNode=outEdges.get(0).getLowNode();
					//					System.out.println("               "+lowerNode);
					int lowerIndex=arrayDFS.length;
					for(int k=0;k<arrayDFS.length;k++)
					{
						if(arrayDFS[k]==lowerNode)
						{
							lowerIndex=k;
							break;
						}
					}
					if(lowerIndex>i)
					{
						lowerIndex=i;
						lowerNode=currentNode;
					}
					for(int j=1;j<outEdges.size();j++)
					{
						if(outEdges.get(j).isTreeEdge())
						{
							NodeEnriched target=outEdges.get(j).getTarget();
							int targetIndex=arrayDFS.length;
							for(int k=0;k<arrayDFS.length;k++)
							{
								if(arrayDFS[k]==target)
								{
									targetIndex=k;
									break;
								}
							}
							outEdges.get(j).setLowNode(nodeLowNodes[targetIndex]);
						}
						NodeEnriched lowerNewNode=outEdges.get(j).getLowNode();
						//						System.out.println("               "+lowerNewNode);
						int lowerNewIndex=arrayDFS.length;
						for(int k=0;k<arrayDFS.length;k++)
						{
							if(arrayDFS[k]==lowerNewNode)
							{
								lowerNewIndex=k;
								break;
							}
						}
						if(lowerNewIndex<lowerIndex)
						{
							lowerNode=lowerNewNode;
							lowerIndex=lowerNewIndex;
						}
					}
					nodeLowNodes[i]=lowerNode;
				}
				else
				{
					nodeLowNodes[i]=arrayDFS[i];
				}
			}
			for(int i=0;i<nodeLowNodes.length;i++)
			{
				System.out.println("Node "+arrayDFS[i].toString() +" reaches node "+nodeLowNodes[i].toString());
			}
		}



		private void computeFringe()
		{
			for(int i=0;i<arrayDFS.length;i++)
			{
				List<EdgeEnriched> outEdges=arrayDFS[i].outEdges;
				for(int j=0;j<outEdges.size();j++)
				{
					EdgeEnriched edge=outEdges.get(j);
					if(!edge.isTreeEdge())
					{						
						List<EdgeEnriched> fringe=new ArrayList<>();
						fringe.add(edge);
						edge.setFringe(fringe);
					}
					else
					{
						List<EdgeEnriched> fringe=new ArrayList<>();
						NodeTree<NodeEnriched> nodeTree=edge.getTarget().getTreeNode();
						List<NodeEnriched> subtreeNodes=nodeTree.toDataList();
						for(int k=0;k<subtreeNodes.size();k++)
						{
							NodeEnriched tempNode=subtreeNodes.get(k);
							List<EdgeEnriched> tempOutEdges=tempNode.outEdges;
							for(int m=0;m<tempOutEdges.size();m++)
							{
								EdgeEnriched tempEdge=tempOutEdges.get(m);
								if(!tempEdge.isTreeEdge())
								{
									NodeEnriched lowNode=tempEdge.getLowNode();
									if(edge.getSource().getTreeNode().isDescendantOf(lowNode.getTreeNode()))
									{
										fringe.add(tempEdge);
									}
								}
							}
						}
						edge.setFringe(fringe);
					}
					System.out.println("edge:"+edge);
					for(int s=0;s<edge.getFringe().size();s++)
					{
						System.out.println("     "+edge.getFringe().get(s));
					}
				}
			}
		}

		private void computeType()
		{
			for(int i=0;i<arrayDFS.length;i++)
			{
				List<EdgeEnriched> outEdges=arrayDFS[i].outEdges;
				for(int j=0;j<outEdges.size();j++)
				{
					EdgeEnriched edge=outEdges.get(j);
					if(!edge.isTreeEdge())
					{
						edge.setType(EdgeEnriched.NONE);
					}
					else if(edge.getFringe().isEmpty())
					{
						edge.setType(EdgeEnriched.BLOCK);
					}
					else
					{
						List<EdgeEnriched> fringe=edge.getFringe();
						NodeEnriched low=fringe.get(0).getLowNode();
						for(int k=1;k<fringe.size();k++)
						{
							if(low!=fringe.get(k).getLowNode())
							{
								edge.setType(EdgeEnriched.THICK);
								break;
							}
						}
						if(edge.getType()<0)
						{
							edge.setType(EdgeEnriched.THIN);
						}
					}
					System.out.println("edge: "+edge.toRichString());
				}
			}
		}

		private void computeEdgeOrdering()
		{
			for(int i=0;i<arrayDFS.length;i++)
			{
				List<List<EdgeEnriched>> order=new LinkedList<List<EdgeEnriched>>();
				List<EdgeEnriched> outEdges=arrayDFS[i].outEdges;
				for(int j=0;j<outEdges.size();j++)
				{
					EdgeEnriched edge=outEdges.get(j);

					if(order.isEmpty())
					{
						List<EdgeEnriched> lowSet=new ArrayList<>();
						lowSet.add(edge);
						order.add(lowSet);
					}
					else
					{
						NodeEnriched low=edge.getLowNode();
						boolean stop=false;
						int position=0;
						while(!stop && position<order.size())
						{
							NodeEnriched tempLow=order.get(position).get(0).getLowNode();
							if(low==tempLow)
							{
								EdgeEnriched firstEdge=order.get(position).get(0);
								int firstType=firstEdge.getType();
								int type=edge.getType();
								if(type==EdgeEnriched.THICK && firstType!=EdgeEnriched.THICK)
								{
									position++;
									List<EdgeEnriched> lowSet=new ArrayList<>();
									lowSet.add(edge);
									order.add(position,lowSet);
									stop=true;
								}
								else if(firstType==EdgeEnriched.THICK && type!=EdgeEnriched.THICK)
								{
									List<EdgeEnriched> lowSet=new ArrayList<>();
									lowSet.add(edge);
									order.add(position,lowSet);
									stop=true;
								}
								else
								{
									order.get(position).add(edge);
									stop=true;
								}								
							}
							else
							{
								if(tempLow.getTreeNode().isAncestorOf(low))
								{
									position++;
								}
								else if(low.getTreeNode().isAncestorOf(tempLow))
								{
									List<EdgeEnriched> lowSet=new ArrayList<>();
									lowSet.add(edge);
									order.add(position,lowSet);
									stop=true;
								}
								else
								{
									order.get(position).add(edge);
									stop=true;
								}
							}
						}
						if(!stop && position==order.size())
						{
							List<EdgeEnriched> lowSet=new ArrayList<>();
							lowSet.add(edge);
							order.add(lowSet);
						}
					}
				}
				arrayDFS[i].setOrderedOutEdges(order);
				System.out.println("Node: "+arrayDFS[i].toString());
				for(int s=0;s<order.size();s++)
				{
					System.out.print("     ");
					for(int m=0;m<order.get(s).size();m++)
					{
						System.out.print(order.get(s).get(m)+"   ");
					}
					System.out.println();
				}
			}

		}

		private void computeConstraints()
		{
			for(int i=arrayDFS.length-1;i>=0;i--)
			{

				NodeEnriched sourceNode=arrayDFS[i];
				System.out.println("Node "+sourceNode);
				List<EdgeEnriched> outEdges=sourceNode.outEdges;
				for(int j=0;j<outEdges.size();j++)
				{
					EdgeEnriched edge=outEdges.get(j);
					if(edge.isTreeEdge())
					{
						//process target node
						NodeEnriched processingNode=edge.getTarget();
						List<List<EdgeEnriched>>orderedOutEdges=processingNode.getOrderedOutEdges();
						if(!orderedOutEdges.isEmpty())
						{
							//add first edges
							List<EdgeEnriched> addingEdges=orderedOutEdges.get(0);
							for(int m=0;m<addingEdges.size();m++)
							{
								edge.dataStructure.addDSEdge(addingEdges.get(m).dataStructure.getDSEdges());
							}
							for(int k=1;k<orderedOutEdges.size();k++)
							{
								addingEdges=orderedOutEdges.get(k);
								for(int m=0;m<addingEdges.size();m++)
								{
									edge.dataStructure.addDSEdge(addingEdges.get(m).dataStructure.getDSEdges());
									//add constraints
									for(int it=0;it<k;it++)
									{
										List<EdgeEnriched> previousEdges=orderedOutEdges.get(it);
										for(int s=0;s<previousEdges.size();s++)
										{
											List<EdgeEnriched> fop1=addingEdges.get(m).computeFop(previousEdges.get(s));
											List<EdgeEnriched> fop2=previousEdges.get(s).computeFop(addingEdges.get(m));
											if((fop1.size()+fop2.size())>1)
											{
												List<?>[] constraint={fop1, fop2};
												edge.dataStructure.addConstraint((List<EdgeEnriched>[]) constraint);
											}
										}
									}
								}
							}
							for(int t=0;t<edge.dataStructure.getDSEdges().size();t++)
							{
								EdgeEnriched tempEdge=edge.dataStructure.getDSEdges().get(t);
								if((!tempEdge.isTreeEdge())&&tempEdge.getLowNode()==sourceNode)
								{
									edge.dataStructure.deleteDSEdge(tempEdge);
								}
							}
						}
						System.out.print("edge "+edge+"\nedges: {");
						for(int s=0;s<edge.dataStructure.getDSEdges().size();s++)
						{
							System.out.print(" "+edge.dataStructure.getDSEdges().get(s)+" ");
						}
						System.out.print("}\nconstraints:\n");
						for(int s=0;s<edge.dataStructure.constraints.size();s++)
						{
							System.out.print("{");
							for(int m=0;m<edge.dataStructure.constraints.get(s)[0].size();m++)
							{
								System.out.print(" "+edge.dataStructure.constraints.get(s)[0].get(m)+" ");
							}
							System.out.print("}=|={");
							for(int m=0;m<edge.dataStructure.constraints.get(s)[1].size();m++)
							{
								System.out.print(" "+edge.dataStructure.constraints.get(s)[1].get(m)+" ");
							}
							System.out.println("}");
						}
					}
				}
			}
		}

		private void computeLowEdges()
		{
			for(int i=arrayDFS.length-1;i>=0;i--)
			{
				NodeEnriched processingNode=arrayDFS[i];
				for(int j=0;j<processingNode.outEdges.size();j++)
				{
					EdgeEnriched processingEdge=processingNode.outEdges.get(j);
					if(!processingEdge.isTreeEdge())
					{
						NodeEnriched checkingLowNode=processingEdge.getLowNode();
						List<NodeEnriched> path=processingNode.getTreeNode().getPathUpToRoot();
						for(int k=0;k<path.size();k++)
						{
							NodeEnriched updateNode=path.get(k);
							if(updateNode.lowEdges.isEmpty())
							{
								updateNode.lowEdges.add(processingEdge);
							}
							else if(updateNode.lowEdges.get(0).getLowNode()==checkingLowNode)
							{
								updateNode.lowEdges.add(processingEdge);
							}
							else
							{
								NodeEnriched currentLowNode=updateNode.lowEdges.get(0).getLowNode();
								if(currentLowNode.getTreeNode().isDescendantOf(checkingLowNode))
								{
									updateNode.lowEdges.clear();
									updateNode.lowEdges.add(processingEdge);
								}
							}
						}
					}
				}
			}
			for(int i=0;i<arrayDFS.length;i++)
			{
				System.out.print("Node "+arrayDFS[i]+"\n    ");
				for(int j=0;j<arrayDFS[i].lowEdges.size();j++)
				{
					System.out.print(" "+arrayDFS[i].lowEdges.get(j)+" ");
				}
				System.out.println();
			}

		}

		private void computeUpEdges()
		{
			for(int i=arrayDFS.length-1;i>=0;i--)
			{
				NodeEnriched processingNode=arrayDFS[i];
				for(int j=0;j<processingNode.outEdges.size();j++)
				{
					EdgeEnriched processingEdge=processingNode.outEdges.get(j);
					if(processingEdge.isTreeEdge())
					{
						List<EdgeEnriched> fringe=processingEdge.getFringe();
						if(!fringe.isEmpty())
						{
							EdgeEnriched upEdge=fringe.get(0);
							NodeEnriched lowNode=upEdge.lowNode;
							for(int k=1;k<fringe.size();k++)
							{
								EdgeEnriched tempEdge=fringe.get(k);
								NodeEnriched tempNode=tempEdge.getLowNode();
								if(tempNode.getTreeNode().isDescendantOf(lowNode))
								{
									lowNode=tempNode;
									upEdge=tempEdge;
								}
							}
							processingEdge.setUpEdge(upEdge);
						}
						System.out.println(processingEdge+" "+processingEdge.getUpEdge());

					}
				}
			}
		}


		//		public Graph createAuxiliaryGraph()
		//		{
		//			Graph auxiliaryGraph=new Graph();
		//			Map<EdgeEnriched,Node> edgeNodeMap=new HashMap<>();
		//			Map<List<EdgeEnriched>,Node> constraintNodeMap=new HashMap<>();
		//			Map<NodeEnriched,Node> nodeNodeMap=new HashMap<>();
		//			List<List<EdgeEnriched>[]> allConstraints=new ArrayList<>();
		//			for(int i=0;i<arrayDFS.length;i++)
		//			{
		//				List<EdgeEnriched> outEdges=arrayDFS[i].outEdges;
		//				for(int j=0;j<outEdges.size();j++)
		//				{
		//					EdgeEnriched edge=outEdges.get(j);
		//
		//					Node edgeNode=auxiliaryGraph.createNode();
		//					edgeNodeMap.put(edge, edgeNode);					
		//					allConstraints.addAll(edge.dataStructure.constraints);
		//					//					System.out.println(edgeNode.index()+": "+edge.toString());
		//				}
		//			}
		//			for(int i=0;i<allConstraints.size();i++)
		//			{
		//				List<EdgeEnriched>[] constraint=allConstraints.get(i);
		//				List<EdgeEnriched> left=constraint[0];
		//				List<EdgeEnriched> right=constraint[1];
		//				Node leftNode=auxiliaryGraph.createNode();
		//				constraintNodeMap.put(left, leftNode);
		//				Node rightNode=auxiliaryGraph.createNode();
		//				constraintNodeMap.put(right, rightNode);
		//				auxiliaryGraph.createEdge(leftNode, rightNode);
		//				//				System.out.println(leftNode.index()+": ");
		//				for(int j=0;j<left.size();j++)
		//				{
		//					Node temp=edgeNodeMap.get(left.get(j));
		//					auxiliaryGraph.createEdge(leftNode, temp);
		//					//					System.out.println("    "+left.get(j));
		//				}
		//				System.out.println(rightNode.index()+": ");
		//				for(int j=0;j<right.size();j++)
		//				{
		//					Node temp=edgeNodeMap.get(right.get(j));
		//					auxiliaryGraph.createEdge(rightNode, temp);
		//					//					System.out.println("    "+right.get(j));
		//				}
		//
		//			}
		//			for(int i=0;i<arrayDFS.length;i++)
		//			{
		//				NodeEnriched vertex=arrayDFS[i];
		//
		//				List<EdgeEnriched> lowEdges=vertex.lowEdges;
		//				Node vertexNode=auxiliaryGraph.createNode();
		//				nodeNodeMap.put(vertex, vertexNode);
		//				for(int j=0;j<lowEdges.size();j++)
		//				{
		//					Node edgeNode=edgeNodeMap.get(lowEdges.get(j));
		//					auxiliaryGraph.createEdge(vertexNode, edgeNode);
		//				}				
		//			}
		//			//			NodeCursor nr=auxiliaryGraph.nodes();
		//			//			for(nr.toFirst();nr.ok();nr.next())
		//			//			{
		//			//				Node node=nr.node();
		//			//				System.out.println(node);
		//			//				EdgeCursor er=node.outEdges();
		//			//				for(er.toFirst();er.ok();er.next())
		//			//				{
		//			//					Edge edge=er.edge();
		//			//					System.out.println("    "+edge.source()+"-->"+edge.target());
		//			//				}
		//			//			}
		//			//			System.out.println();
		//
		//			return auxiliaryGraph;
		//		}
	}

	private class EdgeEnriched
	{
		static final int NONE=0;
		static final int BLOCK=1;
		static final int THICK=2;
		static final int THIN=3;
		private Edge edge;
		private boolean treeEdge;
		private NodeEnriched lowNode;
		private int type;
		private List<EdgeEnriched> fringe;
		private NodeEnriched source;
		private DataStructure dataStructure;
		private EdgeEnriched upEdge;


		private NodeEnriched target;

		public EdgeEnriched(Edge edge)
		{
			this.edge=edge;
			this.lowNode=null;
			this.fringe=null;
			this.type=-1;
			//			this.dataStructure=new DataStructure();
		}

		public void setUpEdge(EdgeEnriched upEdge)
		{
			this.upEdge=upEdge;
		}

		public EdgeEnriched getUpEdge()
		{
			return this.upEdge;
		}

		public NodeEnriched getSource()
		{
			return source;
		}

		public void setSource(NodeEnriched source)
		{
			this.source = source;
		}

		public NodeEnriched getTarget()
		{
			return target;
		}

		public void setTarget(NodeEnriched target)
		{
			this.target = target;
		}

		public boolean isTreeEdge()
		{
			return this.treeEdge;
		}

		public Edge getEdge()
		{
			return edge;
		}


		public NodeEnriched getLowNode()
		{
			return lowNode;
		}


		public int getType()
		{
			return type;
		}


		public List<EdgeEnriched> getFringe()
		{
			return fringe;
		}


		public List<EdgeEnriched> computeFop(EdgeEnriched otherEdge)
		{
			List<EdgeEnriched> result=new ArrayList<>();
			List<EdgeEnriched> fringe=this.getFringe();
			for(int i=0;i<fringe.size();i++)
			{
				if (fringe.get(i).getLowNode().getTreeNode().isDescendantOf(otherEdge.getLowNode()))
					result.add(fringe.get(i));
			}
			return result;
		}




		public void setTreeEdge(boolean treeEdge)
		{
			this.treeEdge = treeEdge;
			this.dataStructure=new DataStructure(this);
		}

		public void setLowNode(NodeEnriched lowNode)
		{
			this.lowNode = lowNode;
		}

		public void setType(int type)
		{
			this.type = type;
		}

		public void setFringe(List<EdgeEnriched> fringe)
		{
			this.fringe = fringe;
		}

		public String toString()
		{
			return this.getSource()+"-->"+this.getTarget();
		}

		public String toRichString()
		{
			String type="";
			switch(this.type)
			{
			case EdgeEnriched.NONE:
			{
				type="none";
				break;
			}
			case EdgeEnriched.BLOCK:
			{
				type="block";
				break;
			}
			case EdgeEnriched.THIN:
			{
				type="thin";
				break;
			}
			case EdgeEnriched.THICK:
			{
				type="thick";
			}
			}
			return this.getSource()+"-->"+this.getTarget()+" ("+type+")";
		}
	}

	private class NodeEnriched
	{
		private Node node;
		private List<EdgeEnriched> outEdges;
		private List<List<EdgeEnriched>> orderedOutEdges;
		private List<EdgeEnriched> lowEdges;


		public List<List<EdgeEnriched>> getOrderedOutEdges()
		{
			return orderedOutEdges;
		}

		public void setOrderedOutEdges(List<List<EdgeEnriched>> orderedOutEdges)
		{
			this.orderedOutEdges = orderedOutEdges;
		}

		private NodeTree<NodeEnriched> treeNode;

		public NodeTree<NodeEnriched> getTreeNode()
		{
			return treeNode;
		}

		public void setTreeNode(NodeTree<NodeEnriched> treeNode)
		{
			this.treeNode = treeNode;
		}

		public NodeEnriched(Node node)
		{
			this.node=node;
			outEdges=new ArrayList<>();
			lowEdges=new ArrayList<>();
		}

		public void addOutEdge(EdgeEnriched edge)
		{
			outEdges.add(edge);
		}

		public String toString()
		{
			return this.node.index()+"";
		}

	}

	private class DataStructure
	{
		private List<EdgeEnriched> edges;
		private List<List<EdgeEnriched>[]> constraints;
		private EdgeEnriched referenceEdge;
		public DataStructure(EdgeEnriched referenceEdge)
		{
			this.referenceEdge=referenceEdge;
			this.edges=new ArrayList<>();
			if(!this.referenceEdge.isTreeEdge())
			{
				this.edges.add(referenceEdge);
			}
			this.constraints=new ArrayList<>();
		}

		public void addDSEdge(List<EdgeEnriched> list)
		{
			this.edges.addAll(list);
		}
		public void deleteDSEdge(EdgeEnriched edge)
		{
			this.edges.remove(edge);
		}
		public List<EdgeEnriched> getDSEdges()
		{
			return this.edges;
		}
		public void addConstraint(List<EdgeEnriched>[] constraint)
		{
			this.constraints.add(constraint);
		}

		public List<List<EdgeEnriched>[]> getDSConstraints()
		{
			return this.constraints;
		}

		public void print()
		{
			System.out.println("-->DS-Edges");
			for(int i=0;i<this.edges.size();i++)
			{
				System.out.println("     "+edges.get(i).getSource()+"->"+edges.get(i).getTarget());
			}
			System.out.println("-->DS-Constraints");
			for(int i=0;i<this.constraints.size();i++)
			{
				System.out.print("{");
				for(int j=0;j<this.constraints.get(i)[0].size();j++)
				{
					System.out.print(" "+this.constraints.get(i)[0].get(j).getSource()+"-->"+this.constraints.get(i)[0].get(j).getTarget());
				}
				System.out.print(" } =|= {");
				for(int j=0;j<this.constraints.get(i)[1].size();j++)
				{
					System.out.print(" "+this.constraints.get(i)[1].get(j).getSource()+"-->"+this.constraints.get(i)[1].get(j).getTarget());
				}
				System.out.println(" }");
			}
		}

	}

}
