package bee.demo.graphTemplate;

import java.awt.Color;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import y.base.Edge;
import y.base.Node;
import y.view.EdgeRealizer;
import y.view.Graph2D;
import bee.option.options.Utilities;

public class EmbeddedGraph
{
	private int [] nodeOrder;

	private int [] [] embeddedEdges;
	
	int pages;
	public EmbeddedGraph()
	{
		this.nodeOrder=null;
		this.embeddedEdges=null;
		pages=0;
	}
	
	public EmbeddedGraph(int[] nodeOrder, int[][] embeddedEdges, int pages)
	{
		this.nodeOrder=nodeOrder;
		this.embeddedEdges=embeddedEdges;
		this.pages=pages;
	}
	
	public EmbeddedGraph(Graph2D graph)
	{
		load(graph, false);
	}
	
	public void clear()
	{
		this.nodeOrder=null;
		this.embeddedEdges=null;
		pages=0;
	}
	
	public int getNumPages(){
		return this.pages;
//		int [] array= new int [Utilities.COLORS.length];
//		for(int i=0; i<array.length;i++){
//			array[i]=0;
//		}
//		for(int i=0;i<embeddedEdges.length;i++){
//			array[embeddedEdges[i][2]]++;
//			
//		}
//		int pages=0;
//		for(int i=0; i<array.length;i++){
//			//System.out.println(""+array[i]);
//			if(array[i]!=0)
//				pages++;
//		}
//		System.out.println(pages+" pages from embedded");
//		return pages;
	}
	
	public void load(Graph2D graph, boolean colored)
	{
		Node[] nodes=graph.getNodeArray();
		this.nodeOrder=new int [nodes.length];
		for(int i=0;i<nodes.length;i++)
		{
			this.nodeOrder[i]=nodes[i].index();
		}
		Edge[] edges=graph.getEdgeArray();
		this.embeddedEdges=new int [edges.length] [3];
		this.pages=0;
		for(int i=0;i<edges.length;i++)
		{
			int source=Math.min(edges[i].source().index(), edges[i].target().index());
			int target=Math.max(edges[i].source().index(), edges[i].target().index());
			
			if(!colored){
				this.embeddedEdges[i][0]= source; //new EmbeddedEdge(source, target);
				this.embeddedEdges[i][1]= target;
			}
			else{
				EdgeRealizer er=graph.getRealizer(edges[i]);
				//this.embeddedEdges[i]=new EmbeddedEdge(source, target, er.getLineColor());
				this.embeddedEdges[i][0]= source; 
				this.embeddedEdges[i][1]= target;
				this.embeddedEdges[i][2]= Utilities.getColorIndex(er.getLineColor());//Utilities.getColorIndex(c)
				if(this.embeddedEdges[i][2]+1>=pages)
					pages=this.embeddedEdges[i][2]+1;
			}
		}
//		pages++;
	}
	
	public int[] getNodeOrder()
	{
		return nodeOrder;
	}
	
	public void setNodeOrder(int[] nodeOrder){
		this.nodeOrder=nodeOrder;
	}

	public int [][] getEmbeddedEdges()
	{
		return embeddedEdges;
	}
	
	public void setEmbeddedEdges(int [][] embeddedEdges){
		this.embeddedEdges=embeddedEdges;
	}
	
	public void colorEdge(Edge e, int colorIndex)
	{
		int source=e.source().index();
		int target=e.target().index();
		this.colorEdge(source, target, colorIndex);
		
	}
	
	public void colorEdge(int source, int target, int colorIndex)
	{
//		if(colorIndex>=pages)
//			pages++;
		for(int i=0;i<this.embeddedEdges.length;i++)
		{
			if((int)this.embeddedEdges[i][0]==source && (int)this.embeddedEdges[i][1]==target
					||(int)this.embeddedEdges[i][1]==source && (int)this.embeddedEdges[i][0]==target)
			{
				this.embeddedEdges[i][2]=colorIndex;//.setColor(colorIndex);
				return;
			}	

		}
	}
	
//	public void colorEdge(Edge e, Color c)
//	{
//		int colorIndex=Utilities.getColorIndex(c);
//		this.colorEdge(e, colorIndex);
//	}
//	
//	public void colorEdge(int source, int target, Color c)
//	{
//		int colorIndex=Utilities.getColorIndex(c);
//		this.colorEdge(source, target, colorIndex);
//	}
	
	public boolean isEmbedded()
	{
		for(int i=0;i<this.embeddedEdges.length;i++)
		{
			if(! ((int)(this.embeddedEdges[i][2])>-1))
				return false;
		}
		return true;
	}
	
	public void moveEdges(List<int[]> edgeIndex, int toPage)
    {
		if(toPage>=pages)
			pages++;
		if(edgeIndex==null)
			return;
	    for(int i=0;i<edgeIndex.size();i++)
	    {
	    	this.colorEdge(edgeIndex.get(i)[0], edgeIndex.get(i)[1], toPage);
	    	
	     }
	  
    }
	
	public void moveEdge(int[] e, int toPage) {
		this.colorEdge(e[0], e[1], toPage);
		
	}
	
	public void moveNodes(List<Integer> nodeIndex, int distIndex)
    {
		if(distIndex>0){			//right
			for(int s=nodeIndex.size()-1;s>=0;s--)
			{
				int startIndex=nodeIndex.get(s);
				int node= this.nodeOrder[startIndex];

				int endIndex=Math.min(this.nodeOrder.length-1, startIndex+distIndex);
				for(int i=startIndex+1;i<=endIndex;i++){				
					this.nodeOrder[i-1]=this.nodeOrder[i];
				}
				this.nodeOrder[endIndex]=node;
				reverseEdges( node);//**
			}
		}else{
			for(int s=0;s<nodeIndex.size();s++)
			{
				int startIndex=nodeIndex.get(s);
				int node= this.nodeOrder[startIndex];
				
				int endIndex= Math.max(startIndex+distIndex, 0);
				for(int it=startIndex-1; it>=endIndex; it--){
					this.nodeOrder[it+1]=this.nodeOrder[it];
				}
				this.nodeOrder[endIndex]=node;
				reverseEdges( node);//**
			}
		}
	    
    }
	
	
	
//	public void deletePage(int page){
//		//delete one page when put its edges in page 0 (color index=0)
//		//and put the edges which are in the next pages (edgepage color index >page) to pages whith index edgepage-1
//
//		for(int i=0;i<embeddedEdges.length;i++){
//			int [] edge =embeddedEdges[i];
//
//			if(edge[2]==page)
//				edge[2]=0;
//
//			if(edge[2]>page)
//				edge[2]=edge[2]-1;
//
//			
////			System.out.println(" deletepage method : the edge"+ edge[0]+"-"+edge[1] + " has color " + edge[2]);
//			
//		}//for
//	}
	
	public void deletePage(int fromPage, int toPage){
		//delete one page when put its edges in page 0 (color index=0)
		//and put the edges which are in the next pages (edgepage color index >page) to pages whith index edgepage-1

		pages--;
		for(int i=0;i<embeddedEdges.length;i++){
			int [] edge =embeddedEdges[i];

			if(edge[2]==fromPage)
				edge[2]=toPage;

			if(edge[2]>fromPage)
				edge[2]=edge[2]-1;

			
//			System.out.println(" deletepage method : the edge"+ edge[0]+"-"+edge[1] + " has color " + edge[2]);
			
		}//for
	}

	
	//******************************************************************************************************************
	public void reverseEdges(int node){
		
		Map<Integer,Integer> nodeIndex= new HashMap<Integer,Integer>();
		
		for (int k=0;k<nodeOrder.length;k++){
			nodeIndex.put(nodeOrder[k], k);
//			System.out.println(" the node"+ nodeOrder[k] + " is " + k+ " in nodeIndex Map");
		}
		
		
		for(int i=0;i<embeddedEdges.length;i++){
			int [] edge =embeddedEdges[i];
			
			if((int)edge[0] ==node){
				if(nodeIndex.get(edge [1]) <nodeIndex.get(node)){
					edge[0]=edge[1];
				edge [1]=node;
				}
			}

			if((int)edge[1]==node){
				if(nodeIndex.get(edge[0]) > nodeIndex.get(node)){
					edge[1]=edge[0];
					edge[0]=node;
				}
			}
		}
	}

	public void printNodes()
	{
		for(int i: this.nodeOrder)
			System.out.print(i+" ");
		System.out.println();
	}
	

//*****
	
	
	
	
	
//	public class EmbeddedEdge
//	{
//		private int source;
//		private int target;
//		private int color;
//		
//		public EmbeddedEdge(int source, int target)
//		{
//			this(source, target, -1);
//		}
//
//		
//
//		public EmbeddedEdge(int source, int target, int colorIndex)
//		{
//			this.source=source;
//			this.target=target;
//			this.color=colorIndex;
//		}
//		
//		public EmbeddedEdge(int source, int target, Color c)
//		{
//			this(source, target,Utilities.getColorIndex(c));
//		}
//		
//		public int getSource()
//		{
//			return source;
//		}
//
//
//		public int getTarget()
//		{
//			return target;
//		}
//
//
//		public int getColor()
//		{
//			return color;
//		}
//
//		public void setColor(int color)
//		{
//			this.color = color;
//		}
//		
//		public boolean isEmbedded()
//		{
//			return this.color>-1;
//		}
//		
//		//***
//		public void setSource(int source2) {
//			this.source=source2;			
//		}
//	
//		public void setTarget(int target2) {
//			this.target=target2;			
//		}
//	}




}




	

