package bee.planar;


import java.awt.Color;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

import bee.option.options.Utilities;
import y.algo.GraphConnectivity;
import y.base.Edge;
import y.base.EdgeCursor;
import y.base.EdgeList;
import y.base.Node;
import y.base.NodeCursor;
import y.layout.planar.DrawingEmbedder;
import y.layout.planar.Face;
import y.layout.planar.PlanarInformation;
import y.view.Graph2D;
import y.view.NodeRealizer;

public class PeelingInfo {

	Graph2D graph;
	//LinkedList<LinkedList<Node>> peelingList;



	public PeelingInfo(Graph2D graph){		
		System.out.println("--------------------------- \n let's find the peeling info \n");
		//peelingList = new LinkedList<LinkedList<Node>>();
		//nodeLevelList= new 	LinkedList<Node>();
		this.graph=graph;		
	}


	public  LinkedList<Node> outerFace(Graph2D graph,boolean cc){//LinkedList<LinkedList<Node>>


		LinkedList<Node> outerfaceList = new LinkedList<Node>();

		PlanarInformation planarinfo= new PlanarInformation(graph);

		DrawingEmbedder de= new DrawingEmbedder();
		de.setPlanarInformation(planarinfo);
		de.setKeepBends(false);
		de.embed();

		System.out.println("outer face "+ planarinfo.getOuterFace().toString());		

		Face outerface=planarinfo.getOuterFace();

		EdgeCursor ec=outerface.edges();
		for (ec.toFirst();ec.ok();ec.next()){
			Edge edge=ec.edge();


			if(outerfaceList.isEmpty()){//outerfaceList.peek()==source
				Node source=edge.source();
				outerfaceList.add(source);
			}		


			Node target= edge.target();
			outerfaceList.add(target);

		}//for


		System.out.println(outerfaceList.toString());

		if(!cc){//cc==false
			Collections.reverse(outerfaceList);
			System.out.println(outerfaceList.toString());
		}

		//peelingList.add(singleList);

		return outerfaceList;

	}



	public void innerGraph(){

//		LinkedList<Node> singleList=peelingList.getFirst();
//		//Graph2D newgraph= new Graph2D(graph);
//		for(int i=0;i<singleList.size()-1;i++)
//			graph.removeNode(singleList.get(i));
//
//
//		if(graph!=null){
//			boolean biconnected =	GraphConnectivity.isBiconnected(graph) ;
//
//			if(!biconnected){
//
//				EdgeList[] el=GraphConnectivity.biconnectedComponents(graph);
//				Graph2D newgraph=(Graph2D) graph.createGraph();
//
//				for(int i=0;i<el.length;i++){
//					System.out.println(el[i].toString());
//				}
//
//				function(graph,null,true);
//			}else
//				function(graph,null,true);
//
//		}
	}//inner graph


	public LinkedList<LinkedList<Node>> function(Graph2D graph, Node leader, boolean cc){

		System.out.println("graph in function");
		System.out.println(graph.toString());

		//find outer face of subgraph
		//outerFace(cc);			

		//leader=peelingList.getLast().getFirst();

		//calc biconnected components
		return null;
	}

	
	

	public LinkedList<LinkedList<Integer>> findLevels(){

		LinkedList<LinkedList<Integer>> levelsIndex= new LinkedList<LinkedList<Integer>>();		
		LinkedList<LinkedList<Node>> levels=new LinkedList<LinkedList<Node>>();	
		
		
		//creates a boolean help array= nodesarray
		boolean [] nodesarray = new boolean [graph.nodeCount()+1];		

//		NodeCursor nc =graph.nodes();
//		for(nc.toFirst();nc.ok();nc.next()){
//			nodesarray[nc.node().index()]=false;
//		}
		
		for(int i=0;i<nodesarray.length;i++){
			nodesarray[i]=false;
		}

		/*
		 *   find level 0
		 */
		LinkedList<Node> Level0List=outerFace(graph,true);
		
		LinkedList<Integer> levelOIndexList=new LinkedList<Integer>();
		
		for(int i=0;i<Level0List.size();i++){
			
			nodesarray[Integer.parseInt(Level0List.get(i).toString())]=true;
			//levelOIndexList.add(Level0List.get(i).index());//ta indexes trela8hkan! :P
			levelOIndexList.add(Integer.parseInt(Level0List.get(i).toString()));
			
			NodeRealizer nr= graph.getRealizer(Level0List.get(i));
		    nr.setFillColor(Color.magenta);
		}

		System.out.println("level 0");
		System.out.println(Level0List.toString());
		
		levels.add(Level0List);
		levelsIndex.add(levelOIndexList);

		System.out.println("levelsIndex: "+levelsIndex.toString());
		/*
		 *   find other levels
		 */

		int number=Level0List.size();
		
				
		LinkedList<Node> previousList=Level0List;
		while(number< (nodesarray.length +1)){
			
			LinkedList<Node> nextlevel= new LinkedList<Node>();
			Color color= new Color((int)(Math.random() * 0x1000000));
			for(int i=0;i<previousList.size();i++){//for each node in outer face
				NodeCursor nodecursor=previousList.get(i).neighbors();
				for(nodecursor.toFirst();nodecursor.ok();nodecursor.next()){
					//System.out.println("Neighbor "+nodecursor.node());
					//if(!nodesarray[nodecursor.node().index()] ) {//narray[node index]=false
					if(!nodesarray[Integer.parseInt(nodecursor.node().toString())] ) {//narray[node index]=false   
						nextlevel.add(nodecursor.node());
						System.out.println("Neighbor"+nodecursor.node()+" added to next level");
						//nodesarray[nodecursor.node().index()]=true;
						nodesarray[Integer.parseInt(nodecursor.node().toString())]=true;
						NodeRealizer nr= graph.getRealizer(nodecursor.node());
					    nr.setFillColor(color);
					}
				}

			}
		
			
//			Graph2D subgraph= new Graph2D(graph );
//			NodeCursor ncr= subgraph.nodes();
//			for(ncr.toFirst();ncr.ok();ncr.next()){
//
//				for(int i=0;i<(previousList.size()-1);i++){
//
//					int index=	previousList.get(i).index();				
//
//					if(ncr.node().index()==index)
//						subgraph.removeNode(ncr.node());}
//			}
//
//
//			nextlevel=outerFace(subgraph,true);
			
			
			/*
			 * this block needs fix
			 */
			LinkedList<Node> used= new LinkedList<Node>();
			if(!nextlevel.isEmpty()){
				
				LinkedList<Node> helpList= new LinkedList<Node>();
				
				Node node=nextlevel.removeFirst();
				used.add(node);
				System.out.println("the first node "+node+" added to used list  ");
				
				while(!nextlevel.isEmpty()){
					//look up neighbors
					NodeCursor nodecursor= node.neighbors();
					System.out.println("node "+node+"neighbors #"+nodecursor.size());
					
					for(nodecursor.toFirst();nodecursor.ok();nodecursor.next()){
						System.out.println("first neigh"+ nodecursor.node());				

						boolean neighbor=false;
						int num=0;
						int position=0;
						for(int i=0;i<nextlevel.size();i++){//den mpainei pote edw mesa
							System.out.println("inside for");
							//if(index==nextlevel.get(i).index()){
							//if(node.toString().equals(nextlevel.get(i).toString())){
							if(	node.equals((Object)(nextlevel.get(i)))){								
								position=i;								
								num++;
								//node=nextlevel.remove(i);
								//used.add(node);
								neighbor=true;
								System.out.println("Node "+node+" has neighbor the node "+nextlevel.get(i)+" the num of neigh in nextlevel"+num);//+"  with index"+nextlevel.get(i).index());
							}	else{
								System.out.println("it cant find the neigh ");
							}

						}						

						if(num>1){
							node=nextlevel.remove(position);
							helpList.add(used.getLast());
							for(int i=0;i<num;i++)
								used.add(node);
							System.out.println("Node "+node+" added at used with frequency "+num);
						}
						//look up help list

						
						if(!helpList.isEmpty()){
							for(int i=0;i<helpList.size();i++){
								used.add(node);
								//System.out.println("node  "+node);
								//if(index==helpList.get(i).index()){		
								//if(	node.equals((Object)(helpList.get(i)))){
								//	if(node.toString().equals(helpList.get(i).toString())){
								if(node.toString()==(helpList.get(i).toString())){
									node=helpList.remove(i);
									used.add(node);
									System.out.println("node "+node+" added to used list with index "+node.index());
									neighbor=true;
								}						

							}	
						}


						if(neighbor==false && (!nextlevel.isEmpty())){
							node=nextlevel.removeFirst();
							System.out.println("we change component");
							//used.add(null);
							used.add(node);
							System.out.println("node "+node+" added to used list with index "+node.index());
						}
					}//for each neighbor
				}//while 
			}
			System.out.println("used list");
			System.out.println(used.toString());
			
			LinkedList<Integer> nextlevelIndexList=new LinkedList<Integer>();
			for(int i=0;i<used.size();i++){
				nextlevelIndexList.add(Integer.parseInt(used.get(i).toString()));
			}


			if(nextlevelIndexList.isEmpty())
				return levelsIndex;

			
			levels.add(used);
			levelsIndex.add(nextlevelIndexList);	
			previousList=used;

			number =number+used.size();
			System.out.println("number"+number);

		}//while

		return levelsIndex;
	}


}//PeelingInfo class
