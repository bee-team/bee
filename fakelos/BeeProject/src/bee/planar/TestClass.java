package bee.planar;

import y.algo.GraphConnectivity;
import y.base.Edge;
import y.base.EdgeList;
import y.base.Node;
import y.view.Graph2D;

public  class TestClass {

	
	Graph2D graph;
	
	public  TestClass(){
		
		  graph= new Graph2D();
		
		Node node1=graph.createNode();
		Node node2=graph.createNode();
		Node node3=graph.createNode();
		Node node4=graph.createNode();
		Node node5=graph.createNode();
		Node node6=graph.createNode();
		Node node7=graph.createNode();
		Node node8=graph.createNode();
		Node node9=graph.createNode();
		Node node10=graph.createNode();
		Node node11=graph.createNode();
		Node node12=graph.createNode();
		
		
//		Node node13=graph.createNode();
//		Node node15=graph.createNode();
//		Node node14=graph.createNode();
		
		Edge edge1= graph.createEdge(node1,node2);
		Edge edge2= graph.createEdge(node1,node7);
		Edge edge3= graph.createEdge(node1,node8);
		Edge edge4= graph.createEdge(node2,node3);
		Edge edge5= graph.createEdge(node2,node8);
		Edge edge6= graph.createEdge(node3,node4);
		Edge edge7= graph.createEdge(node3,node5);
		 graph.createEdge(node5,node4);
		 graph.createEdge(node8,node3);
		 graph.createEdge(node7,node3);
		Edge edge8= graph.createEdge(node6,node5);
		Edge edge9= graph.createEdge(node7,node9);
		Edge edge10= graph.createEdge(node9,node11);
		Edge edge11= graph.createEdge(node9,node12);
		Edge edge12= graph.createEdge(node9,node10);
		Edge edge13= graph.createEdge(node12,node11);
		Edge edge14= graph.createEdge(node10,node11);
		Edge edge15= graph.createEdge(node12,node10);
		
//	 graph.createEdge(node13,node14);
//		 graph.createEdge(node14,node15);
//		 graph.createEdge(node13,node15);
	
	}
	
	
	public void print(){
		EdgeList[] el=GraphConnectivity.biconnectedComponents(graph);
		for(int i=0;i<el.length;i++){
			System.out.println(el[i].toString());
		}
		System.out.println(el.toString());
	}
	
	
	public static void main(String args []){
		TestClass tc=new TestClass();
		tc.print();
    }
}



