package wrapper;


import y.base.Edge;
import y.base.Node;
import y.layout.circular.CircularLayouter;
import y.view.EdgeRealizer;
import y.view.Graph2D;
import y.view.NodeRealizer;
import bee.demo.graphTemplate.EmbeddedGraph;
import bee.option.options.Utilities;

public class BEWrapper
{
	public static Graph2D createGraph2DFromEmbeddedGraph(EmbeddedGraph graph)
	{
//		Graph simpleGraph=new Graph();
		Graph2D result=new Graph2D();
//		result.
		int[] nodeOrder=graph.getNodeOrder();
		int noOfNodes=nodeOrder.length-1;
		for(int i=0;i<nodeOrder.length;i++)
		{
			if(nodeOrder[i]>noOfNodes)
				noOfNodes=nodeOrder[i];
		}
		noOfNodes++;
		boolean[] nodeExists=new boolean[noOfNodes];
		for(int i=0;i<nodeOrder.length;i++)
		{
			nodeExists[nodeOrder[i]]=true;
		}
		Node[] nodeTemp=new Node[noOfNodes];
		for(int i=0;i<nodeTemp.length;i++)
		{
			Node node=result.createNode();
			if(node.index()!=i)
				System.err.println("error");
			result.setLabelText(node, ""+node.index());
//			if(nodeExists[i])
				nodeTemp[i]=node;
//			else
//				result.removeNode(node);
//			NodeRealizer nr=result.getRealizer(node);
//			nr.
		}
		int i0=0;
		Node[] nodes=result.getNodeArray();
		for(Node t:nodes)
		{
			if(t!=nodeTemp[i0])
				System.err.println("Error");
			i0++;
		}
		
		int[][] embeddedEdges=graph.getEmbeddedEdges();
//		Node[] nodes=new Node[nodeOrder.length];
//		for(int i=0;i<nodes.length;i++)
//		{
//			Node node=result.createNode();
//			result.setLabelText(node, ""+node.index());
//			nodes[i]=node;
////			NodeRealizer nr=result.getRealizer(node);
////			nr.
//		}
		for(int i=0;i<embeddedEdges.length;i++)
		{
			int[] embeddedEdge=embeddedEdges[i];
			Edge edge=result.createEdge(nodeTemp[embeddedEdge[0]], nodeTemp[embeddedEdge[1]]);
			EdgeRealizer er=result.getRealizer(edge);
			er.setLineColor(Utilities.COLORS[embeddedEdge[2]]);			
		}
		for(int i=0;i<nodeTemp.length;i++)
		{
			if(!nodeExists[i])
				result.removeNode(nodeTemp[i]);
		}
		
		CircularLayouter cl = new CircularLayouter();
		cl.doLayout(result);		
		return result;
	}
}
